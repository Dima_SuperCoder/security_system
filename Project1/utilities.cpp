#include "utilities.h"
#include <vector>
#include <cctype>

std::string MarshalString(System::String ^ s) {
	using namespace System::Runtime::InteropServices;
	const char* chars =
		(const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
	std::string res = chars;
	Marshal::FreeHGlobal(System::IntPtr((void*)chars));
	return res;
}


bool is_variable(std::string c)
{
	char first_char = c.c_str()[0];
	std::vector<char> symbols = { '/', '\\', '-' };
	int len = c.length();
	for (int i = 1; i < len; ++i) {
		if (std::find(symbols.begin(), symbols.end(), first_char) != symbols.end()) {
			first_char = c.c_str()[i];
		}
		else {
			break;
		}
	}
	if (isdigit(first_char)) { return false; }
	return !islower(first_char);
}

