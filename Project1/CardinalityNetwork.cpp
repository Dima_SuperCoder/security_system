﻿#include "CardinalityNetwork.h"


int CardinalityNetwork::twoceil(int k)
{
	int t = 1;
	while (t < k) { t = t * 2; }
	return t;
}

std::vector <int> CardinalityNetwork::Selectodd(std::vector<int> a)
{
	std::vector <int> res;
	for (int i = 0; i < a.size(); i = i + 2) {
		int t = a[i];
		res.push_back(t);
	}
	return res;
}

std::vector <int> CardinalityNetwork::Selecteven(std::vector<int> a)
{
	std::vector <int> res;
	for (int i = 1; i < a.size(); i = i + 2) {
		int t = a[i];
		res.push_back(t);
	}
	return res;
}

std::vector <int> CardinalityNetwork::HMerge(std::vector<int> &a, std::vector<int> &b, int n, long long int &nVars, std::vector<std::vector<int>> &network)
{
	std::vector<int> res;
	for (int i = 0; i < 2 * n; i++) { int t = ++nVars; res.push_back(t); }
	if (n == 1) {
		//	fprintf(f,"%i %i %i 0\t\n",-a[0], -b[0], res[1]);
		network.push_back({ -a[0], -b[0], res[1] });
		
		//	fprintf(f,"%i %i 0\t\n",-a[0], res[0]);
		network.push_back({ -a[0], res[0] });

		//		fprintf(f,"%i %i 0\t\n",-b[0], res[0]);
		network.push_back({ -b[0], res[0] });
		//	fprintf(f,"%i %i %i 0\t\n",a[0], b[0], -res[0]);
		network.push_back({ a[0], b[0], -res[0] });
	
		//		fprintf(f,"%i %i 0\t\n",a[0], -res[1]);
		network.push_back({ a[0], -res[1] });
		//	fprintf(f,"%i %i 0\t\n",b[0], -res[1]);
		network.push_back({ b[0], -res[1] });
	}
	else
	{
		std::vector<int> aeven = Selecteven(a);
		std::vector<int> aodd = Selectodd(a);
		std::vector<int> beven = Selecteven(b);
		std::vector<int> bodd = Selectodd(b);

		std::vector<int> D = HMerge(aodd, bodd, aodd.size(), nVars, network);
		std::vector<int> E = HMerge(aeven, beven, aeven.size(), nVars, network);
		for (int i = 1; i < n; i++) {
			res[0] = D[0];
			res[2 * n - 1] = E[n - 1];
			//			fprintf(f,"%i %i %i 0\t\n",-D[i+1-1],-E[i-1],res[2*i+1-1]);
			network.push_back({ -D[i + 1 - 1], -E[i - 1], res[2 * i + 1 - 1] });
			//		fprintf(f,"%i %i 0\t\n",-D[i+1-1], res[2*i-1]);
			network.push_back({ -D[i + 1 - 1], res[2 * i - 1] });
			//	fprintf(f,"%i %i 0\t\n",-E[i-1], res[2*i-1]);
			network.push_back({ -E[i - 1], res[2 * i - 1] });

			//			fprintf(f,"%i %i %i 0\t\n",D[i+1-1],E[i-1],-res[2*i-1]);
			network.push_back({ D[i + 1 - 1], E[i - 1], -res[2 * i - 1] });
			//		fprintf(f,"%i %i 0\t\n",D[i+1-1], -res[2*i+1-1]);
			network.push_back({ D[i + 1 - 1], -res[2 * i + 1 - 1] });
			//	fprintf(f,"%i %i 0\t\n",E[i-1], -res[2*i+1-1]);
			network.push_back({ E[i - 1], -res[2 * i + 1 - 1] });
		}
	}
	return res;
}

std::vector<int> CardinalityNetwork::HSort(std::vector<int> &a, int n, long long int &nVars, std::vector<std::vector<int>> &network)
{
	std::vector<int>res;
	if (n / 2 > 1) {
		std::vector<int>l;
		std::vector<int>r;
		for (int i = 0; i < n / 2; i++) {
			l.push_back(a[i]);
			r.push_back(a[n / 2 + i]);
		}
		std::vector<int> lsort = HSort(l, n / 2, nVars, network);
		std::vector<int> rsort = HSort(r, n / 2, nVars, network);
		res = HMerge(lsort, rsort, n / 2, nVars, network);
	}
	else {
		std::vector <int> l;
		std::vector <int> r;
		l.push_back(a[0]);
		r.push_back(a[1]);
		res = HMerge(l, r, 1, nVars, network);
	}
	return res;
}

std::vector<int> CardinalityNetwork::Notmoreatstart(int n, std::vector<int> tobesorted, long long int &nVars, std::vector<std::vector<int>> &cnf)
{
	const int nzero = ++nVars;
	cnf.push_back({ -nzero });
	int hcnt = twoceil(tobesorted.size());
	for (int i = tobesorted.size(); i < hcnt; i++) { tobesorted.push_back(nzero); }
	std::vector<int> r;
	r = HSort(tobesorted, hcnt, nVars, cnf);

	cnf.push_back(r);
	cnf.push_back({ r[n-1] });
	cnf.push_back({ -r[n] });
	return r;

}

std::vector<int>  CardinalityNetwork::Notmoreatstart(int n, int m, std::vector<int> tobesorted, long long int &nVars, std::vector<std::vector<int>> &cnf)
{
	const int nzero = ++nVars; 
	cnf.push_back({ -nzero });
	int hcnt = twoceil(tobesorted.size());
	for (int i = tobesorted.size(); i < hcnt; i++) { tobesorted.push_back(nzero); }
	std::vector<int> r;
	r = HSort(tobesorted, hcnt, nVars, cnf);

	cnf.push_back(r);
	cnf.push_back({ r[n] });
	cnf.push_back({ -r[m+1] });
	return r;
}
