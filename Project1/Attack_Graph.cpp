#include "Attack_Graph.h"

#include <fstream>

void Attack_Graph::add_vert(int number, std::string name, std::string shape, int rank)
{
	AG_vert v;
	v.number = number;
	v.name = name;
	v.shape = shape;
	if (rank > -1) {
		//ranks[rank].push_back(shared_v);
		v.rank = rank;
	}
	auto shared_v = std::make_shared<AG_vert>(v);

	verts[number] = shared_v;
	
}

void Attack_Graph::add_arc(int start_vert, int end_vert)
{
	verts[start_vert]->out_verts.push_back(verts[end_vert]);
	verts[end_vert]->in_verts.push_back(verts[start_vert]);
	std::pair<std::shared_ptr<AG_vert>, std::shared_ptr<AG_vert>> arc = { verts[start_vert],  verts[end_vert] };
	arcs.push_back(arc);
}


void Attack_Graph::add_rank(int vert_number, int rank)
{
	ranks[rank].push_back(verts[vert_number]);
}

void Attack_Graph::reset()
{
	verts.clear();
	ranks.clear();
	arcs.clear();
}


void Attack_Graph::write_gv_graph(std::string filename)
{
	std::ofstream myfile;
	myfile.open(filename.c_str());
	myfile << gv_string().rdbuf();
	myfile.close();
}

std::stringstream Attack_Graph::gv_string()
{
	std::stringstream res;
	res << "strict digraph{ \n rankdir = tb;\n";

	for (auto& arc : arcs) {
		res << arc.first->number << "->" << arc.second->number << ";\n";
	}

	for (auto& vert : verts) {
		res << vert.first << "[label=\"" << vert.second->name << "[" << vert.second->rank << "]" << "\", shape = " << vert.second->shape << "];\n";
	}

	for (auto& rank : ranks) {
		res << "{rank = same; ";
		for (auto& vert : rank.second) {
			res << vert->number << "; ";
		}
		res << "};\n";
	}

	res << " \n}";

	return res;
}
