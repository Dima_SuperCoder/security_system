#include "security_system.h"

#include <fstream>
#include <sstream>

#include "sql_wrapper.h"
#include "utilities.h"
#include "CardinalityNetwork.h"
#include <random>

security_system::security_system()
{
	
}

void security_system::return_all_names(std::vector<std::string>& facts_names, std::vector<std::string>& postcond_names, std::vector<std::string>& attack_names)
{
	for (auto& f : fact_template) {
		facts_names.push_back(f.first);
	}

	for (auto& or_attack : postconditions_template) {
		postcond_names.push_back(or_attack.first);
	}

	for (auto& and_attack : attacks_template) {
		attack_names.push_back(and_attack.first);
	}
}

void security_system::add_fact_template(std::string fact_proto_name, signs s)
{
	all_facts_template[fact_proto_name] = fact(fact_proto_name, s);
	fact_template[fact_proto_name] = all_facts_template[fact_proto_name];

	is_processed = false;
}

bool security_system::add_fact_template(std::string fact_proto_name, std::vector<std::string> in_types, std::vector<std::string> names, std::vector<std::string> groups)
{
	if (!is_name_exists(fact_proto_name)) {
		int len = in_types.size();
		signs s;
		for (int i = 0; i < len; ++i) {
			record r(types[in_types[i]], names[i]);
			std::pair<record, std::string> p(r, groups[i]);
			s.push_back(p);
		}
		all_facts_template[fact_proto_name] = fact(fact_proto_name, s);
		fact_template[fact_proto_name] = all_facts_template[fact_proto_name];

		is_processed = false;
		return true;
	}
	return false;
}

void security_system::add_postcond_template(std::string name, signs s)
{
	all_facts_template[name] = fact(name, s);
	postconditions_template[name] = all_facts_template[name];

	is_processed = false;
}

bool security_system::add_postcond_template(std::string name, std::vector<std::string> in_types, std::vector<std::string> names, std::vector<std::string> groups)
{
	if (!is_name_exists(name)) {
		int len = in_types.size();
		signs s;
		for (int i = 0; i < len; ++i) {
			record r(types[in_types[i]], names[i]);
			std::pair<record, std::string> p(r, groups[i]);
			s.push_back(p);
		}
		all_facts_template[name] = fact(name, s);
		postconditions_template[name] = all_facts_template[name];
		postcond_to_attacks_template[name];

		is_processed = false;
		return true;
	}
	return false;
}

void security_system::add_type(std::string t)
{
	types[t] = std::make_shared<std::string>(t);
}

void security_system::update_type(std::string old_val, std::string new_val)
{
	types[new_val] = types[old_val];
	*types[new_val] = new_val;
	types.erase(old_val);
}

bool security_system::delete_type(std::string t)
{
	if (types.find(t) != types.end()) {
		if (types[t].use_count() == 1) {
			types.erase(t);

			is_processed = false;
			return true;
		}
	}
	return false;
}

std::vector<std::string> security_system::return_types_names()
{
	std::vector<std::string> res;
	for (auto& t : types) {
		res.push_back(t.first);
	}
	return res;
}

bool security_system::delete_attack(std::string atack_name, std::string postcond_name)
{
	if (postconditions_template.find(postcond_name) != postconditions_template.end() && 
		attacks_template.find(atack_name) != attacks_template.end()) {
		attack_rule a = attacks_template[atack_name];
		auto it = std::find(postcond_to_attacks_template[postcond_name].begin(), postcond_to_attacks_template[postcond_name].end(), a);
		if (it != postcond_to_attacks_template[postcond_name].end()) {
			postcond_to_attacks_template[postcond_name].erase(it);

			is_processed = false;
			return true;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}

bool security_system::delete_postcondition(std::string name)
{
	auto or_it = postconditions_template.find(name);
	if (or_it != postconditions_template.end()) {
		auto and_it = postcond_to_attacks_template.find(name);
		if (postcond_to_attacks_template[name].size() == 0) {
			postconditions_template.erase(or_it);
			postcond_to_attacks_template.erase(and_it);
			
			is_processed = false;
			return true;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}

bool security_system::delete_fact_template(std::string fact_name)
{
	for (auto& attack : attacks_template) {
		if (attack.second.is_contain_fact(fact_name)) {
			auto f = fact_template.find(fact_name);
			fact_template.erase(f);
			
			auto ff = all_facts_template.find(fact_name);
			all_facts_template.erase(ff);

			is_processed = false;
			return false;
		}
	}

	return true;
}

std::vector<std::string> security_system::return_attack_for_postcondition(std::string postcond_name)
{
	std::vector<std::string> res;
	for (auto& a : postcond_to_attacks_template[postcond_name]) {
		res.push_back(a.return_name());
	}
	return res;
}

void security_system::solve_task_plus(sql_wrapper^ wrapper, double& gen_time, double& solve_time, bool test_calc)
{
	this->test_calc = test_calc;

	double start_time = cpuTime();
	
	proceed_with_steps_eq_one(wrapper);


	std::map<int, int> attack_to_pcond;
	for (auto& deuce : postcond_to_attack_num) {
		for (auto& a : deuce.second) {
			attack_to_pcond[a] = deuce.first;
		}
	}

	//������������� ������� �������� � ��������� ������ �������
	values.clear();
	values.push_back({false});
	for (auto& n : facts_num) {
		values[0].push_back(true);
	}
	for (auto& n : postcond_to_attack_num) {
		values[0].push_back(false);
		values[0].push_back(false); 
	}
	for (auto& n : attack_to_facts_num) {
		values[0].push_back(false);
		values[0].push_back(false);
	}
	for (auto& n : scissoring_nums) {
		values[0].push_back(false);
		values[0].push_back(false);
	}

	gen_time = cpuTime() - start_time;
	double tmp_time = cpuTime();
	//�������� ���� ��������
	bool continue_calc = true;
	while (continue_calc) {
		int values_real_size = values.size() - 1;
		std::vector<bool> tmp_vals = values[values_real_size];

		//������� ��� ���������
		for (auto& el : scissoring_nums) {
			tmp_vals[el.second[0]] = tmp_vals[el.second[0]] | tmp_vals[el.second[1]];
		}

		//������� ��� ����
		for (auto& deuce : attack_to_facts_num) {			
			bool res = true;
			for (auto el : deuce.second) {
				if (abs(el) > start_postconditions) {
					if (el > 0) {
						el = scissoring_nums[el][1];
					}
					else {
						el = -scissoring_nums[abs(el)][1];
					}
				}
				if (el > 0) {
					res = res && tmp_vals[el];
				}
				else {
					res = res && !tmp_vals[abs(el)];
				}
			}
			res = res && !tmp_vals[scissoring_nums[attack_to_pcond[deuce.first]][1]];
			tmp_vals[deuce.first] = res;			
		}

		//������� ��� �����������
		for (auto& deuce : postcond_to_attack_num) {
			bool res = false;
			for (auto& el : deuce.second) {
				res = res | tmp_vals[el];
			}
			tmp_vals[deuce.first] = res;
		}

		//������� ��� ���������
		for (auto& el : scissoring_nums) {
			tmp_vals[el.second[1]] = tmp_vals[el.second[0]] | tmp_vals[el.first];
		}
		
		if (tmp_vals == values[values_real_size]) {
			continue_calc = false;
		}
		else {
			values.push_back(tmp_vals);
		}
	}

	solve_time = cpuTime() - tmp_time;

	if (!test_calc) {
		generate_ag_plus(wrapper);
	}
}

void security_system::write_gv_file_iterable_plus(std::string output_file, sql_wrapper^ wrapper)
{

	std::map<int, std::string> all_names = get_names_from_table(wrapper);

	std::ofstream myfile;
	myfile.open(output_file.c_str());
	//fopen_s(&f, filne_path.c_str(), "w+");
	std::set<int> verts;
	std::map<int, std::vector<int>> verts_ranks;
	std::map<int, int> verts_to_step, and_verts;
	myfile << "strict digraph{ \n rankdir = TB;\n";

	int size = values.size();

	for (int i = 1; i < size; ++i) {
		for (auto& l : assumptions[0]) {
			auto lit = -l;
			if (values[i][lit] && (postcond_to_attack_num.find(lit) != postcond_to_attack_num.end())) {
				verts.insert(lit);
				if (verts_to_step.find(lit) == verts_to_step.end()) {
					verts_to_step[lit] = i;

					if (lit > start_postconditions && lit <= start_attacks) { verts_ranks[i].push_back(lit); }


					for (auto& v : postcond_to_attack_num[lit]) {
						if (values[i][v]) {

							myfile << v << "->" << lit << ";\n";
							and_verts[v] = i;
							verts_to_step[v] = i;
						}
					}

				}
			}
		}
	}

	for (auto& and_vert : and_verts) {
		for (auto& v : attack_to_facts_num[and_vert.first]) {
			if (values[and_vert.second][and_vert.first]) {
				myfile << v << "->" << and_vert.first << ";\n";
				verts_to_step[v] = and_vert.second - 1;
				verts.insert(v);
				verts.insert(and_vert.first);
			}
		}
	}

	for (auto& vert : verts) {
		myfile << vert << "[label = \"";
		if (vert <= start_postconditions) {
			myfile << all_names[vert] << "[0]\" ,shape = rectangle";
		}
		else if (vert > start_postconditions && vert <= start_attacks) {
			myfile << all_names[vert] << "[" << verts_to_step[vert] << "]\" ,shape = diamond";
		}
		else if (vert > start_attacks) {
			myfile << all_names[vert] << "[" << verts_to_step[vert] << "]\" ,shape = oval";
		}
		myfile << "];" << std::endl;
	}

	for (auto& r : verts_ranks) {
		myfile << "{rank = same; ";
		for (auto& v : r.second) {
			myfile << v << "; ";
		}
		myfile << "}\n";
	}

	myfile << " \n}";
	myfile.close();

}

void security_system::add_patch_variable(int var)
{
	patch_variables.push_back(var);
}

void security_system::delete_patch_variable(int var)
{

}

bool security_system::add_attack_template(std::string name, signs s, std::vector<fact> f, std::string postcond)
{
	if (!is_name_exists(name)) {
		attack_rule a(name, s, f);
		a.set_pcond(postcond);
		attacks_template[name] = a;
		postcond_to_attacks_template[postcond].push_back(attacks_template[name]);
		return true;
	}
	return false;
}

std::string security_system::return_cnf()
{
	std::string res;

	for (auto& row : cnf) {
		for (auto& el : row) {
			res += std::to_string(el) + " ";
		}
		res += "0\t\n";
	}

	return res;
}

std::string security_system::solve_task_iter(sql_wrapper^ wrapper, double& gen_time, double& solve_time, bool test_calc)
{

	this->test_calc = test_calc;
	double start_time = cpuTime();
	proceed_with_steps_eq_one(wrapper);

	encode_cnf_iterable();
	gen_time = cpuTime() - start_time;
	run_solver_iterable(solve_time);
	
	if (!test_calc) {
		generate_ag_iterable(wrapper);
	}

	return "Solved";
}

std::string security_system::solve_task_iter_for_patch(sql_wrapper^ wrapper, int steps, double& gen_time, double& solve_time, std::vector<int> attacks_to_block, bool test_calc, int& n, int m/*=0*/, double timeout/*=0.0*/)
{
	this->test_calc = test_calc;

	set_steps(steps);

	double start_time = cpuTime();

	process_enumeration(wrapper);
	
	encode_cnf_for_patch(attacks_to_block, n, m);
	gen_time = cpuTime() - start_time;
	minisat22_wrapper wrp;
	Minisat::Problem p = wrp.convert_to_problem(cnf);
	run_solver_single_for_patch(solve_time, p, n, m, timeout);
	for (auto& i : p) {
		delete i;
		i = NULL;
	}
	p.clear();
	if (sat_assignments.size() > 0) {
		return "Solved";
	}
	else {
		return "Unsolved";
	}

}

void security_system::set_steps(int s)
{
	if (steps != s) {
		steps = s;
		steps_changed = true;
	}
}

void security_system::enumerate(sql_wrapper^ db)
{
	nVars = 0;
	for (auto& f : fact_template) {
		db->enumerate_table(f.second.return_name(), nVars, false);
	}
	start_postconditions = nVars;
 	enumerate_postconditions(db);
	start_attacks = nVars;
 	enumerate_attacks(db);
	delta = nVars - start_postconditions;
	end_attack = nVars;
	enumerate_attacks_by_steps();
}

void security_system::encode_cnf_base()
{
	for (int i = 1; i <= steps; ++i) {
		std::map<int, int> attack_to_pcond;
		for (auto& pcond : postcond_to_attack_num) {
			std::vector<int> disj;
			for (auto& k : pcond.second) {
				attack_to_pcond[k + delta * (i-1)] = pcond.first + delta * (i-1);
				disj.push_back(k + delta * i);
			}
			encode_disjunct(cnf, pcond.first + delta * i, disj);
			sat_connections[pcond.first + delta * (i-1)] = disj;

			encode_disjunct(cnf, scissoring_nums[pcond.first][i], { scissoring_nums[pcond.first][i - 1],  pcond.first + delta * (i - 1) });
		}
		for (auto& attack : attack_to_facts_num) {
			std::vector<int> conj = attack.second;
			
			for (auto& el : conj) {
				if (el > start_postconditions) {
					el = scissoring_nums[el][i];
				}
			}		
			
			if (current_scissoring_mode == postcondition_scissoring) {
				conj.push_back(-scissoring_nums[attack_to_pcond[attack.first]][i]);
			}
			else if (current_scissoring_mode == attacks_scissoring) {
				conj.push_back(-scissoring_nums[attack.first][i]);
				encode_disjunct(cnf, scissoring_nums[attack.first][i], { scissoring_nums[attack.first][i - 1],  attack.first + delta * (i - 1) });
			}
			//conj.push_back(-and_to_or[attack.first + delta * (i-1)]);
			encode_conjuct(cnf, attack.first + delta * i, conj);			
			sat_connections[attack.first + delta * i] = attack.second;
		}
	}
}

void security_system::encode_cnf_base_for_patch()
{
	for (int i = 1; i <= steps; ++i) {
		std::map<int, int> and_to_or;
		for (auto& pcond : postcond_to_attack_num) {
			std::vector<int> disj;
			for (auto& k : pcond.second) {
				disj.push_back(k + delta * i);
			}
			encode_disjunct(cnf, pcond.first + delta * i, disj);
			sat_connections[pcond.first + delta * (i - 1)] = disj;

			encode_disjunct(cnf, scissoring_nums[pcond.first][i], { scissoring_nums[pcond.first][i - 1],  pcond.first + delta * (i - 1) });
		}
		for (auto& attack : attack_to_facts_num) {
			std::vector<int> conj = attack.second;

			if (i > 1) {
				for (auto& el : conj) {
					if (el > start_postconditions) {
						el = scissoring_nums[el][i];
					}
				}
			}

			//conj.push_back(-attacks_scissoring_nums[attack.first][i]);
			//conj.push_back(-and_to_or[attack.first + delta * (i-1)]);
			encode_conjuct(cnf, attack.first + delta * i, conj);
			/*std::vector<int> tmp = { attacks_scissoring_nums[attack.first][i - 1],  and_to_or[attack.first + delta * i] };
			encode_disjunct(cnf, attacks_scissoring_nums[attack.first][i], tmp);*/
			sat_connections[attack.first + delta * i] = attack.second;
		}
	}
}

void security_system::encode_cnf_iterable()
{
	cnf.clear();
	sat_assignments.clear();	
	
	encode_cnf_base();
	for (auto& f : facts_num) {
		cnf.push_back({ f });
	}
}

void security_system::encode_cnf_for_patch(std::vector<int> attacks_to_block, int n, int m/*=0*/)
{
	if (r.size() == 0) {
		cnf.clear();
		patch_variables.clear();
		sat_assignments.clear();
		encode_cnf_base_for_patch();
		for (auto& f : facts_num) {
			if (std::find(patch_variables_to_sort.begin(), patch_variables_to_sort.end(), f) != patch_variables_to_sort.end()) {
				int tmp_variable = ++nVars; // �������������� ���������� ��� �����
				cnf.push_back({ f, tmp_variable });
				cnf.push_back({ -f, -tmp_variable });
				patch_variables.push_back(tmp_variable);
			}
			else {
				cnf.push_back({ f });
			}
			//cnf.push_back({ f });
		}
		for (auto& attack : attacks_to_block) {
			for (int i = 0; i <= steps; ++i) {
				cnf.push_back({ -(attack + delta * i) });
			}
		}
	
			if (m == 0) {
				r = CardinalityNetwork::Notmoreatstart(n, patch_variables, nVars, cnf);
			}
			else {
				r = CardinalityNetwork::Notmoreatstart(n, m, patch_variables, nVars, cnf);
			}
	}
	else {
		cnf.pop_back();
		cnf.pop_back();
		cnf.push_back({ r[n - 1] });
		cnf.push_back({ -r[n] });
	}
	
	
}

void security_system::write_cnf(std::string filename)
{
	std::ofstream file(filename.c_str(), std::ios_base::out);
	if (file.is_open()) {
		file << "p cnf " << nVars << " " << cnf.size() << std::endl;
		for (const auto& con : cnf) {
			for (const auto& lit : con) {
				file << lit << " ";
			}
			file << "0" << std::endl;
		}
	}
	file.close();
}

void security_system::write_gv_file_iterable(std::string output_file, sql_wrapper^ wrapper)
{
	std::map<int, std::string> all_names = get_names_from_table(wrapper);

	std::ofstream myfile;
	myfile.open(output_file.c_str());
	//fopen_s(&f, filne_path.c_str(), "w+");
	std::set<int> verts;
	std::map<int, std::vector<int>> verts_ranks;
	std::map<int, int> verts_to_step, and_verts;
	myfile << "strict digraph{ \n rankdir = TB;\n";
	std::map<int, int> all_connections;

	int size = assumptions.size();

	for (int i = 1; i < size; ++i) {
		for (auto& lit : assumptions[i]) {
			if (lit > 0 && (sat_connections.find(lit) != sat_connections.end())) {
				verts.insert(lit);
				if (verts_to_step.find(lit) == verts_to_step.end()) {
					verts_to_step[lit] = i;

					if (lit > start_postconditions && lit <= start_attacks) { verts_ranks[i].push_back(lit); }

					for (auto& v : sat_connections[lit]) {
						if (sat_assignments[i - 1][v - 1] > 0) {

							myfile << v << "->" << lit << ";\n";
							and_verts[v] = i;
							verts_to_step[v] = i;
						}
					}

				}
			}
		}
	}

	for (auto& and_vert : and_verts) {
		for (auto& v : sat_connections[and_vert.first]) {
			if (sat_assignments[and_vert.second - 1][and_vert.first - 1] > 0) {
				myfile << v << "->" << and_vert.first << ";\n";
				verts_to_step[v] = and_vert.second - 1;
				verts.insert(v);
				verts.insert(and_vert.first);
			}
		}
	}

	for (auto& vert : verts) {
		myfile << vert << "[label = \"";
		if (vert <= start_postconditions) {
			myfile << all_names[vert] << "[0]\" ,shape = rectangle";
		}
		else if (vert > start_postconditions && vert <= start_attacks) {
			myfile << all_names[vert] << "[" << verts_to_step[vert] << "]\" ,shape = diamond";
		}
		else if (vert > start_attacks) {
			myfile << all_names[vert - delta] << "[" << verts_to_step[vert] << "]\" ,shape = oval";
		}
		myfile << "];" << std::endl;
	}

	for (auto& r : verts_ranks) {
		myfile << "{rank = same; ";
		for (auto& v : r.second) {
			myfile << v << "; ";
		}
		myfile << "}\n";
	}

	myfile << " \n}";
	myfile.close();
}

void security_system::write_gv_file_patch(std::string output_file, sql_wrapper^ wrapper)
{
	// work in progress
	std::map<int, std::string> all_names = get_names_from_table(wrapper);

	std::ofstream myfile;
	myfile.open(output_file.c_str());
	//fopen_s(&f, filne_path.c_str(), "w+");
	std::set<int> verts;
	std::map<int, std::vector<int>> verts_ranks;
	std::map<int, int> verts_to_step, and_verts;
	myfile << "strict digraph{ \n rankdir = TB;\n";
	std::map<int, int> all_connections;

	int size = steps;
	std::vector<int> tmp;
	for (auto i: tmp_vector_assumption_patch)
	{
		for (auto j : i) {
			tmp.push_back(j);
		}
	}

	std::ofstream myfile_tmp;
	myfile_tmp.open("patch_name.txt");
	for (int i = 0; i < start_postconditions; ++i) {
		if (sat_assignments[0][i] < 0) {
			myfile_tmp << all_names[i] << std::endl;
		}
	}
	myfile_tmp.close();

	for (int i = 1; i <= size; ++i) {
		for (auto lit : tmp) {
			lit = -lit;
			int el = lit;
			lit += delta * i;
			if ((sat_connections.find(lit) != sat_connections.end()) && sat_assignments[0][lit-1]>0) {
				verts.insert(el);
				if (verts_to_step.find(el) == verts_to_step.end()) {
					verts_to_step[el] = i;

					if (el > start_postconditions && el <= start_attacks) { verts_ranks[i].push_back(el); }

					int lit_tmp = lit - delta;
					for (auto& v : sat_connections[lit_tmp]) {
						if (sat_assignments[0][v - 1] > 0) {
							int vv = v - delta * (i - 1);
							myfile << vv << "->" << el << ";\n";
							and_verts[v] = i;
							verts_to_step[vv] = i;
						}
					}
				}
			}
		}
	}

	for (auto& and_vert : and_verts) {
		if (sat_assignments[0][and_vert.first - 1] > 0) {
			for (auto& v : sat_connections[and_vert.first]) {
			
				int vv = v;
				if (vv > start_attacks) {
					while (std::find(tmp.begin(), tmp.end(), vv ) == tmp.end()) {
						vv -= delta;
					}
				}
				myfile << vv << "->" << and_vert.first - delta*(and_vert.second-1) << ";\n";
				
				verts_to_step[vv] = and_vert.second - 1;
				verts.insert(vv);
				verts.insert(and_vert.first - delta * (and_vert.second - 1));
			}
		}
	}

	for (auto& vert : verts) {
		myfile << vert << "[label = \"";
		if (vert <= start_postconditions) {
			myfile << all_names[vert] << "[0]\" ,shape = rectangle";
		}
		else if (vert > start_postconditions && vert <= start_attacks) {
			myfile << all_names[vert] << "[" << verts_to_step[vert] << "]\" ,shape = diamond";
		}
		else if (vert > start_attacks) {
			myfile << all_names[vert - delta] << "[" << verts_to_step[vert] << "]\" ,shape = oval";
		}
		myfile << "];" << std::endl;
	}

	for (auto& r : verts_ranks) {
		myfile << "{rank = same; ";
		for (auto& v : r.second) {
			myfile << v << "; ";
		}
		myfile << "}\n";
	}

	myfile << " \n}";
	myfile.close();
}

void security_system::enumerate_attacks_by_steps() //������������� �������
{
	if (steps_changed || !is_processed) {
		int scissoring_num = end_attack + delta * steps;
		scissoring_nums.clear();
		assumptions_num.clear();
		assumptions.clear();
		tmp_vector_assumption_patch.clear();
		for (int i = 1; i <= steps; ++i) {
			for (auto& postcond : postcond_to_attack_num) {
				assumptions_num[postcond.first + delta * (i - 1)] = postcond.first + delta * i;
				if (i == 1) {
					tmp_vector_assumption_patch.push_back({ -postcond.first });
					scissoring_nums[postcond.first].push_back(++scissoring_num);
				}
				scissoring_nums[postcond.first].push_back(++scissoring_num);
				assumptions_num[scissoring_nums[postcond.first][i - 1]] = scissoring_nums[postcond.first][i];
			}
			if (current_scissoring_mode == attacks_scissoring) {
				for (auto& attack : attack_to_facts_num) {
					if (i == 1) {
						scissoring_nums[attack.first].push_back(++scissoring_num); // ����� ��� ����� � ������� ������ �������
					}
					scissoring_nums[attack.first].push_back(++scissoring_num);
					assumptions_num[scissoring_nums[attack.first][i - 1]] = scissoring_nums[attack.first][i];
					assumptions_num[attack.first + delta * (i - 1)] = attack.first + delta * i;
					if (i == 1) {
						tmp_vector_assumption_patch.push_back({ -scissoring_nums[attack.first][0] });
					}
				}
			}
		}


		facts_num.clear();
		for (int i = 1; i <= start_postconditions; ++i) {
			facts_num.insert(i);
		}
		nVars = scissoring_num;

		std::vector<int> assumption;
		for (auto& assump : assumptions_num) {
			assumption.push_back(-assump.first);
		}
		assumptions.push_back(assumption);
		steps_changed = false;
	}
}

void security_system::run_solver_iterable(double& time)
{
	minisat22_wrapper wrp;
	Minisat::Problem p = wrp.convert_to_problem(cnf);
	if (assumptions.size() > 1) {
		assumptions.erase(assumptions.begin() + 1, assumptions.end());
	}
	double initial_time = cpuTime();

	Solver S;
	S.verbosity = 0;
	S.addProblem_limited(p, nVars);

	S.print_learnts = true;

	int k = 0;
	while (k<assumptions.size()) {
		vec<Lit> local_assumptions;
		std::vector<int> current_ss;
		std::vector<int> tmp;
		wrp.convertClause(assumptions[k], local_assumptions);
		k++;

		lbool ret = S.solveLimited(local_assumptions);

		if (ret == l_True) {
			for (int i = 0; i < S.nVars(); ++i) {
				int val = (S.model[i] == l_True) ? (i + 1) : -(i + 1);
				current_ss.push_back(val);
			}
		
			for (auto& assump : assumptions_num) {
				if (current_ss[assump.second-1] > 0) {
					tmp.push_back(assump.first);
				}
				else {
					tmp.push_back(-assump.first);
				}
			}

			if (assumptions[assumptions.size() - 1] == tmp) {
				//assumptions.pop_back();
				break;
			}
			assumptions.push_back(tmp);
			sat_assignments.push_back(current_ss);
		}
	}
	for (auto& i : p) {
		delete i;
		i = NULL;
	}
	p.clear();
	time = cpuTime() - initial_time;
}

void security_system::run_solver_single(double& time)
{
	minisat22_wrapper wrp;
	Minisat::Problem p = wrp.convert_to_problem(cnf);

	double initial_time = cpuTime();

	Solver S;
	S.verbosity = 0;
	S.addProblem(p);

	S.print_learnts = true;

	vec<Lit> dummy;
	lbool ret = S.solveLimited(dummy);
	

	if (ret == l_True) {
		std::vector<int> current_ss;
			for (int i = 0; i < S.nVars(); ++i) {
				int val = (S.model[i] == l_True) ? (i + 1) : -(i + 1);
					current_ss.push_back(val);
			}
		sat_assignments.push_back(current_ss);
	}
	for (auto& i : p) {
		delete i;
		i = NULL;
	}
	p.clear();
	time = cpuTime() - initial_time;
}

void security_system::run_solver_single_for_patch(double& time, Problem &p, int& n, int m/*=0*/, double timeout/*=0*/)
{
	minisat22_wrapper wrp;
	double initial_time = cpuTime();

	
	while (n < patch_variables_to_sort.size()-1) {
		Solver S;
		S.verbosity = 0;
		S.max_solving_time = timeout;
		S.addProblem(p);

		S.print_learnts = true;

		vec<Lit> dummy;
		lbool ret = S.solveLimited(dummy);

		if (ret == l_True) {
			std::vector<int> current_ss;
			for (int i = 0; i < S.nVars(); ++i) {
				int val = (S.model[i] == l_True) ? (i + 1) : -(i + 1);
				current_ss.push_back(val);
			}
			sat_assignments.push_back(current_ss);
			break;
		}
		auto it = p.end();
		it--;
		delete *it;
		*it = NULL;
		it--;
		delete* it;
		*it = NULL;
		p.pop_back();
		p.pop_back();

		n++;
		minisat22_wrapper wrp;
		Minisat::Disjunct *lits = new Minisat::Disjunct;
		wrp.convertClause(std::vector<int>{ r[n - 1] }, *lits);
		p.push_back(lits);

		lits = new Minisat::Disjunct;
		wrp.convertClause(std::vector<int>{ -r[n] }, *lits);
		p.push_back(lits);
	}
	time = cpuTime() - initial_time;
}

void security_system::encode_conjuct(std::vector<std::vector<int>>& local_cnf, int v1, std::vector<int> conjunct)
{
	std::vector<int> tmp;
	tmp.push_back(v1);
	for (auto& lit : conjunct) {
		tmp.push_back(-lit);
	}
	local_cnf.push_back(tmp);
	for (auto& lit : conjunct) {
		tmp = { -v1, lit };
		local_cnf.push_back(tmp);
	}
}

void security_system::encode_disjunct(std::vector<std::vector<int>>& local_cnf, int v1, std::vector<int> disjunct)
{
	std::vector<int> tmp;
	tmp.push_back(-v1);
	for (auto& lit : disjunct) {
		tmp.push_back(lit);
	}
	local_cnf.push_back(tmp);
	for (auto& lit : disjunct) {
		tmp = { v1, -lit };
		local_cnf.push_back(tmp);
	}
}

void security_system::create_attack_tables(std::vector<std::string>& attack_names, sql_wrapper^ wrapper)
{
	attack_names.clear();
	for (auto& attack : postconditions_template) {
		attack_names.push_back(attack.first);
		wrapper->create_table(attack.first, attack.second.return_columns(false), attack.second.return_additional_columns(),true);
	}
	for (auto& attack_n : attacks_template) {
		attack_names.push_back(attack_n.first);
		wrapper->create_table(attack_n.first, attack_n.second.return_columns(), attack_n.second.return_additional_columns(),true);
	}
}

void security_system::drop_attack_tables(std::vector<std::string> attack_names, sql_wrapper^ wrapper)
{
	for (auto& name : attack_names) {
		wrapper->drop_table(name, true);
	}
	attack_names.clear();
	is_processed = false;
}


void security_system::fill_tables_for_postcondition(sql_wrapper^ wrapper)
{
	for (auto& attack : postconditions_template) {
		for (auto& f : fact_template) {
			if (attack.second.is_active()) {
				std::vector<std::vector<std::string>> r = f.second.return_values_by_signature(attack.second.return_signature());
				if (r.size() > 0) {
					for (auto& el : r) {
						wrapper->fill_table_for_postcondition(attack.second.return_name(), attack.second.return_columns(false), f.second.return_name(), el);
					}
				}
			}
		}
	}
}

void security_system::drop_tables_for_postconditions(sql_wrapper^ wrapper)
{
	for (auto& attack : postconditions_template) {
		wrapper->drop_table(attack.first);
	}
}

void security_system::process_enumeration(sql_wrapper^ wrapper)
{
	AG.reset();
	if (!is_processed || test_calc) {
		wrapper->detach_memory_database();
		wrapper->attach_memory_database();
		//drop_attack_tables(attack_names, wrapper);
		create_attack_tables(attack_names, wrapper);
		enumerate(wrapper);	
		
		set_proceed(true);
	}
	else if (is_processed) {
		enumerate_attacks_by_steps();
	}
}

void security_system::set_proceed(bool value)
{
	is_processed = value;
}



void security_system::proceed_with_steps_eq_one(sql_wrapper^ wrapper)
{
	int current_steps = 1;
	if (steps > 1) {
		current_steps = steps;
		set_steps(1);
	}
	process_enumeration(wrapper);
	if (current_steps > 1) {
		set_steps(current_steps);
	}
}

bool security_system::is_name_exists(std::string name)
{
	if (fact_template.find(name) == fact_template.end() ||
		postconditions_template.find(name) == postconditions_template.end() ||
		attacks_template.find(name) == attacks_template.end()) {
		return false;
	}
	return true;
}

std::map<int, std::string> security_system::get_names_from_table(sql_wrapper^ wrapper)
{
	wrapper->create_table("names_to_nums", { "name",  "num" }, { }, true);
	for (auto& f : fact_template) {
		wrapper->fill_names(f.second.return_name(), f.second.return_columns(false));
		wrapper->fill_table_for_graphviz("names_to_nums", { "name", "num" }, f.first, { "name", "num" });
	}
	for (auto& attack : postconditions_template) {
		wrapper->fill_names(attack.second.return_name(), attack.second.return_columns(false));
		wrapper->fill_table_for_graphviz("names_to_nums", { "name", "num" }, attack.first, { "name", "num" });
	}
	for (auto& attack1 : attacks_template) {
		wrapper->fill_names(attack1.second.return_name(), attack1.second.return_columns(false));
		wrapper->fill_table_for_graphviz("names_to_nums", { "name", "num" }, attack1.first, { "name", "num" });
	}

	std::map<int, std::string> all_names;
	std::vector<std::map<std::string, std::string>> resp = wrapper->select_from_table("names_to_nums", { "name", "num" }, {}, true);
	for (auto& row : resp) {
		all_names[std::atoi(row["num"].c_str())] = row["name"];
	}

	wrapper->drop_table("names_to_nums", true);
	return all_names;
}

void security_system::get_names_from_table(sql_wrapper^ wrapper, std::map<std::string, int> &facts, std::map<std::string, int> &attacks)
{
	process_enumeration(wrapper);

	wrapper->create_table("names_to_nums", { "name",  "num" }, { }, true);
	for (auto& f : fact_template) {
		wrapper->fill_names(f.second.return_name(), f.second.return_columns(false));
		wrapper->fill_table_for_graphviz("names_to_nums", { "name", "num" }, f.first, { "name", "num" });
	}
	std::vector<std::map<std::string, std::string>> resp = wrapper->select_from_table("names_to_nums", { "name", "num" }, {}, true);
	for (auto& row : resp) {
		facts[row["name"]] = std::atoi(row["num"].c_str());
	}
	wrapper->drop_table("names_to_nums", true);

	wrapper->create_table("names_to_nums", { "name",  "num" }, { }, true);
	fill_tables_for_postcondition(wrapper);

	for (auto& attack : postconditions_template) {
		wrapper->fill_names(attack.second.return_name(), attack.second.return_columns(false));
		wrapper->fill_table_for_graphviz("names_to_nums", { "name", "num" }, attack.first, { "name", "num" });
	}
	
	resp = wrapper->select_from_table("names_to_nums", { "name", "num" }, {}, true);
	for (auto& row : resp) {
		attacks[row["name"]] = std::atoi(row["num"].c_str());
	}
	wrapper->drop_table("names_to_nums", true);
	drop_attack_tables(attack_names, wrapper);

}

std::vector<int> security_system::get_patches()
{
	std::vector<int> res;
	for (int i = 0; i < start_postconditions; i++) {
		if (sat_assignments[0][i] < 0) {
			res.push_back(i+1);
		}
	}
	return res;
}

int security_system::get_numbers_to_patch(std::vector<int>& attacks_to_patch, std::vector<int> &facts_to_patch, sql_wrapper^ wrapper)
{
	wrapper->create_table("names_to_nums", { "name",  "num" }, { }, true);
	for (auto& f : fact_template) {
		wrapper->fill_names(f.second.return_name(), f.second.return_columns(false));
		wrapper->fill_table_for_graphviz("names_to_nums", { "name", "num" }, f.first, { "name", "num" });
	}
	for (auto& attack : postconditions_template) {
		wrapper->fill_names(attack.second.return_name(), attack.second.return_columns(false));
		wrapper->fill_table_for_graphviz("names_to_nums", { "name", "num" }, attack.first, { "name", "num" });
	}
	for (auto& attack1 : attacks_template) {
		wrapper->fill_names(attack1.second.return_name(), attack1.second.return_columns(false));
		wrapper->fill_table_for_graphviz("names_to_nums", { "name", "num" }, attack1.first, { "name", "num" });
	}

	std::map<int, std::string> all_names;
	std::vector<std::map<std::string, std::string>> resp = wrapper->select_from_table("names_to_nums", { "name", "num" }, {}, true);
	for (auto& row : resp) {
		all_names[std::atoi(row["num"].c_str())] = row["name"];
	}

	wrapper->drop_table("names_to_nums");

	std::set<int> verts;
	std::map<int, std::vector<int>> verts_ranks;
	std::map<int, int> verts_to_step;
	std::set<std::string> hosts_to_block;
	int size = values.size()-1;

	for (int i = 1; i < size; ++i) {
		for (auto& l : assumptions[0]) {
			auto lit = -l;
			if (values[i][lit] && (postcond_to_attack_num.find(lit) != postcond_to_attack_num.end())) {
				verts.insert(lit);
				if (verts_to_step.find(lit) == verts_to_step.end()) {
					verts_to_step[lit] = i;
					if (lit > start_postconditions && lit <= start_attacks) { verts_ranks[i].push_back(lit); }
				}
			}
		}
	}
	std::vector<std::pair<int, std::string>> names;
	std::string tmp = "", host_1 = "", host_2 = "";
	if (verts_ranks.size() > 2) {
		for (int i = size; i >= int(size / 2); --i) {
			for (auto& j : verts_ranks[i]) {
				if (all_names[j].c_str()[0] == 'e' && all_names[j].c_str()[all_names[j].size()-2] == 't') {
					std::string a1 = all_names[j];
					auto start = a1.find('(');
					auto end = a1.find(',');
					tmp = a1.substr(start + 1, end - start - 1);
					names.push_back(std::make_pair(j, tmp));
				}
			}
		}
		if (names.size() > 1) {
			std::default_random_engine generator;
			int len = names.size() - 1;
			std::uniform_int_distribution<int> distribution(0, len);
			int index_1 = distribution(generator);
			int index_2 = distribution(generator);
			while (index_2 == index_1) {
				index_2 = distribution(generator);
			}
			host_1 = names[index_1].second;
			host_2 = names[index_2].second;
			attacks_to_patch.push_back(names[index_1].first);
			attacks_to_patch.push_back(names[index_2].first);
		}
		else {
			host_1 = names[0].second;
			attacks_to_patch.push_back(names[0].first);
		}

		for (int i = 1; i < 3; ++i) {
			for (auto& j : verts_ranks[i]) {
				std::string a1 = all_names[j];
				auto start = a1.find('(');
				auto end = a1.find(',');
				std::string tmp = a1.substr(start + 1, end - start - 1);
				if (tmp != host_1 && tmp != host_2) {
					hosts_to_block.insert(tmp);
				}
			}
		}


		wrapper->return_numbers_to_patch(host_1, host_2, facts_to_patch, hosts_to_block);
	}
	else {
		size = 1;
	}
	return size;
}

void security_system::generate_ag_iterable(sql_wrapper^ wrapper)
{
	std::map<int, std::string> all_names = get_names_from_table(wrapper);

	std::set<int> verts;
	std::map<int, std::vector<int>> verts_ranks;
	std::map<int, int> verts_to_step, and_verts;
	std::vector<std::pair<int, int>> all_arcs;

	int size = assumptions.size();

	for (int i = 1; i < size; ++i) {
		for (auto& lit : assumptions[i]) {
			if (lit > 0 && (sat_connections.find(lit) != sat_connections.end())) {
				verts.insert(lit);
				if (verts_to_step.find(lit) == verts_to_step.end()) {
					verts_to_step[lit] = i;

					if (lit > start_postconditions && lit <= start_attacks) { verts_ranks[i].push_back(lit); }

					for (auto& v : sat_connections[lit]) {
						if (sat_assignments[i - 1][v - 1] > 0) {
							all_arcs.push_back(std::make_pair(v,lit));
							and_verts[v] = i;
							verts_to_step[v] = i;
						}
					}
				}
			}
		}
	}

	for (auto& and_vert : and_verts) {
		for (auto& v : sat_connections[and_vert.first]) {
			if (sat_assignments[and_vert.second - 1][and_vert.first - 1] > 0) {
				all_arcs.push_back(std::make_pair(v, and_vert.first));
				verts_to_step[v] = and_vert.second - 1;
				verts.insert(v);
				verts.insert(and_vert.first);
			}
		}
	}

	for (auto& v : verts) {
		std::string shape = "";
		int vv = v;
		if (v <= start_postconditions) {
			shape = "rectangle";
			verts_to_step[v] = 0;
		}
		else if (v > start_postconditions && v <= start_attacks) {
			shape = "diamond";
			
		}
		else {
			shape = "oval";
			vv = v - delta;
		}
		AG.add_vert(v, all_names[vv], shape, verts_to_step[v]);
	}

	for (auto& arc : all_arcs) {
		AG.add_arc(arc.first, arc.second);
	}

	for (auto& rank : verts_ranks) {
		for (auto& vert : rank.second) {
			AG.add_rank(vert, rank.first);
		}
	}
}

void security_system::generate_ag_plus(sql_wrapper^ wrapper)
{
	std::map<int, std::string> all_names = get_names_from_table(wrapper);

	std::ofstream myfile;
	std::set<int> verts;
	std::map<int, std::vector<int>> verts_ranks;
	std::map<int, int> verts_to_step, and_verts;
	std::vector<std::pair<int, int>> all_arcs;

	int size = values.size();

	for (int i = 1; i < size; ++i) {
		for (auto& l : assumptions[0]) {
			auto lit = -l;
			if (values[i][lit] && (postcond_to_attack_num.find(lit) != postcond_to_attack_num.end())) {
				verts.insert(lit);
				if (verts_to_step.find(lit) == verts_to_step.end()) {
					verts_to_step[lit] = i;

					if (lit > start_postconditions && lit <= start_attacks) { verts_ranks[i].push_back(lit); }

					for (auto& v : postcond_to_attack_num[lit]) {
						if (values[i][v]) {
							all_arcs.push_back(std::make_pair(v, lit));
							and_verts[v] = i;
							verts_to_step[v] = i;
						}
					}

				}
			}
		}
	}

	for (auto& and_vert : and_verts) {
		for (auto& v : attack_to_facts_num[and_vert.first]) {
			if (values[and_vert.second][and_vert.first]) {
				all_arcs.push_back(std::make_pair(v, and_vert.first));
				verts_to_step[v] = and_vert.second - 1;
				verts.insert(v);
				verts.insert(and_vert.first);
			}
		}
	}

	for (auto& v : verts) {
		std::string shape = "";
		if (v <= start_postconditions) {
			shape = "rectangle";
			verts_to_step[v] = 0;
		}
		else if (v > start_postconditions && v <= start_attacks) {
			shape = "diamond";
		}
		else {
			shape = "oval";
		}
		AG.add_vert(v, all_names[v], shape, verts_to_step[v]);
	}

	for (auto& arc : all_arcs) {
		AG.add_arc(arc.first, arc.second);
	}

	for (auto& rank : verts_ranks) {
		for (auto& vert : rank.second) {
			AG.add_rank(vert, rank.first);
		}
	}
}

void security_system::generate_ag_patch(sql_wrapper^ wrapper)
{
	// work in progress
	std::map<int, std::string> all_names = get_names_from_table(wrapper);


	std::set<int> verts;
	std::map<int, std::vector<int>> verts_ranks;
	std::map<int, int> verts_to_step, and_verts;
	std::vector<std::pair<int, int>> all_arcs;

	int size = steps;
	std::vector<int> tmp;
	for (auto i : tmp_vector_assumption_patch)
	{
		for (auto j : i) {
			tmp.push_back(j);
		}
	}

	std::ofstream myfile_tmp;
	myfile_tmp.open("patch_name.txt");
	for (int i = 0; i < start_postconditions; ++i) {
		if (sat_assignments[0][i] < 0) {
			myfile_tmp << all_names[i] << std::endl;
		}
	}
	myfile_tmp.close();

	for (int i = 1; i <= size; ++i) {
		for (auto lit : tmp) {
			lit = -lit;
			int el = lit;
			lit += delta * i;
			if ((sat_connections.find(lit) != sat_connections.end()) && sat_assignments[0][lit - 1] > 0) {
				verts.insert(el);
				if (verts_to_step.find(el) == verts_to_step.end()) {
					verts_to_step[el] = i;

					if (el > start_postconditions && el <= start_attacks) { verts_ranks[i].push_back(el); }

					int lit_tmp = lit - delta;
					for (auto& v : sat_connections[lit_tmp]) {
						if (sat_assignments[0][v - 1] > 0) {
							int vv = v - delta * (i - 1);
							all_arcs.push_back(std::make_pair(vv, el));
							and_verts[v] = i;
							verts_to_step[vv] = i;
						}
					}
				}
			}
		}
	}

	for (auto& and_vert : and_verts) {
		if (sat_assignments[0][and_vert.first - 1] > 0) {
			for (auto& v : sat_connections[and_vert.first]) {

				int vv = v;
				if (vv > start_attacks) {
					while (std::find(tmp.begin(), tmp.end(), vv) == tmp.end()) {
						vv -= delta;
					}
				}
				all_arcs.push_back(std::make_pair(vv, and_vert.first - delta * (and_vert.second - 1)));
				verts_to_step[vv] = and_vert.second - 1;
				verts.insert(vv);
				verts.insert(and_vert.first - delta * (and_vert.second - 1));
			}
		}
	}


	for (auto& v : verts) {
		std::string shape = "";
		int vv = v;
		if (v <= start_postconditions) {
			shape = "rectangle";
			verts_to_step[v] = 0;
		}
		else if (v > start_postconditions && v <= start_attacks) {
			shape = "diamond";
		}
		else {
			shape = "oval";
			vv = v - delta;
		}
		AG.add_vert(v, all_names[vv], shape, verts_to_step[v]);
	}

	for (auto& arc : all_arcs) {
		AG.add_arc(arc.first, arc.second);
	}

	for (auto& rank : verts_ranks) {
		for (auto& vert : rank.second) {
			AG.add_rank(vert, rank.first);
		}
	}
}

void security_system::save_gv_file(std::string filename)
{
	AG.write_gv_graph(filename);
}

void security_system::write_gv_file(std::string& st)
{
	st = AG.gv_string().str();
}

void security_system::drop_all_tables(sql_wrapper^ wrapper)
{
	for (auto& attack : postconditions_template) {
		wrapper->drop_table(attack.first);
	}
	for (auto& fact : fact_template) {
		wrapper->drop_table(fact.first);
	}
	for (auto& name : attack_names) {
		wrapper->drop_table(name);
	}
}

void security_system::enumerate_postconditions(sql_wrapper^ wrapper)
{
	postconditions.clear();
	for (auto& postcond : postconditions_template) {
		for (auto& f : fact_template) {
			if (postcond.second.is_active()) {
				std::vector<std::vector<std::string>> r = f.second.return_values_by_signature(postcond.second.return_signature());
				if ( r.size() > 0 ) {
					for (auto& el : r) {
						wrapper->fill_table_for_postcondition(postcond.second.return_name(), postcond.second.return_columns(false), f.second.return_name(), el);
					}
				}
			}
		}
		std::map<std::string, std::string> k_v;
		wrapper->enumerate_table(postcond.second.return_name(), nVars, true);
		std::vector<std::map<std::string, std::string>> postcond_values = wrapper->select_from_table(postcond.second.return_name(), postcond.second.return_columns(), k_v, true);
		
		for (auto& row : postcond_values) {
			signs s = postcond.second.return_signature();
			int len = s.size();
			
			for (int i = 0; i < len;++i) {
				s[i].first.set_value(row[s[i].first.return_name()]);
			}			
			fact pcond(postcond.second.return_name(), s);
			pcond.add_num(std::atoi(row["num"].c_str()));

			postconditions[pcond.return_name()].push_back(pcond);

		}			
	}
}

void security_system::enumerate_attacks(sql_wrapper^ db)
{
	for (auto& attack : attacks_template) {
		db->fill_table_for_attack(attack.second.return_name(), attack.second.return_all_columns(), attack.second.return_variables_by_facts(), attack.second.return_consts_by_facts(), attack.second.return_pcond());
		db->enumerate_table(attack.second.return_name(), nVars, true);		
	}

	postcond_to_attack_num.clear();
	attack_to_facts_num.clear();
	for (auto& v_attack : postconditions) {
		for (auto& or_attack : v_attack.second) {
			for (auto and_attack : postcond_to_attacks_template[or_attack.return_name()]){
				std::map<int, std::vector<int>> conjunct;
				std::vector<int> a = and_attack.return_numbers_by_signature(or_attack.return_signature(), db, conjunct);
				postcond_to_attack_num[or_attack.return_num(0)].insert(postcond_to_attack_num[or_attack.return_num(0)].begin(), a.begin(), a.end());
				for (auto& el : conjunct) {
					attack_to_facts_num[el.first].insert(attack_to_facts_num[el.first].end(), el.second.begin(), el.second.end());
				}
			}
		}
	}	
}