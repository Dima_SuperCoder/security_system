#pragma once
#include <string>
#include <vector>
#include "my_types.h"
#include "fact.h"

#include "sql_wrapper.h"

class attack_rule: public fact
{
public:
	attack_rule() {};
	attack_rule(std::string attack_name, signs sign, std::vector<fact> f);

	void set_description(std::string desc) { description = desc; }
	void set_pcond(std::string attack) { postcondition = attack; }
	std::vector<std::string> return_columns(bool with_nums = true) const override;

	std::map<std::string, std::vector<std::pair<std::string, std::string>>> return_variables_by_facts() const;
	std::map<std::string, std::vector<std::pair<std::string, std::string>>> return_consts_by_facts() const;
	std::vector<int> return_numbers_by_signature(signs or_attack_signature, sql_wrapper^ wrapper, std::map<int, std::vector<int>>& and_vector);

	std::vector<fact> return_facts() { return facts; }
	std::string return_pcond() { return postcondition; }
	std::map<std::string, std::vector<std::string>> return_all_columns();

	bool is_contain_fact(std::string fact_name);

	bool operator==(const attack_rule& r1) const { return name == r1.name && signature == r1.signature; }
	bool operator<(const attack_rule& r1) const;

protected:

	std::string description;
	std::string postcondition;
	std::vector<fact> facts;
	std::map<std::string, std::shared_ptr<std::string>> variables, constants;
	std::map<std::string, std::string> values;
	std::vector<std::string> get_columns() const;

	bool active = true;

	mutable std::vector<int> num;
	mutable std::vector<int> facts_num;

};

