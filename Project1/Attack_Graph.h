#pragma once
#include <map>
#include <memory>
#include <string>
#include <vector>
#include <sstream>

struct AG_vert {
	std::string name;
	int number;
	std::string shape;
	int rank;
	std::vector<std::shared_ptr<AG_vert>> in_verts, out_verts;
};

class Attack_Graph
{
public:
	void add_vert(int number, std::string name, std::string shape, int rank = -1);
	void add_arc(int start_vert, int end_vert);
	void write_gv_graph(std::string filename);
	std::stringstream gv_string();
	void add_rank(int vert_number, int rank);
	void reset();
private:
	std::map<int, std::shared_ptr<AG_vert>> verts;
	std::map<int, std::vector<std::shared_ptr<AG_vert>>> ranks;
	std::vector<std::pair<std::shared_ptr<AG_vert>, std::shared_ptr<AG_vert>>> arcs;
};

