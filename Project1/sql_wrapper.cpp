#include "sql_wrapper.h"
#include "utilities.h"
#include <set>


sql_wrapper::sql_wrapper()
{
	
}

sql_wrapper::~sql_wrapper()
{
	close_connection();
}

sql_wrapper::!sql_wrapper()
{

}

bool sql_wrapper::open_connection(std::string db_path /*= ""*/)
{
	std::string conn;
	if (db_path == "") {
		conn = "Data Source=:memory:";
	}
	else {
		conn = "Data Source=\"" + db_path + "\"";
	}
	db = gcnew SQLiteConnection();
	try {
		db->ConnectionString = gcnew System::String(conn.c_str());
		db->Open();
		return true;
	}
	catch (System::Exception ^e) {
		return false;
	}
	return false;
}

void sql_wrapper::close_connection()
{
	db->Close();
	//delete db;
}

System::Data::SQLite::SQLiteDataAdapter^ sql_wrapper::return_adapter(std::string table_name, std::vector<std::string> columns)
{
	SQLiteCommand ^cmd, ^del_cmd, ^upd_cmd, ^ins_cmd;
	cmd = db->CreateCommand();
	del_cmd = db->CreateCommand();
	upd_cmd = db->CreateCommand();
	ins_cmd = db->CreateCommand();
	std::string statement = "SELECT ";
	
	int len = columns.size();
	if (len > 0) {
		for (int i = 0; i < len; ++i) {
			statement += columns[i];
			
			if (i < len - 1) {
				statement += ", ";
				
			}
		}
	}
	
	statement += " FROM " + secure_string(table_name) + ";";

	cmd->CommandText = gcnew System::String(statement.c_str());
	SQLiteDataAdapter^ ad = gcnew SQLiteDataAdapter(cmd);

	std::string delete_cmd= "DELETE from " + table_name + " WHERE";
	for (int i = 0; i < len; ++i) {
		std::string param_name = "@param" + std::to_string(i + 1);
		System::String^ param_name_sys = gcnew System::String(param_name.c_str());
		delete_cmd += " " + columns[i] + "=" + param_name;
		del_cmd->Parameters->Add(gcnew SQLiteParameter(param_name_sys));
		del_cmd->Parameters[param_name_sys]->SourceColumn = gcnew System::String(columns[i].c_str());
		del_cmd->Parameters[param_name_sys]->SourceVersion = System::Data::DataRowVersion::Original;
		if (i < len - 1) {
			delete_cmd += " AND ";
		}
	}
	del_cmd->CommandText = gcnew System::String(delete_cmd.c_str());
	
	std::string col_param = "(";
	std::string insert_cmd = "INSERT OR REPLACE INTO " + secure_string(table_name) + "(";
	for (int i = 0; i < len; ++i) {
		insert_cmd += columns[i];
		std::string param_name = "@param" + std::to_string(i + 1);
		System::String^ param_name_sys = gcnew System::String(param_name.c_str());
		ins_cmd->Parameters->Add(gcnew SQLiteParameter(param_name_sys));
		ins_cmd->Parameters[param_name_sys]->SourceColumn = gcnew System::String(columns[i].c_str());

		col_param += "@param" + std::to_string(i + 1);
		if (i < len - 1) {
			insert_cmd += ",";
			col_param += ",";
		}
	}
	col_param += ")";
	insert_cmd += ") VALUES " + col_param;
	ins_cmd->CommandText = gcnew System::String(insert_cmd.c_str());

	std::string update_cmd = "UPDATE OR REPLACE " + secure_string(table_name) + " SET ";
	col_param = " WHERE ";
	for (int i = 0; i < len; ++i) {
		std::string param_name = "@param" + std::to_string(i + 1);
		update_cmd += " " + columns[i] + "=" + param_name;

		System::String^ param_name_sys = gcnew System::String(param_name.c_str());

		std::string old_param_name = "@old_param" + std::to_string(i + 1);
		System::String^ old_param_name_sys = gcnew System::String(old_param_name.c_str());
		col_param += " " + columns[i] + "=" + old_param_name;

		upd_cmd->Parameters->Add(gcnew SQLiteParameter(param_name_sys));
		upd_cmd->Parameters[param_name_sys]->SourceColumn = gcnew System::String(columns[i].c_str());

		upd_cmd->Parameters->Add(gcnew SQLiteParameter(old_param_name_sys));
		upd_cmd->Parameters[old_param_name_sys]->SourceColumn = gcnew System::String(columns[i].c_str());
		upd_cmd->Parameters[old_param_name_sys]->SourceVersion = System::Data::DataRowVersion::Original;

		if (i < len - 1) {
			update_cmd += ",";
			col_param += " AND";
		}
	}
	update_cmd += col_param + ";";
	upd_cmd->CommandText = gcnew System::String(update_cmd.c_str());

	ad->DeleteCommand = del_cmd;
	ad->InsertCommand = ins_cmd;
	ad->UpdateCommand = upd_cmd;

	return ad;
}

void sql_wrapper::enumerate_table(std::string table_name, long long int& nV, bool is_temporary)
{
	std::string statement = "UPDATE " + secure_string(table_name) + " SET num=" + secure_string(std::to_string(nV)) + ";";
	statement += "UPDATE " + secure_string(table_name) + " SET num = rowid + num;";
	execute_statement(statement);
	std::vector<std::string> v_to_s;
	v_to_s = { "COUNT(*)" };
	std::map<std::string, std::string> k_v;
	std::vector<std::map<std::string, std::string>> r = select_from_table(table_name, v_to_s, k_v, is_temporary);
	nV += std::atoi(r[0]["COUNT(*)"].c_str());
	
}

void sql_wrapper::fill_table_for_graphviz(std::string dst_table_name, std::vector<std::string> dst_table_columns, std::string src_table_name, std::vector<std::string> src_table_columns)
{
	std::string statement = "INSERT OR IGNORE INTO " + secure_string(dst_table_name) + "(";
	int len = dst_table_columns.size();
	for (int i = 0; i < len; ++i) {
		statement += dst_table_columns[i];
		if (i < len - 1) {
			statement += ", ";
		}
	}
	statement += ") SELECT ";
	for (int i = 0; i < len; ++i) {
		statement += src_table_columns[i];
		if (i < len - 1) {
			statement += ", ";
		}
	}
	statement += " FROM " + secure_string(src_table_name);

	execute_statement(statement);
}

void sql_wrapper::fill_table_for_attack(std::string dst_table_name, std::map<std::string, std::vector<std::string>> dst_table_columns, std::map<std::string, std::vector<std::pair<std::string, std::string>>> params, std::map<std::string, std::vector<std::pair<std::string, std::string>>> consts, std::string or_attack_name)
{
	std::string fill_statement, join_statement, constraints;
	std::vector<std::string> all_constraints;
	std::set <std::string> table_names;

	std::vector <std::string> table_columns = dst_table_columns["vars"];
	table_columns.insert(table_columns.end(), dst_table_columns["num"].begin(), dst_table_columns["num"].end());

	std::string columns = " (";
	join_statement = " SELECT ";
	constraints = " WHERE ";
	int len = params.size();
	int counter = 0;
	for (auto param : params) {
		auto it = std::find(table_columns.begin(), table_columns.end(), param.first);
		if (it != table_columns.end()) {
			columns += secure_string(*it);
			if (counter < len - 1) { columns += ", "; }
			bool flag = false;
			for (auto i : param.second) {
				if (!flag) {
					join_statement += secure_string(i.first) + "." + secure_string(i.second) + " as " + secure_string(*it);
					flag = true;
					if (counter < len - 1) { join_statement += " , "; }
				}
				else {
					all_constraints.push_back(secure_string(i.first) + "." + secure_string(i.second) + "=" + secure_string(params[*it][0].first) + "." + secure_string(params[*it][0].second));
				}
				table_names.insert(i.first);
			}
		}
		else {
			auto col = param.second[0];
			table_names.insert(col.first);
			int size = param.second.size();
			for (int i = 1; i < size; ++i) {
				all_constraints.push_back(secure_string(param.second[i].first) + "." + secure_string(param.second[i].second) + "=" + secure_string(col.first) + "." + secure_string(col.second));
				table_names.insert(param.second[i].first);
			}
		}
		counter++;
	}

	columns += ") ";
	join_statement += " FROM ";
	auto it = table_names.begin();
	auto end_it = table_names.end(); end_it--;
	while (it!= table_names.end()) {
		join_statement += secure_string(it->c_str());		
		if (it != end_it) {
			join_statement += " JOIN ";
		}
		it++;
	}

	
	if (consts.size() > 0) {
		len = consts.size();
		for (auto& val : consts) {		
			int size = val.second.size();
			for (int i = 0; i < size; ++i) {
				if (val.first.c_str()[0] == '-'){
					all_constraints.push_back(secure_string(val.second[i].first) + "." + secure_string(val.second[i].second) + "<>" + secure_string(val.first.substr(1, val.first.size()-1)));
				}
				else {
					all_constraints.push_back(secure_string(val.second[i].first) + "." + secure_string(val.second[i].second) + "=" + secure_string(val.first));
				}
			}
		}
	}

	len = all_constraints.size();
	for (int i = 0; i < len; ++i) {
		constraints += all_constraints[i];
		if (i < len - 1) { constraints += " AND "; }
	}
	if (constraints == " WHERE ") { constraints = ""; }

	fill_statement =  join_statement + constraints;

	if (dst_table_columns["consts"].size() > 0) {
		fill_statement = "CREATE TABLE " +secure_string("tmpTable") + " AS " + fill_statement + ";";


		join_statement = "SELECT ";
		join_statement += secure_string(or_attack_name) + "." + secure_string(dst_table_columns["vars"][0]) + " AS " + secure_string(dst_table_columns["vars"][0]);
		columns = " (" + secure_string(dst_table_columns["vars"][0]);
		constraints = " WHERE ";
		constraints += or_attack_name + "." + dst_table_columns["vars"][0] + " = " + secure_string("tmpTable") + "." + dst_table_columns["vars"][0];
		int size = dst_table_columns["vars"].size();
		for (int i = 1; i < size; ++i) {
			columns += "," + secure_string(dst_table_columns["vars"][i]);
			join_statement += "," + secure_string(or_attack_name) + "." + secure_string(dst_table_columns["vars"][i]) + " AS " + secure_string(dst_table_columns["vars"][i]);
			constraints += " AND " + secure_string(or_attack_name) + "." + secure_string(dst_table_columns["vars"][i]) + " = " + secure_string("tmpTable") + "." + secure_string(dst_table_columns["vars"][i]);
		}
		size = dst_table_columns["consts"].size();
		for (int i = 0; i < size; ++i) {
			columns += "," + secure_string(dst_table_columns["consts"][i]);
			join_statement += "," + secure_string(or_attack_name) + "." + secure_string(dst_table_columns["consts"][i]) + " AS " + secure_string(dst_table_columns["consts"][i]);
			if (dst_table_columns["vals"][i].c_str()[0] == '-') {
				constraints += " AND " + secure_string(or_attack_name) + "." + secure_string(dst_table_columns["consts"][i]) + " <> " + secure_string(dst_table_columns["vals"][i].substr(1, dst_table_columns["vals"][i].size()-1));
			}
			else {
				constraints += " AND " + secure_string(or_attack_name) + "." + secure_string(dst_table_columns["consts"][i]) + " = " + secure_string(dst_table_columns["vals"][i]);
			}
		}
		size = dst_table_columns["num"].size();
		for (int i = 0; i < size; ++i) {
			columns += "," + secure_string(dst_table_columns["num"][i]);
			join_statement += "," + secure_string("tmpTable") + "." + secure_string(dst_table_columns["num"][i]);
		}
		columns += ") ";

		fill_statement = fill_statement + " INSERT OR IGNORE INTO " + secure_string(dst_table_name) + columns + join_statement + " FROM " + secure_string(or_attack_name) + " JOIN " + secure_string("tmpTable") + constraints + "; DROP TABLE " + secure_string("tmpTable") + ";";
	}
	else {
		fill_statement = "INSERT OR IGNORE INTO " + secure_string(dst_table_name) + columns + fill_statement;
	}

	execute_statement(fill_statement);
	
}

void sql_wrapper::fill_table_for_postcondition(std::string dst_table_name, std::vector<std::string> dst_table_columns, std::string src_table_name, std::vector<std::string> src_table_columns)
{
	std::string statement = "INSERT OR IGNORE INTO " + secure_string(dst_table_name) + "(";
	int len = dst_table_columns.size();
	for (int i = 0; i < len; ++i) {
		statement += dst_table_columns[i];
		if (i < len - 1) {
			statement += ", ";
		}
	}
	std::string constraints = "(";
	statement += ") SELECT ";
	for (int i = 0; i < len; ++i) {
		statement += src_table_columns[i];
		constraints += src_table_columns[i] + "<>" + secure_string("*");
		if (i < len - 1) {
			statement += ", ";
			constraints += " AND ";
		}
	}
	constraints += ");";
	statement += " FROM " + secure_string(src_table_name) + " WHERE " + constraints;

	execute_statement(statement);
}

void sql_wrapper::fill_names(std::string table_name, std::vector <std::string> columns)
{
	std::string statement = "UPDATE " + secure_string(table_name) + " SET name="+secure_string(table_name+"(") + " || ";

	int len = columns.size();
	for (int i = 0; i < len; ++i) {
		statement += columns[i];
		if (i < len - 1) {
			statement += " || " + secure_string(", ") + " || ";
		}
	}

	statement += " || " + secure_string(")");

	execute_statement(statement);
}

void sql_wrapper::update_table(std::string table_name, std::vector<std::map<std::string, std::string>> values_to_update, std::vector<std::map<std::string, std::string>> known_values)
{
	std::string statement = "";
	int len = values_to_update.size();
	for (int i = 0; i < len; i++) {
		statement += "UPDATE  " + secure_string(table_name) + " SET ";
		int size = values_to_update[i].size();
		int j = 0;
		for (auto& val : values_to_update[i]) {
			statement += val.first + "=" + secure_string(val.second);
			if (j < size - 1) {
				statement += ", ";
			}
			j++;
		}
		statement += " WHERE ";
		j = 0;
		size = known_values[i].size();
		for (auto& val : known_values[i]) {
			statement += val.first + "=" + secure_string(val.second);
			if (j < size - 1) {
				statement += " AND ";
			}
			j++;
		}
		statement += ";";
	}

	
	execute_statement(statement);
}

void sql_wrapper::create_table(std::string name, std::vector<std::string> colunms, std::vector<std::string> num_columns)
{
	std::string statement = "CREATE TABLE " + secure_string(name);
	std::string constraint = " UNIQUE (";
	statement += "(";
	int len = colunms.size();
	for (int i = 0; i < len;++i) {
		statement += colunms[i] + " NOT NULL ";		
		constraint += colunms[i];
		
		if (i < len - 1) { 		
			statement += ",";
			constraint += ",";
		}
		
	}
	len = num_columns.size();
	statement += ",";
	for (int i = 0; i < len; ++i) {
		statement += num_columns[i];
		statement += ",";
	}
	constraint += ")";
	statement += constraint;
	statement += ");";
	execute_statement(statement);
}

void sql_wrapper::create_table(std::string name, std::vector<std::string> colunms, std::vector<std::string> num_columns, bool is_temporary)
{
	std::string db_name = "main";
	if (is_temporary) { db_name = MarshalString(in_memory_db); }
	std::string statement = "CREATE TABLE " + secure_string(db_name)+"."+secure_string(name);
	std::string constraint = " UNIQUE (";
	statement += "(";
	int len = colunms.size();
	for (int i = 0; i < len; ++i) {
		statement += colunms[i] + " NOT NULL ";
		constraint += colunms[i];

		if (i < len - 1) {
			statement += ",";
			constraint += ",";
		}

	}
	len = num_columns.size();
	statement += ",";
	for (int i = 0; i < len; ++i) {
		statement += num_columns[i];
		statement += ",";
	}
	constraint += ")";
	statement += constraint;
	statement += ");";
	execute_statement(statement);
}

void sql_wrapper::attach_memory_database()
{
	std::string stm = "	ATTACH DATABASE" + secure_string(":memory:") +" AS " + MarshalString(in_memory_db) + " ;";
	execute_statement(stm);
}

void sql_wrapper::detach_memory_database()
{
	std::string stm = "	DETACH DATABASE " + secure_string(MarshalString(in_memory_db)) + " ;";
	execute_statement(stm);
}

void sql_wrapper::add_row(std::string table_name, std::map<std::string, std::string> cols_to_values)
{
	std::string statement = "INSERT INTO " + secure_string(table_name) + "(";
	std::string values_string = "VALUES(";
	int len = cols_to_values.size();
	auto it = cols_to_values.begin();
	int i = 0;
	while ( i < len-1) {
		statement += it->first + ", ";
		values_string += secure_string(it->second) + ", ";
		++it;
		++i;
	}
	values_string += secure_string(it->second) + ");";
	statement += it->first + ") " + values_string;
	execute_statement(statement);
	
}

void sql_wrapper::drop_table(std::string table_name)
{
	std::string statement = "DROP TABLE " + secure_string(table_name) + ";";
	execute_statement(statement);
}

void sql_wrapper::drop_table(std::string table_name, bool is_temporary)
{
	std::string db_name = "main";
	if (is_temporary) { db_name = MarshalString(in_memory_db); }
	std::string statement = "DROP TABLE " + secure_string(db_name) + "." + secure_string(table_name) + ";";
	execute_statement(statement);
}

std::vector<std::map<std::string, std::string>> sql_wrapper::select_from_table(std::string table_name, std::vector<std::string> values_to_select, std::map<std::string, std::string> known_values, bool is_temporary)
{
	std::string statement = "SELECT ";
	int len = values_to_select.size();
	if (len > 0) {
		for (int i = 0; i < len; ++i) {
			statement += values_to_select[i];
			if (i < len - 1) {
				statement += ", ";
			}
		}
	}
	else {
		statement += "*";
	}

	
	statement += " FROM " + secure_string(table_name);

	if (known_values.size() > 0) {
		statement += " WHERE ";
		auto it = known_values.begin();
		int size = known_values.size();
		for (int i = 0; i < size; ++i) {
			statement += "(" + it->first + "=" + secure_string(it->second) + " OR " + it->first + "=" +secure_string("*") + ")";
			++it;
			if (i < size - 1) {
				statement += " AND ";
			}
		}		
		
	}
	statement += ";";
	std::vector<std::map<std::string, std::string>> res;

	SQLiteCommand ^cmdSelect = db->CreateCommand();
	cmdSelect->CommandText = gcnew System::String(statement.c_str());
	SQLiteDataReader ^reader = cmdSelect->ExecuteReader();
	while (reader->Read()) {
		std::map<std::string, std::string> values;
		if (len > 0) {
			for (int i = 0; i < len; ++i) {
				values[values_to_select[i]] = MarshalString(reader->GetValue(i)->ToString());				
			}
			res.push_back(values);
		}
		else {
			res.push_back(known_values);
			break;
		}
	}
	
	delete cmdSelect;
	delete reader;
	return res;

}


void sql_wrapper::return_numbers_to_patch(std::string host_1, std::string host_2, std::vector<int>& numbers, std::set<std::string>& hosts_to_block)
{
	std::vector<std::string>tables = {"canAccessFile", "networkServiceInfo", "vulExists"};
	for (auto &i : tables) {
		std::vector<int> tmp = select_from_table_for_patch(i, host_1, host_2, hosts_to_block);
		numbers.insert(numbers.end(), tmp.begin(), tmp.end());
	}
	
}

std::vector<int> sql_wrapper::select_from_table_for_patch(std::string table_name, std::string host_1, std::string host_2, std::set<std::string>& hosts_to_block)
{
	std::string statement = "SELECT num ";	
	statement += " FROM " + secure_string(table_name);	
	statement += " WHERE ";
	int size = hosts_to_block.size(), i = 0;
 	for (auto& el : hosts_to_block) {
 		statement += "host=" + secure_string(el);
		i++;
		if (i < size) {
			statement += " OR ";
		}
 	}
	statement += ";";
	std::vector<int> res;

	SQLiteCommand ^cmdSelect = db->CreateCommand();
	cmdSelect->CommandText = gcnew System::String(statement.c_str());
	SQLiteDataReader ^reader = cmdSelect->ExecuteReader();
	std::map<std::string, std::string> values;
	while (reader->Read()) {		
		res.push_back(int::Parse(reader->GetValue(0)->ToString()));
	}
	delete cmdSelect;
	delete reader;
	return res;
}

std::string sql_wrapper::secure_string(std::string s)
{
	return "'" + s + "'";
}

void sql_wrapper::execute_statement(std::string statement)
{
	SQLiteCommand ^stmt1 = db->CreateCommand();
	stmt1->CommandText = gcnew System::String(statement.c_str());
	try {
		stmt1->ExecuteNonQuery();
	}
	catch (System::Exception ^e) {
		int k = 0;
	}
	delete stmt1;
}
