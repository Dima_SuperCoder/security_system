#include "xml_wrapper.h"
#include "utilities.h"


xml_wrapper::xml_wrapper()
{
}

bool xml_wrapper::open_file(std::string xmlfile, security_system& sys, sql_wrapper^ wrapper_sql)
{
	System::String^ filename = gcnew System::String(xmlfile.c_str());
	using namespace System::Xml;
	XmlDocument doc;

	std::map<std::string, std::shared_ptr<std::string>> types;
	doc.Load(filename);

	auto it = doc.ChildNodes->GetEnumerator();
	while (it->MoveNext()) {
		XmlNode^ node = dynamic_cast<XmlNode^>(it->Current);
		if (node->Name == "types") {
			XmlDocumentType^ dt = dynamic_cast<XmlDocumentType^>(it->Current);
			auto it2 = dt->Entities->GetEnumerator();
			while (it2->MoveNext()) {
				XmlEntity^ t = dynamic_cast<XmlEntity^>(it2->Current);

				std::string n = MarshalString(t->InnerText);
				auto sh_pt = std::make_shared<std::string>(n);
				types[n] = sh_pt;
				delete t;
			}
			delete dt;
			sys.set_types(types);
		}
		if (node->Name == "scenario") {
			XmlElement^ scenario = dynamic_cast<XmlElement^>(it->Current);
			auto it2 = scenario->ChildNodes->GetEnumerator();
			while (it2->MoveNext()) {
				XmlNode^ scenario_subnode = dynamic_cast<XmlNode^>(it2->Current);
				System::String^ name = scenario_subnode->Name;
				if (name == "facts") {
					auto facts_iterator = scenario_subnode->ChildNodes->GetEnumerator();
					while (facts_iterator->MoveNext()) {
						XmlNode^ fact_node = dynamic_cast<XmlNode^>(facts_iterator->Current);
						std::string fact_name = MarshalString(fact_node->Attributes[0]->Value);
						signs fact_signature;
						std::vector<std::string> columns;
						XmlNode^ signature_node = dynamic_cast<XmlNode^>(fact_node->ChildNodes[0]);
						auto signature_it = signature_node->ChildNodes->GetEnumerator();
						while (signature_it->MoveNext()) {
							XmlNode^ param_node = dynamic_cast<XmlNode^>(signature_it->Current);
							std::string type_string = MarshalString(param_node->ChildNodes[0]->InnerText);
							std::string name_string = MarshalString(param_node->ChildNodes[1]->InnerText);
							std::string group_string = "";
							if (param_node->ChildNodes->Count == 3) {
								group_string = MarshalString(param_node->ChildNodes[2]->InnerText);
							}
							record r(types[type_string], name_string);
							fact_signature.push_back(std::make_pair(r, group_string));
							columns.push_back(name_string);
						}
						delete signature_node;
						delete signature_it;
						
						std::vector <std::string > num_column;
						num_column.push_back("num");
						num_column.push_back("name");
						sys.add_fact_template(fact_name, fact_signature);
						int len = fact_signature.size();
						wrapper_sql->create_table(fact_name, columns, num_column);
						if (fact_node->ChildNodes->Count == 2) {
							XmlNode^ instances_node = dynamic_cast<XmlNode^>(fact_node->ChildNodes[1]);
							auto instances_it = instances_node->ChildNodes->GetEnumerator();
							while (instances_it->MoveNext()) {
								XmlNode^ instance_node = dynamic_cast<XmlNode^>(instances_it->Current);
								std::vector<record> recs;
								std::string name_string = fact_name + "(";
								std::map<std::string, std::string> row;
								for (int i = 0; i < len; ++i) {
									std::string s1, s2, t;
									s1 = MarshalString(instance_node->ChildNodes[i]->Name);
									s2 = MarshalString(instance_node->ChildNodes[i]->InnerText);
									record r(fact_signature[i].first.return_type(), s1, s2);
									recs.push_back(r);
									row[s1] = s2;
									name_string += s2;
									if (i < len - 1) {
										name_string += ",";
									}
								}
								name_string += ")";
								delete instance_node;
								wrapper_sql->add_row(fact_name, row);
							}
							delete instances_node;
							delete fact_node;
						}
					}
					delete facts_iterator;
				}
				else if (name == "postconditions") {
					auto attacks_iterator = scenario_subnode->ChildNodes->GetEnumerator();
					while (attacks_iterator->MoveNext()) {
						XmlNode^ attack_node = dynamic_cast<XmlNode^>(attacks_iterator->Current);
						std::string attack_name = MarshalString(attack_node->Attributes[0]->Value);
						signs attack_signature;
						std::vector<std::string> columns;

						XmlNode^ signature_node = dynamic_cast<XmlNode^>(attack_node->ChildNodes[0]);
						auto signature_it = signature_node->ChildNodes->GetEnumerator();
						while (signature_it->MoveNext()) {
							XmlNode^ param_node = dynamic_cast<XmlNode^>(signature_it->Current);
							std::string type_string = MarshalString(param_node->ChildNodes[0]->InnerText);
							std::string name_string = MarshalString(param_node->ChildNodes[1]->InnerText);
							std::string group_string = "";
							if (param_node->ChildNodes->Count == 3) {
								group_string = MarshalString(param_node->ChildNodes[2]->InnerText);
							}
							record r(types[type_string], name_string, group_string);
							attack_signature.push_back(std::make_pair(r, group_string));
							columns.push_back(name_string);
							delete param_node;
						}
						sys.add_postcond_template(attack_name, attack_signature);
						delete attack_node;
						delete signature_node;
						delete signature_it;

					}
					delete attacks_iterator;

				}
				else if (name == "attacks") {
					auto attacks_iterator = scenario_subnode->ChildNodes->GetEnumerator();
					while (attacks_iterator->MoveNext()) {
						XmlNode^ attack_node = dynamic_cast<XmlNode^>(attacks_iterator->Current);
						std::string attack_name = MarshalString(attack_node->Attributes[0]->Value);
						signs attack_signature;
						std::vector<fact> facts;
						XmlNode^ or_attack_node = dynamic_cast<XmlNode^>(attack_node->ChildNodes[0]);
						std::string or_attack_name = MarshalString(or_attack_node->InnerText);

						XmlNode^ signature_node = dynamic_cast<XmlNode^>(attack_node->ChildNodes[1]);
						auto signature_it = signature_node->ChildNodes->GetEnumerator();
						while (signature_it->MoveNext()) {
							XmlNode^ param_node = dynamic_cast<XmlNode^>(signature_it->Current);
							std::string type_string = MarshalString(param_node->ChildNodes[0]->InnerText);
							std::string name_string = MarshalString(param_node->ChildNodes[1]->InnerText);
							std::string val_string = MarshalString(param_node->ChildNodes[2]->InnerText);
							std::string group_string = "";
							if (param_node->ChildNodes->Count == 4) {
								group_string = MarshalString(param_node->ChildNodes[3]->InnerText);
							}
							record r(types[type_string], name_string, val_string);
							attack_signature.push_back(std::make_pair(r, group_string));
							delete param_node;
						}

						std::vector <std::string > columns;
						XmlNode^ facts_node = dynamic_cast<XmlNode^>(attack_node->ChildNodes[2]);
						auto facts_it = facts_node->ChildNodes->GetEnumerator();
						while (facts_it->MoveNext()) {
							XmlNode^ fact_node = dynamic_cast<XmlNode^>(facts_it->Current);
							std::string fact_name = MarshalString(fact_node->Attributes[0]->Value);
							signs s = sys.return_fact_template(fact_name).return_signature();

							int len = s.size();
							for (int i = 0; i < len; ++i) {
								s[i].first.set_value(MarshalString(fact_node->ChildNodes[i]->InnerText));
							}
							
							fact f(fact_name, s);
							facts.push_back(f);
							delete fact_node;
						}
						delete facts_node;
						delete attack_node;
						delete or_attack_node;
						delete signature_node;

						sys.add_attack_template(attack_name, attack_signature, facts, or_attack_name);
						
					}
					delete attacks_iterator;
				}
				delete scenario_subnode;
			}
			delete scenario;
			delete it2;
		}
		delete node;
	}
	delete it;
	delete filename;
	return true;
}

bool xml_wrapper::open_file(std::string xmlfile, security_system& sys)
{
	System::String^ filename = gcnew System::String(xmlfile.c_str());
	using namespace System::Xml;
	XmlDocument doc;

	std::map<std::string, std::shared_ptr<std::string>> types;
	doc.Load(filename);

	auto it = doc.ChildNodes->GetEnumerator();
	while (it->MoveNext()) {
		XmlNode^ node = dynamic_cast<XmlNode^>(it->Current);
		if (node->Name == "types") {
			XmlDocumentType^ dt = dynamic_cast<XmlDocumentType^>(it->Current);
			auto it2 = dt->Entities->GetEnumerator();
			while (it2->MoveNext()) {
				XmlEntity^ t = dynamic_cast<XmlEntity^>(it2->Current);

				std::string n = MarshalString(t->InnerText);
				auto sh_pt = std::make_shared<std::string>(n);
				types[n] = sh_pt;
				delete t;
			}
			delete dt;
			sys.set_types(types);
		}
		if (node->Name == "scenario") {
			XmlElement^ scenario = dynamic_cast<XmlElement^>(it->Current);
			auto it2 = scenario->ChildNodes->GetEnumerator();
			while (it2->MoveNext()) {
				XmlNode^ scenario_subnode = dynamic_cast<XmlNode^>(it2->Current);
				System::String^ name = scenario_subnode->Name;
				if (name == "facts") {
					auto facts_iterator = scenario_subnode->ChildNodes->GetEnumerator();
					while (facts_iterator->MoveNext()) {
						XmlNode^ fact_node = dynamic_cast<XmlNode^>(facts_iterator->Current);
						std::string fact_name = MarshalString(fact_node->Attributes[0]->Value);
						signs fact_signature;
						std::vector<std::string> columns;
						XmlNode^ signature_node = dynamic_cast<XmlNode^>(fact_node->ChildNodes[0]);
						auto signature_it = signature_node->ChildNodes->GetEnumerator();
						while (signature_it->MoveNext()) {
							XmlNode^ param_node = dynamic_cast<XmlNode^>(signature_it->Current);
							std::string type_string = MarshalString(param_node->ChildNodes[0]->InnerText);
							std::string name_string = MarshalString(param_node->ChildNodes[1]->InnerText);
							std::string group_string = "";
							if (param_node->ChildNodes->Count == 3) {
								group_string = MarshalString(param_node->ChildNodes[2]->InnerText);
							}
							record r(types[type_string], name_string);
							fact_signature.push_back(std::make_pair(r, group_string));
							columns.push_back(name_string);
							delete param_node;
						}
						delete signature_node;
						delete signature_it;
						
						sys.add_fact_template(fact_name, fact_signature);						
						delete fact_node;
					}
				}
				else if (name == "postconditions") {
					auto attacks_iterator = scenario_subnode->ChildNodes->GetEnumerator();
					while (attacks_iterator->MoveNext()) {
						XmlNode^ attack_node = dynamic_cast<XmlNode^>(attacks_iterator->Current);
						std::string attack_name = MarshalString(attack_node->Attributes[0]->Value);
						signs attack_signature;
						std::vector<std::string> columns;

						XmlNode^ signature_node = dynamic_cast<XmlNode^>(attack_node->ChildNodes[0]);
						auto signature_it = signature_node->ChildNodes->GetEnumerator();
						while (signature_it->MoveNext()) {
							XmlNode^ param_node = dynamic_cast<XmlNode^>(signature_it->Current);
							std::string type_string = MarshalString(param_node->ChildNodes[0]->InnerText);
							std::string name_string = MarshalString(param_node->ChildNodes[1]->InnerText);
							std::string group_string = "";
							if (param_node->ChildNodes->Count == 3) {
								group_string = MarshalString(param_node->ChildNodes[2]->InnerText);
							}
							record r(types[type_string], name_string, group_string);
							attack_signature.push_back(std::make_pair(r, group_string));
							columns.push_back(name_string);
							delete param_node;
						}
						sys.add_postcond_template(attack_name, attack_signature);
						delete attack_node;
						delete signature_node;
						delete signature_it;
						//std::vector <std::string > num_column;
						//num_column.push_back("num");
						//num_column.push_back("name");
						//wrapper->create_table(attack_name, columns, num_column);
					}
					delete attacks_iterator;

				}
				else if (name == "attacks") {
					auto attacks_iterator = scenario_subnode->ChildNodes->GetEnumerator();
					while (attacks_iterator->MoveNext()) {
						XmlNode^ attack_node = dynamic_cast<XmlNode^>(attacks_iterator->Current);
						std::string attack_name = MarshalString(attack_node->Attributes[0]->Value);
						signs attack_signature;
						std::vector<fact> facts;
						XmlNode^ or_attack_node = dynamic_cast<XmlNode^>(attack_node->ChildNodes[0]);
						std::string or_attack_name = MarshalString(or_attack_node->InnerText);

						XmlNode^ signature_node = dynamic_cast<XmlNode^>(attack_node->ChildNodes[1]);
						auto signature_it = signature_node->ChildNodes->GetEnumerator();
						while (signature_it->MoveNext()) {
							XmlNode^ param_node = dynamic_cast<XmlNode^>(signature_it->Current);
							std::string type_string = MarshalString(param_node->ChildNodes[0]->InnerText);
							std::string name_string = MarshalString(param_node->ChildNodes[1]->InnerText);
							std::string val_string = MarshalString(param_node->ChildNodes[2]->InnerText);
							std::string group_string = "";
							if (param_node->ChildNodes->Count == 4) {
								group_string = MarshalString(param_node->ChildNodes[3]->InnerText);
							}
							record r(types[type_string], name_string, val_string);
							attack_signature.push_back(std::make_pair(r, group_string));
							delete param_node;
						}

						std::vector <std::string > columns;
						XmlNode^ facts_node = dynamic_cast<XmlNode^>(attack_node->ChildNodes[2]);
						auto facts_it = facts_node->ChildNodes->GetEnumerator();
						while (facts_it->MoveNext()) {
							XmlNode^ fact_node = dynamic_cast<XmlNode^>(facts_it->Current);
							std::string fact_name = MarshalString(fact_node->Attributes[0]->Value);
							signs s = sys.return_fact_template(fact_name).return_signature();

							int len = s.size();
							for (int i = 0; i < len; ++i) {
								s[i].first.set_value(MarshalString(fact_node->ChildNodes[i]->InnerText));
							}

							fact f(fact_name, s);
							facts.push_back(f);
							delete fact_node;
						}
						delete facts_node;
						delete attack_node;
						delete or_attack_node;
						delete signature_node;
						// 						num_column.push_back("num");
						// 						num_column.push_back("name");
						sys.add_attack_template(attack_name, attack_signature, facts, or_attack_name);
						//wrapper->create_table(attack_name, columns, num_column);
					}
					delete attacks_iterator;
				}
				delete scenario_subnode;
			}
			delete scenario;
			delete it2;
		}
		delete node;
	}
	delete it;
	delete filename;
	return true;
}

bool xml_wrapper::save_file(std::string xmlfile, security_system& sys, sql_wrapper^ wrapper_sql)
{
	System::String^ filename = gcnew System::String(xmlfile.c_str());
	using namespace System::Xml;
	XmlDocument doc;

	System::String^ int_subset = "";
	for (auto& t : sys.return_types_names()) {
		System::String^ n = gcnew System::String(t.c_str());
		int_subset += "\r\n <!ENTITY type_" + n + " \"" + n + "\">";
	}

	XmlDocumentType^ dt = doc.CreateDocumentType("types", nullptr, nullptr, int_subset);
	doc.AppendChild(dt);

	XmlElement^ el = doc.CreateElement("", "scenario", "");

	for (int i = 0; i < 3; ++i) {
		System::String^ n = "facts";
		if (i == 1) { n = "postconditions"; }
		if (i == 2) { n = "attacks"; }
		XmlNode^ scenario_subnode = doc.CreateNode(XmlNodeType::Element, n, "");

		if (i == 0) {
			for (auto& f : sys.return_fact_template()) {
				XmlNode^ fact_subnode = doc.CreateNode(XmlNodeType::Element, "fact", "");

				XmlAttribute^ att = doc.CreateAttribute("", "name", "");
				att->Value = gcnew System::String(f.first.c_str());
				fact_subnode->Attributes->Append(att);

				std::vector<std::string> columns;
				//формирование сигнатуры
				XmlNode^ sign_node = doc.CreateNode(XmlNodeType::Element, "signature", "");
				for (auto& rec : f.second.return_signature()) {
					XmlNode^ param_node = doc.CreateNode(XmlNodeType::Element, "param", "");
					XmlNode^ type_node = doc.CreateNode(XmlNodeType::Element, "type", "");
					System::String^ tmp = gcnew System::String(rec.first._return_type().c_str());
					type_node->InnerXml = "&type_" + tmp + ";";
					param_node->AppendChild(type_node);

					XmlNode^ name_node = doc.CreateNode(XmlNodeType::Element, "name", "");
					tmp = gcnew System::String(rec.first.return_name().c_str());
					name_node->InnerText = tmp;
					columns.push_back(rec.first.return_name());
					param_node->AppendChild(name_node);
					if (rec.second != "") {
						XmlNode^ group_node = doc.CreateNode(XmlNodeType::Element, "group", "");
						tmp = gcnew System::String(rec.second.c_str());
						group_node->InnerText = tmp;
						param_node->AppendChild(group_node);
					}

					sign_node->AppendChild(param_node);
				}
				fact_subnode->AppendChild(sign_node);

				//выписывание фактов
				XmlNode^ instances_node = doc.CreateNode(XmlNodeType::Element, "instances", "");
				std::vector<std::map<std::string, std::string>> instances = wrapper_sql->select_from_table(f.first, columns, {}, false);
				for (auto& instance : instances) {
					XmlNode^ instance_node = doc.CreateNode(XmlNodeType::Element, "instance", "");

					for (auto& column : columns) {
						System::String^ n = gcnew System::String(column.c_str());
						XmlNode^ sub_instance_node = doc.CreateNode(XmlNodeType::Element, n, "");
						System::String^ v = gcnew System::String(instance[column].c_str());
						sub_instance_node->InnerText = v;

						instance_node->AppendChild(sub_instance_node);
					}
					instances_node->AppendChild(instance_node);
				}

				fact_subnode->AppendChild(instances_node);

				scenario_subnode->AppendChild(fact_subnode);

			}
			el->AppendChild(scenario_subnode);
		}

		if (i == 1) {
			for (auto& or_attack : sys.return_postcondition_template()) {
				XmlNode^ or_subnode = doc.CreateNode(XmlNodeType::Element, "attack", "");

				XmlAttribute^ att = doc.CreateAttribute("", "name", "");
				att->InnerText = gcnew System::String(or_attack.first.c_str());
				or_subnode->Attributes->Append(att);

				//формирование сигнатуры
				XmlNode^ sign_node = doc.CreateNode(XmlNodeType::Element, "signature", "");
				for (auto& rec : or_attack.second.return_signature()) {
					XmlNode^ param_node = doc.CreateNode(XmlNodeType::Element, "param", "");
					XmlNode^ type_node = doc.CreateNode(XmlNodeType::Element, "type", "");
					System::String^ tmp = gcnew System::String(rec.first._return_type().c_str());
					type_node->InnerXml = "&type_" + tmp + ";";
					param_node->AppendChild(type_node);

					XmlNode^ name_node = doc.CreateNode(XmlNodeType::Element, "name", "");
					tmp = gcnew System::String(rec.first.return_name().c_str());
					name_node->InnerText = tmp;
					param_node->AppendChild(name_node);
					if (rec.second != "") {
						XmlNode^ group_node = doc.CreateNode(XmlNodeType::Element, "group", "");
						tmp = gcnew System::String(rec.second.c_str());
						group_node->InnerText = tmp;
						param_node->AppendChild(group_node);
					}

					sign_node->AppendChild(param_node);
				}
				or_subnode->AppendChild(sign_node);

				scenario_subnode->AppendChild(or_subnode);
			}
			el->AppendChild(scenario_subnode);

		}

		if (i == 2) {
			for (auto& and_attack : sys.return_attack_template()) {
				XmlNode^ and_subnode = doc.CreateNode(XmlNodeType::Element, "attack", "");

				XmlAttribute^ att = doc.CreateAttribute("", "name", "");
				att->InnerText = gcnew System::String(and_attack.first.c_str());
				and_subnode->Attributes->Append(att);

				XmlNode^ or_attack = doc.CreateNode(XmlNodeType::Element, "or_attack", "");
				or_attack->InnerText = gcnew System::String(and_attack.second.return_pcond().c_str());
				and_subnode->AppendChild(or_attack);

				//формирование сигнатуры
				XmlNode^ sign_node = doc.CreateNode(XmlNodeType::Element, "signature", "");
				for (auto& rec : and_attack.second.return_signature()) {
					XmlNode^ param_node = doc.CreateNode(XmlNodeType::Element, "param", "");

					XmlNode^ type_node = doc.CreateNode(XmlNodeType::Element, "type", "");
					System::String^ tmp = gcnew System::String(rec.first._return_type().c_str());
					type_node->InnerXml = "&type_" + tmp + ";";
					param_node->AppendChild(type_node);

					XmlNode^ name_node = doc.CreateNode(XmlNodeType::Element, "name", "");
					tmp = gcnew System::String(rec.first.return_name().c_str());
					name_node->InnerText = tmp;
					param_node->AppendChild(name_node);

					XmlNode^ value_node = doc.CreateNode(XmlNodeType::Element, "value", "");
					tmp = gcnew System::String(rec.first.return_value().c_str());
					value_node->InnerText = tmp;
					param_node->AppendChild(value_node);

					if (rec.second != "") {
						XmlNode^ group_node = doc.CreateNode(XmlNodeType::Element, "group", "");
						tmp = gcnew System::String(rec.second.c_str());
						group_node->InnerText = tmp;
						param_node->AppendChild(group_node);
					}

					sign_node->AppendChild(param_node);

				}
				and_subnode->AppendChild(sign_node);

				//формирование фактов
				XmlNode^ facts_node = doc.CreateNode(XmlNodeType::Element, "facts", "");
				for (auto& f : and_attack.second.return_facts()) {
					XmlNode^ fact_node = doc.CreateNode(XmlNodeType::Element, "fact", "");
					XmlAttribute^ att = doc.CreateAttribute("", "name", "");
					att->InnerText = gcnew System::String(f.return_name().c_str());
					fact_node->Attributes->Append(att);

					for (auto& s : f.return_signature()) {
						System::String^ n = gcnew System::String(s.first.return_name().c_str());
						System::String^ v = gcnew System::String(s.first.return_value().c_str());
						XmlNode^ param_node = doc.CreateNode(XmlNodeType::Element, n, "");
						param_node->InnerText = v;

						fact_node->AppendChild(param_node);
					}

					facts_node->AppendChild(fact_node);
				}

				and_subnode->AppendChild(facts_node);

				scenario_subnode->AppendChild(and_subnode);

			}

			el->AppendChild(scenario_subnode);
		}

	}
	doc.AppendChild(el);
	doc.Save(filename);
	return true;
}

bool xml_wrapper::save_file(std::string xmlfile, security_system& sys)
{
	System::String^ filename = gcnew System::String(xmlfile.c_str());
	using namespace System::Xml;
	XmlDocument doc;

	System::String^ int_subset = "";
	for (auto& t : sys.return_types_names()) {
		System::String^ n = gcnew System::String(t.c_str());
		int_subset += "\r\n <!ENTITY type_" + n + " \"" + n + "\">";
	}

	XmlDocumentType^ dt = doc.CreateDocumentType("types", nullptr, nullptr, int_subset);
	doc.AppendChild(dt);

	XmlElement^ el = doc.CreateElement("", "scenario", "");

	for (int i = 0; i < 3; ++i) {
		System::String^ n = "facts";
		if (i == 1) { n = "postconditions"; }
		if (i == 2) { n = "attacks"; }
		XmlNode^ scenario_subnode = doc.CreateNode(XmlNodeType::Element, n, "");

		if (i == 0) {
			for (auto& f : sys.return_fact_template()) {
				XmlNode^ fact_subnode = doc.CreateNode(XmlNodeType::Element, "fact", "");

				XmlAttribute^ att = doc.CreateAttribute("", "name", "");
				att->Value = gcnew System::String(f.first.c_str());
				fact_subnode->Attributes->Append(att);

				std::vector<std::string> columns;
				//формирование сигнатуры
				XmlNode^ sign_node = doc.CreateNode(XmlNodeType::Element, "signature", "");
				for (auto& rec : f.second.return_signature()) {
					XmlNode^ param_node = doc.CreateNode(XmlNodeType::Element, "param", "");
					XmlNode^ type_node = doc.CreateNode(XmlNodeType::Element, "type", "");
					System::String^ tmp = gcnew System::String(rec.first._return_type().c_str());
					type_node->InnerXml = "&type_" + tmp + ";";
					param_node->AppendChild(type_node);

					XmlNode^ name_node = doc.CreateNode(XmlNodeType::Element, "name", "");
					tmp = gcnew System::String(rec.first.return_name().c_str());
					name_node->InnerText = tmp;
					columns.push_back(rec.first.return_name());
					param_node->AppendChild(name_node);
					if (rec.second != "") {
						XmlNode^ group_node = doc.CreateNode(XmlNodeType::Element, "group", "");
						tmp = gcnew System::String(rec.second.c_str());
						group_node->InnerText = tmp;
						param_node->AppendChild(group_node);
					}

					sign_node->AppendChild(param_node);
				}
				fact_subnode->AppendChild(sign_node);

				scenario_subnode->AppendChild(fact_subnode);

			}
			el->AppendChild(scenario_subnode);
		}

		if (i == 1) {
			for (auto& or_attack : sys.return_postcondition_template()) {
				XmlNode^ or_subnode = doc.CreateNode(XmlNodeType::Element, "attack", "");

				XmlAttribute^ att = doc.CreateAttribute("", "name", "");
				att->InnerText = gcnew System::String(or_attack.first.c_str());
				or_subnode->Attributes->Append(att);

				//формирование сигнатуры
				XmlNode^ sign_node = doc.CreateNode(XmlNodeType::Element, "signature", "");
				for (auto& rec : or_attack.second.return_signature()) {
					XmlNode^ param_node = doc.CreateNode(XmlNodeType::Element, "param", "");
					XmlNode^ type_node = doc.CreateNode(XmlNodeType::Element, "type", "");
					System::String^ tmp = gcnew System::String(rec.first._return_type().c_str());
					type_node->InnerXml = "&type_" + tmp + ";";
					param_node->AppendChild(type_node);

					XmlNode^ name_node = doc.CreateNode(XmlNodeType::Element, "name", "");
					tmp = gcnew System::String(rec.first.return_name().c_str());
					name_node->InnerText = tmp;
					param_node->AppendChild(name_node);
					if (rec.second != "") {
						XmlNode^ group_node = doc.CreateNode(XmlNodeType::Element, "group", "");
						tmp = gcnew System::String(rec.second.c_str());
						group_node->InnerText = tmp;
						param_node->AppendChild(group_node);
					}

					sign_node->AppendChild(param_node);
				}
				or_subnode->AppendChild(sign_node);

				scenario_subnode->AppendChild(or_subnode);
			}
			el->AppendChild(scenario_subnode);

		}

		if (i == 2) {
			for (auto& and_attack : sys.return_attack_template()) {
				XmlNode^ and_subnode = doc.CreateNode(XmlNodeType::Element, "attack", "");

				XmlAttribute^ att = doc.CreateAttribute("", "name", "");
				att->InnerText = gcnew System::String(and_attack.first.c_str());
				and_subnode->Attributes->Append(att);

				XmlNode^ or_attack = doc.CreateNode(XmlNodeType::Element, "or_attack", "");
				or_attack->InnerText = gcnew System::String(and_attack.second.return_pcond().c_str());
				and_subnode->AppendChild(or_attack);

				//формирование сигнатуры
				XmlNode^ sign_node = doc.CreateNode(XmlNodeType::Element, "signature", "");
				for (auto& rec : and_attack.second.return_signature()) {
					XmlNode^ param_node = doc.CreateNode(XmlNodeType::Element, "param", "");

					XmlNode^ type_node = doc.CreateNode(XmlNodeType::Element, "type", "");
					System::String^ tmp = gcnew System::String(rec.first._return_type().c_str());
					type_node->InnerXml = "&type_" + tmp + ";";
					param_node->AppendChild(type_node);

					XmlNode^ name_node = doc.CreateNode(XmlNodeType::Element, "name", "");
					tmp = gcnew System::String(rec.first.return_name().c_str());
					name_node->InnerText = tmp;
					param_node->AppendChild(name_node);

					XmlNode^ value_node = doc.CreateNode(XmlNodeType::Element, "value", "");
					tmp = gcnew System::String(rec.first.return_value().c_str());
					value_node->InnerText = tmp;
					param_node->AppendChild(value_node);

					if (rec.second != "") {
						XmlNode^ group_node = doc.CreateNode(XmlNodeType::Element, "group", "");
						tmp = gcnew System::String(rec.second.c_str());
						group_node->InnerText = tmp;
						param_node->AppendChild(group_node);
					}

					sign_node->AppendChild(param_node);

				}
				and_subnode->AppendChild(sign_node);

				//формирование фактов
				XmlNode^ facts_node = doc.CreateNode(XmlNodeType::Element, "facts", "");
				for (auto& f : and_attack.second.return_facts()) {
					XmlNode^ fact_node = doc.CreateNode(XmlNodeType::Element, "fact", "");
					XmlAttribute^ att = doc.CreateAttribute("", "name", "");
					att->InnerText = gcnew System::String(f.return_name().c_str());
					fact_node->Attributes->Append(att);

					for (auto& s : f.return_signature()) {
						System::String^ n = gcnew System::String(s.first.return_name().c_str());
						System::String^ v = gcnew System::String(s.first.return_value().c_str());
						XmlNode^ param_node = doc.CreateNode(XmlNodeType::Element, n, "");
						param_node->InnerText = v;

						fact_node->AppendChild(param_node);
					}

					facts_node->AppendChild(fact_node);
				}

				and_subnode->AppendChild(facts_node);

				scenario_subnode->AppendChild(and_subnode);

			}

			el->AppendChild(scenario_subnode);
		}

	}
	doc.AppendChild(el);
	doc.Save(filename);
	return true;
}

bool xml_wrapper::parse_file(std::string xmlfile, sql_wrapper^ wrapper_sql)
{
	System::String^ filename = gcnew System::String(xmlfile.c_str());
	using namespace System::Xml;
	XmlDocument doc;

	std::map<std::string, std::shared_ptr<std::string>> types;
	doc.Load(filename);

	auto it = doc.ChildNodes->GetEnumerator();
	while (it->MoveNext()) {
		XmlNode^ node = dynamic_cast<XmlNode^>(it->Current);
		if (node->Name == "scenario") {
			XmlElement^ scenario = dynamic_cast<XmlElement^>(it->Current);
			auto it2 = scenario->ChildNodes->GetEnumerator();
			while (it2->MoveNext()) {
				XmlNode^ scenario_subnode = dynamic_cast<XmlNode^>(it2->Current);
				System::String^ name = scenario_subnode->Name;
				if (name == "facts") {
					auto facts_iterator = scenario_subnode->ChildNodes->GetEnumerator();
					while (facts_iterator->MoveNext()) {
						XmlNode^ fact_node = dynamic_cast<XmlNode^>(facts_iterator->Current);
						std::string fact_name = MarshalString(fact_node->Attributes[0]->Value);
						std::vector<std::string> columns;
						XmlNode^ signature_node = dynamic_cast<XmlNode^>(fact_node->ChildNodes[0]);
						auto signature_it = signature_node->ChildNodes->GetEnumerator();
						while (signature_it->MoveNext()) {
							XmlNode^ param_node = dynamic_cast<XmlNode^>(signature_it->Current);
							std::string name_string = MarshalString(param_node->ChildNodes[1]->InnerText);							
							columns.push_back(name_string);
							delete param_node;
						}
						delete signature_node;
						delete signature_it;				

						std::vector <std::string > num_column;
						num_column.push_back("num");
						num_column.push_back("name");
						
						int len = columns.size();

						wrapper_sql->create_table(fact_name, columns, num_column);
						if (fact_node->ChildNodes->Count == 2) {
							XmlNode^ instances_node = dynamic_cast<XmlNode^>(fact_node->ChildNodes[1]);
							auto instances_it = instances_node->ChildNodes->GetEnumerator();
							while (instances_it->MoveNext()) {
								XmlNode^ instance_node = dynamic_cast<XmlNode^>(instances_it->Current);
								std::map<std::string, std::string> row;
								for (int i = 0; i < len; ++i) {
									std::string s1, s2, t;
									s1 = MarshalString(instance_node->ChildNodes[i]->Name);
									s2 = MarshalString(instance_node->ChildNodes[i]->InnerText);
									row[s1] = s2;
								}
								delete instance_node;
								wrapper_sql->add_row(fact_name, row);
							}
							delete instances_node;
							delete fact_node;
						}
					}
					delete facts_iterator;
				}
				delete scenario_subnode;
			}
			delete scenario;
			delete it2;
		}
		delete node;
	}
	delete it;
	delete filename;
	return true;
}

