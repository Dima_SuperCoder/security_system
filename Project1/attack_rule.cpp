#include "attack_rule.h"
#include <algorithm>
#include <utility>
#include <set>
#include <list>
#include <iostream>
#include "utilities.h"

attack_rule::attack_rule(std::string attack_name, signs sign, std::vector<fact> f)
{
	name = attack_name;
	signature = sign;
	facts = f;
}

std::vector<std::string> attack_rule::return_columns(bool with_nums /*= true*/) const
{
	std::vector<std::string> res = get_columns();
	if (with_nums) {
		for (auto& f : facts) {
			res.push_back(f.return_name() + "_num");
		}
	}
	return res;
}


std::map<std::string, std::vector<std::pair<std::string, std::string>>> attack_rule::return_variables_by_facts() const
{
	std::map<std::string, std::string> sign_val_to_name;
	
	for (auto s : signature) {
		sign_val_to_name[s.first.return_value()] = s.first.return_name();
	}

	std::map<std::string, std::vector<std::pair<std::string, std::string>>> res;
	for (auto& f : facts) {
		for (auto& s : f.return_signature()) {
			if (is_variable(s.first.return_value()) && s.first.return_value() != "*") {
				auto it = sign_val_to_name.find(s.first.return_value());
				if (it != sign_val_to_name.end()) {
					res[it->second].push_back(std::make_pair(f.return_name(), s.first.return_name()));
				}
				else {
					res[s.first.return_value()].push_back(std::make_pair(f.return_name(), s.first.return_name()));
				}
			}
			else if (s.first.return_value() == "*") {
				res[f.return_name() + "_" + s.first.return_name()].push_back(std::make_pair(f.return_name(), s.first.return_name()));
			}
		}
		res[f.return_name() + "_num"].push_back(std::make_pair(f.return_name(), "num"));
	}
	return res;
}

std::map<std::string, std::vector<std::pair<std::string, std::string>>> attack_rule::return_consts_by_facts() const
{
	std::map<std::string, std::vector<std::pair<std::string, std::string>>> res;
	for (auto& f : facts) {
		for (auto& s : f.return_signature()) {
			if (!is_variable(s.first.return_value()) && s.first.return_value() != "*") {
				res[s.first.return_value()].push_back(std::make_pair(f.return_name(), s.first.return_name()));
			}
		}
	}
	return res;
}

std::vector<int> attack_rule::return_numbers_by_signature(signs postcond, sql_wrapper^ wrapper, std::map<int, std::vector<int>>& and_vector)
{
	std::vector<int> res;
	
	
	std::map<std::string, std::string> vals;
	std::vector<std::string> v_to_s = { "num" };

	int size = signature.size();
	for (int i = 0; i < size; ++i) {
		vals[signature[i].first.return_name()] = postcond[i].first.return_value();
	}

	for (auto& f : facts) {
		v_to_s.push_back(f.return_name() + "_num");
	}
		
	std::vector<std::map<std::string, std::string>> all_vals = wrapper->select_from_table(name, v_to_s, vals, true);

	for (auto& row : all_vals) {
		res.push_back(std::atoi(row["num"].c_str()));
		for (auto& el : row) {
			if (el.first != "num") {
				and_vector[std::atoi(row["num"].c_str())].push_back(std::atoi(el.second.c_str()));
			}
		}
	}

	return res;
}


std::map<std::string, std::vector<std::string>> attack_rule::return_all_columns()
{
	std::map<std::string, std::vector<std::string>> res;
	res["vars"];
	res["vals"];
	res["consts"];
	res["num"];

	for (auto& s : signature) {
		if (is_variable(s.first.return_value())) {
			res["vars"].push_back(s.first.return_name());
		}
		else if (s.first.return_value() != "*") {
			res["consts"].push_back(s.first.return_name());
			res["vals"].push_back(s.first.return_value());
		}
	}

	for (auto f : facts) {
		res["num"].push_back(f.return_name() + "_num");
	}

	return res;
}

bool attack_rule::is_contain_fact(std::string fact_name)
{
	for (auto& fct : facts) {
		if (fct.return_name() == fact_name) {
			return true;
		}
	}
	return false;
}

bool attack_rule::operator<(const attack_rule& r1) const
{
	if (name != r1.name) {
		return name < r1.name;
	}
	else {
		if (signature != r1.signature) {
			return signature < r1.signature;
		}
		else {
			return facts < r1.facts;
		}
	}
}

std::vector<std::string> attack_rule::get_columns() const
{
	std::vector<std::string> res;

	for (auto s : signature) {
		res.push_back(s.first.return_name());
	}

	return res;
}
