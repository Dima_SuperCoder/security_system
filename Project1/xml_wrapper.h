#pragma once
#include "security_system.h"
#include "sql_wrapper.h"
ref class xml_wrapper
{
public:
	xml_wrapper();
	bool open_file(std::string filename, security_system& s, sql_wrapper^ wrapper_sql);
	bool save_file(std::string filename, security_system& s, sql_wrapper^ wrapper_sql);

	bool open_file(std::string filename, security_system& s);
	bool save_file(std::string filename, security_system& s);

	bool parse_file(std::string filename, sql_wrapper^ wrapper_sql);
};

