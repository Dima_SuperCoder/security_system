#include "my_types.h"
#include <iostream>

record::record(std::shared_ptr<std::string> t, std::string n, std::string v/* = ""*/)
{
	r_type = t;
	name = n;
	set_value(v);
}

void record::set_value(std::string val)
{
	value = val;
}

bool record::compare_records(record& r1, record& r2)
{
	return (r1.r_type == r2.r_type && r1.name == r2.name);
}