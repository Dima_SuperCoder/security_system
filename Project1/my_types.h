#pragma once
#include <string>
#include <functional>
#include <vector>
#include "map"

class record
{
public:
	record(std::shared_ptr<std::string> t, std::string n, std::string v = "" );
	void set_value(std::string val);
	std::string return_name() const { return name; }
	std::string return_value() const { return value; }
	std::shared_ptr<std::string> return_type() const { return r_type; }
	std::string _return_type() const { return *r_type; }
	static bool compare_records(record& r1, record& r2);
	
private:
	std::shared_ptr<std::string> r_type;
	std::string name;
	std::string value;
};

inline bool operator==(const record& r1, const record& r2) { return r1.return_value() == r2.return_value(); }
inline bool operator<(const record& r1, const record& r2) { return r1.return_name() < r2.return_name() || r1.return_value() < r2.return_value(); }

typedef std::vector <std::pair<record, std::string>> signs; // signature
typedef std::map<std::string, std::vector<record>> sbg; //signature by group - <group, vector< type, var_name>>