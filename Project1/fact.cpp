#include "fact.h"



fact::fact(std::string fact_name, signs in_signature, bool act /*= true*/)
{
	name = fact_name;
	signature = in_signature;
	active = act;
	for (auto& p : signature) {
		signature_by_group[p.second].push_back(p.first);
	}
}



std::string fact::return_full_name() const
{

	std::string res = name + "(";
	int size = signature.size();
	for (int i = 0; i < size; ++i) {
		res += signature[i].first.return_value();
		if (i < size - 1) { res += ", "; }
	}

	res += ")";
	return res;
}

std::vector<std::string> fact::return_columns(bool with_num) const
{
	std::vector<std::string> res;
	for (auto& s : signature) {
		res.push_back(s.first.return_name());
	}
	if (with_num) {
		res.push_back("num");
	}
	return res;
}

std::vector<std::string> fact::return_additional_columns() const
{
	return std::vector<std::string>{"num", "name"};
}

std::vector<std::vector<std::string>> fact::return_values_by_signature(signs s)
{
	std::vector<std::vector<std::string>> res;
	std::vector<std::string> group;
	bool ans = true;
	int size = s.size();
	for (auto& g : signature_by_group) {
		group.clear();
		ans = true;
		std::vector<bool> answers(size, false);
		for (auto& el : g.second) {
			for (int i = 0; i < size; ++i) {
				if (el.return_type() == s[i].first.return_type()) {
					answers[i] = true;
					group.push_back(el.return_name());
				}
			}
		}
		for (auto& a : answers) { ans *= a; }
		if (ans) {
			res.push_back(group);
		}
	}

	return res;
}

bool fact::operator<(const fact& r1) const
{
	if (name != r1.name) {
		return name < r1.name;
	}
	else {
		return signature < r1.signature;
	}
}
