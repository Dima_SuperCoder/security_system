﻿#pragma once
#include "security_system.h"
#include "sql_wrapper.h"
#include "my_types.h"
#include "xml_wrapper.h"
#include "utilities.h"

namespace Project1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	
	
	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MainForm : public System::Windows::Forms::Form
	{
	private: security_system* s_system;
	private: System::Windows::Forms::Button^  ANDAttackUpdateFactBtn;

	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::SaveFileDialog^ saveFileDialog;


	private: System::Windows::Forms::ToolStripMenuItem^  importToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  opt1ToolStripMenuItem;
	private: System::Windows::Forms::Button^  button1;

	private: System::Windows::Forms::TabPage^  BlockingTab;
	private: System::Windows::Forms::Button^  AddFactToBlockBtn;





	private: System::Windows::Forms::ListBox^  FactsNamesToBlockLB;

	private: System::Windows::Forms::ListBox^  AllFactsNamesLB;
	private: System::Windows::Forms::Button^  GetNamesToBlockBtn;
	private: System::Windows::Forms::Button^  AddOrAttackToBlockBtn;



	private: System::Windows::Forms::ListBox^  PcondsToBlockLB;
	private: System::Windows::Forms::ListBox^  AllPcondNamesLB;
	private: System::Windows::Forms::Button^  DeleteOrAttackFromBlockBtn;
	private: System::Windows::Forms::Button^  DeleteFactFromBlockBtn;
	private: System::Windows::Forms::Label^  label13;
	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::TextBox^  UpBlockingBoundaryTxt;
	private: System::Windows::Forms::TextBox^  LowBlockingBoundaryTxt;
	private: System::Windows::Forms::TextBox^  NumberOfStepsTxt;

	private: System::Windows::Forms::Button^  WriteGraphvizBlockingBtn;
	private: System::Windows::Forms::Button^  CalcBlocksBtn;
	private: System::Windows::Forms::GroupBox^  groupBox4;
	private: System::Windows::Forms::GroupBox^  groupBox3;
	private: System::Windows::Forms::Button^ WriteGraphvizBtn;


	private: System::Windows::Forms::Button^  showPatchesBtn;
	private: System::Windows::Forms::TabPage^  TestingTab;
	private: System::Windows::Forms::Button^  TestPatchesBtn;
	private: System::Windows::Forms::RichTextBox^  TestingBox;

	private: System::Windows::Forms::OpenFileDialog^  opentTestFiles;

	private: System::Windows::Forms::GroupBox^ ScissoringSwotcher;
	private: System::Windows::Forms::RadioButton^ AtScissoring;
	private: System::Windows::Forms::RadioButton^ PostScissoring;
	private: System::Windows::Forms::Button^ TestTimesBtn;
	private: System::Windows::Forms::TextBox^ textBox1;

	private: System::Windows::Forms::ToolStripMenuItem^ newToolStripMenuItem;
	private: System::Windows::Forms::GroupBox^ groupBox5;
	private: System::Windows::Forms::TextBox^ TimeoutTxtBox;
	private: System::Windows::Forms::Label^ label14;
	private: System::Windows::Forms::Label^ label16;
	private: System::Windows::Forms::TextBox^ TimeoutTestTxt;
	private: System::Windows::Forms::Label^ label15;


	private: std::vector<fact> *attack_facts;
		     bool show_test_tab = false; //change to true if u need to calc tests;
	public:
		MainForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			s_system = new security_system();
			attack_facts = new std::vector<fact>;
			wrapper_sql = gcnew sql_wrapper();
			wrapper_sql->open_connection(MarshalString(db_path));
			wrapper_xml = gcnew xml_wrapper();
			facts_to_block = new std::map<std::string, int>;
			attacks_to_block = new std::map<std::string, int>;
			if (!show_test_tab) {
				SystemTab->TabPages->RemoveAt(5);
			}
		}
	public: bool add_fact_proto(System::String^ fact_name, std::vector<std::string> types, std::vector<std::string> names, std::vector<std::string> groups);
	private: void set_names();
	private: void set_types_names(std::vector<std::string> types);
	private: void move_listview_row_up(ListView^ lv);
	private: void move_listview_row_down(ListView^ lv);
	private: void add_row_to_fact_proto_lv(String^ t, String^ name, String^ group);
	private: void add_row_to_or_attack_lv(String^ t, String^ name);
	public: bool add_postcond_proto(System::String^ fact_name, std::vector<std::string> types, std::vector<std::string> names, std::vector<std::string> groups);
	public: void add_row_to_and_attack_lv(String^ t, String^ name, String^ value);
	private: void set_status(String^ status);
	private: void set_status(std::string status);
	private: void clear_cm_boxes();
	private: void delete_type(String^ name);
	private: void set_times(double gen_time, double solve_time);
	private: void clear_all_boxes();
	private: sql_wrapper^ wrapper_sql;
	private: xml_wrapper^ wrapper_xml;
	private: System::String^ extract_path = System::IO::Directory::GetCurrentDirectory()+"\\extract folder";
	private: System::String^ db_path = extract_path + "\\facts.db";
	private: System::String^ xml_path = extract_path + "\\scheme.xml";
	private: std::map<std::string, int> *facts_to_block, *attacks_to_block;
	private: System::Windows::Forms::SaveFileDialog^  saveGraphvizFile;
	private: System::Data::DataSet^  dataSet1;
	private: System::Windows::Forms::TabPage^  AttacksTab;
	private: System::Windows::Forms::GroupBox^  ANDAttacksBox;
	private: System::Windows::Forms::Button^  DeleteAndAttackBtn;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Button^  SaveANDAttackBtn;

	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::ComboBox^  AttackFactNamesCmb;
	private: System::Windows::Forms::ListBox^  AttackFactLb;


	private: System::Windows::Forms::Label^  ANDAttackNameLbl;
	private: System::Windows::Forms::ComboBox^  AttackToPcondCmb;
	private: System::Windows::Forms::GroupBox^  ANDAttackSignatureBox;
	private: System::Windows::Forms::Button^  UpdateANDVariableBtn;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::TextBox^  AttackSignValueTxt;
	private: System::Windows::Forms::ListView^  AttackSignLV;
	private: System::Windows::Forms::ColumnHeader^  ANDSignType;
	private: System::Windows::Forms::ColumnHeader^  ANDSignName;
	private: System::Windows::Forms::ColumnHeader^  ANDSignValue;
	private: System::Windows::Forms::Button^  DeleteANDVariableBtn;
	private: System::Windows::Forms::Button^  AddANDVariableBtn;
	private: System::Windows::Forms::Button^  ANDDownVariableBtn;
	private: System::Windows::Forms::Button^  ANDUpVariableBtn;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::ComboBox^  AttackSignTypeCmb;
	private: System::Windows::Forms::TextBox^  AttackSignNameTxt;
	private: System::Windows::Forms::ComboBox^  AttackNames;
	private: System::Windows::Forms::GroupBox^  ORAttacksBox;
	private: System::Windows::Forms::Button^  DeleteORAttackBtn;
	private: System::Windows::Forms::Label^  ORAttackNameLbl;
	private: System::Windows::Forms::GroupBox^  ORAttackSignatureBox;
	private: System::Windows::Forms::ListView^  PcondSignLV;
	private: System::Windows::Forms::ColumnHeader^  ORTypeColumn;
	private: System::Windows::Forms::ColumnHeader^  OrNameColumn;
	private: System::Windows::Forms::Button^  DeleteORVariableBtn;
	private: System::Windows::Forms::Button^  AddORVariableBtn;
	private: System::Windows::Forms::Button^  ORDownVariableBtn;
	private: System::Windows::Forms::Button^  ORUpVariableBtn;
	private: System::Windows::Forms::Label^  OrAttackVarNameLbl;
	private: System::Windows::Forms::Label^  OrAttackValTypeLbl;
	private: System::Windows::Forms::ComboBox^  PcondSignTypeCmb;
	private: System::Windows::Forms::TextBox^  OrAttackSignNameTxt;
	private: System::Windows::Forms::Button^  SaveORAttackBtn;

	private: System::Windows::Forms::ComboBox^  PcondNames;
	private: System::Windows::Forms::Button^  AndFromOrBtn;
	private: System::Windows::Forms::TabPage^  FactsTab;
	private: System::Windows::Forms::SplitContainer^  splitContainer2;
	private: System::Windows::Forms::GroupBox^  FactsProtoBox;
	private: System::Windows::Forms::Button^  DeleteFactBtn;

	private: System::Windows::Forms::ComboBox^  FactProtoNameCmBox;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::GroupBox^  FactSignatureBox;
	private: System::Windows::Forms::ListView^  FactSignatureLV;
	private: System::Windows::Forms::ColumnHeader^  FactTypeColumn;
	private: System::Windows::Forms::ColumnHeader^  FactNameColumn;
	private: System::Windows::Forms::ColumnHeader^  FactGroupColumn;
	private: System::Windows::Forms::Button^  FactDownVariableBtn;
	private: System::Windows::Forms::Button^  FactUpVariableBtn;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::ComboBox^  FactVariableTypesCmBox;
	private: System::Windows::Forms::TextBox^  FactVariableNameTxt;
	private: System::Windows::Forms::TextBox^  FactVariableGroupTxt;
	private: System::Windows::Forms::Button^  DeleteFactVariableBtn;
	private: System::Windows::Forms::Button^  AddFactVariableBtn;
	private: System::Windows::Forms::Button^  SaveFactBtn;
	private: System::Windows::Forms::GroupBox^  FactsBox;
	private: System::Windows::Forms::SplitContainer^  splitContainer1;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Button^  UpdateFacts;
	private: System::Windows::Forms::ComboBox^  FactsNamesCmBox;
	private: System::Windows::Forms::Button^  ShowFactBtn;
	private: System::Windows::Forms::Button^  DeleteDBFactBtn;
	private: System::Windows::Forms::DataGridView^  AllFactsView;
	private: System::Windows::Forms::TabPage^  TypesTab;
	private: System::Windows::Forms::Button^  DeleteTypeBtn;
	private: System::Windows::Forms::Button^  UpdateTypeBtn;
	private: System::Windows::Forms::ListBox^  TypesLbx;
	private: System::Windows::Forms::Button^  AddTypeBtn;
	private: System::Windows::Forms::Label^  TypeNameLb;
	private: System::Windows::Forms::TextBox^  TypeTextBox;
	private: System::Windows::Forms::TabPage^  MainTab;
	private: System::Windows::Forms::RichTextBox^  CNFTextBox;
	private: System::Windows::Forms::Label^  SolvingTimeLbl;
	private: System::Windows::Forms::Label^  SolvingTimeNameLbl;
	private: System::Windows::Forms::Label^  GenTimeLbl;
	private: System::Windows::Forms::Label^  GenTimeNameLbl;
	private: System::Windows::Forms::Label^  StatusLbl;
	private: System::Windows::Forms::Label^  StatusNameLbl;
	private: System::Windows::Forms::Button^  LoadAndCalculate;
	private: System::Windows::Forms::Button^  WriteCnf;
private: System::Windows::Forms::Button^ SaveGraphvizButton;

	private: System::Windows::Forms::Button^  CalculateButton;
	private: System::Windows::Forms::TabControl^  SystemTab;
private: System::Windows::Forms::ListView^  AttackFactValuesLv;

	private: System::Windows::Forms::ColumnHeader^  columnHeader2;
private: System::Windows::Forms::ListView^  AttackFactNamesLv;


	private: System::Windows::Forms::ColumnHeader^  columnHeader1;
	private: System::Windows::Forms::Label^  label7;


	private: System::Windows::Forms::Button^  ANDAttackDeleteFactBtn;
	private: System::Windows::Forms::Button^  ANDAttackAddFactDtn;
	private: SQLiteDataAdapter ^ ad;
	
	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MainForm()
		{
			if (components)
			{
				delete components;
			}
			clear_extract_folder();
		}

	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  openToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  saveToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  aboutToolStripMenuItem;
	private: System::Windows::Forms::OpenFileDialog^  openFile;
	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->newToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->openToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->saveToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->importToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->opt1ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->aboutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->openFile = (gcnew System::Windows::Forms::OpenFileDialog());
			this->saveGraphvizFile = (gcnew System::Windows::Forms::SaveFileDialog());
			this->dataSet1 = (gcnew System::Data::DataSet());
			this->AttacksTab = (gcnew System::Windows::Forms::TabPage());
			this->ANDAttacksBox = (gcnew System::Windows::Forms::GroupBox());
			this->DeleteAndAttackBtn = (gcnew System::Windows::Forms::Button());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->SaveANDAttackBtn = (gcnew System::Windows::Forms::Button());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->ANDAttackUpdateFactBtn = (gcnew System::Windows::Forms::Button());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->ANDAttackDeleteFactBtn = (gcnew System::Windows::Forms::Button());
			this->ANDAttackAddFactDtn = (gcnew System::Windows::Forms::Button());
			this->AttackFactValuesLv = (gcnew System::Windows::Forms::ListView());
			this->columnHeader2 = (gcnew System::Windows::Forms::ColumnHeader());
			this->AttackFactNamesLv = (gcnew System::Windows::Forms::ListView());
			this->columnHeader1 = (gcnew System::Windows::Forms::ColumnHeader());
			this->AttackFactNamesCmb = (gcnew System::Windows::Forms::ComboBox());
			this->AttackFactLb = (gcnew System::Windows::Forms::ListBox());
			this->ANDAttackNameLbl = (gcnew System::Windows::Forms::Label());
			this->AttackToPcondCmb = (gcnew System::Windows::Forms::ComboBox());
			this->ANDAttackSignatureBox = (gcnew System::Windows::Forms::GroupBox());
			this->UpdateANDVariableBtn = (gcnew System::Windows::Forms::Button());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->AttackSignValueTxt = (gcnew System::Windows::Forms::TextBox());
			this->AttackSignLV = (gcnew System::Windows::Forms::ListView());
			this->ANDSignType = (gcnew System::Windows::Forms::ColumnHeader());
			this->ANDSignName = (gcnew System::Windows::Forms::ColumnHeader());
			this->ANDSignValue = (gcnew System::Windows::Forms::ColumnHeader());
			this->DeleteANDVariableBtn = (gcnew System::Windows::Forms::Button());
			this->AddANDVariableBtn = (gcnew System::Windows::Forms::Button());
			this->ANDDownVariableBtn = (gcnew System::Windows::Forms::Button());
			this->ANDUpVariableBtn = (gcnew System::Windows::Forms::Button());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->AttackSignTypeCmb = (gcnew System::Windows::Forms::ComboBox());
			this->AttackSignNameTxt = (gcnew System::Windows::Forms::TextBox());
			this->AttackNames = (gcnew System::Windows::Forms::ComboBox());
			this->ORAttacksBox = (gcnew System::Windows::Forms::GroupBox());
			this->DeleteORAttackBtn = (gcnew System::Windows::Forms::Button());
			this->ORAttackNameLbl = (gcnew System::Windows::Forms::Label());
			this->ORAttackSignatureBox = (gcnew System::Windows::Forms::GroupBox());
			this->PcondSignLV = (gcnew System::Windows::Forms::ListView());
			this->ORTypeColumn = (gcnew System::Windows::Forms::ColumnHeader());
			this->OrNameColumn = (gcnew System::Windows::Forms::ColumnHeader());
			this->DeleteORVariableBtn = (gcnew System::Windows::Forms::Button());
			this->AddORVariableBtn = (gcnew System::Windows::Forms::Button());
			this->ORDownVariableBtn = (gcnew System::Windows::Forms::Button());
			this->ORUpVariableBtn = (gcnew System::Windows::Forms::Button());
			this->OrAttackVarNameLbl = (gcnew System::Windows::Forms::Label());
			this->OrAttackValTypeLbl = (gcnew System::Windows::Forms::Label());
			this->PcondSignTypeCmb = (gcnew System::Windows::Forms::ComboBox());
			this->OrAttackSignNameTxt = (gcnew System::Windows::Forms::TextBox());
			this->SaveORAttackBtn = (gcnew System::Windows::Forms::Button());
			this->PcondNames = (gcnew System::Windows::Forms::ComboBox());
			this->AndFromOrBtn = (gcnew System::Windows::Forms::Button());
			this->FactsTab = (gcnew System::Windows::Forms::TabPage());
			this->splitContainer2 = (gcnew System::Windows::Forms::SplitContainer());
			this->FactsProtoBox = (gcnew System::Windows::Forms::GroupBox());
			this->DeleteFactBtn = (gcnew System::Windows::Forms::Button());
			this->FactProtoNameCmBox = (gcnew System::Windows::Forms::ComboBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->FactSignatureBox = (gcnew System::Windows::Forms::GroupBox());
			this->FactSignatureLV = (gcnew System::Windows::Forms::ListView());
			this->FactTypeColumn = (gcnew System::Windows::Forms::ColumnHeader());
			this->FactNameColumn = (gcnew System::Windows::Forms::ColumnHeader());
			this->FactGroupColumn = (gcnew System::Windows::Forms::ColumnHeader());
			this->FactDownVariableBtn = (gcnew System::Windows::Forms::Button());
			this->FactUpVariableBtn = (gcnew System::Windows::Forms::Button());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->FactVariableTypesCmBox = (gcnew System::Windows::Forms::ComboBox());
			this->FactVariableNameTxt = (gcnew System::Windows::Forms::TextBox());
			this->FactVariableGroupTxt = (gcnew System::Windows::Forms::TextBox());
			this->DeleteFactVariableBtn = (gcnew System::Windows::Forms::Button());
			this->AddFactVariableBtn = (gcnew System::Windows::Forms::Button());
			this->SaveFactBtn = (gcnew System::Windows::Forms::Button());
			this->FactsBox = (gcnew System::Windows::Forms::GroupBox());
			this->splitContainer1 = (gcnew System::Windows::Forms::SplitContainer());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->UpdateFacts = (gcnew System::Windows::Forms::Button());
			this->FactsNamesCmBox = (gcnew System::Windows::Forms::ComboBox());
			this->ShowFactBtn = (gcnew System::Windows::Forms::Button());
			this->DeleteDBFactBtn = (gcnew System::Windows::Forms::Button());
			this->AllFactsView = (gcnew System::Windows::Forms::DataGridView());
			this->TypesTab = (gcnew System::Windows::Forms::TabPage());
			this->DeleteTypeBtn = (gcnew System::Windows::Forms::Button());
			this->UpdateTypeBtn = (gcnew System::Windows::Forms::Button());
			this->TypesLbx = (gcnew System::Windows::Forms::ListBox());
			this->AddTypeBtn = (gcnew System::Windows::Forms::Button());
			this->TypeNameLb = (gcnew System::Windows::Forms::Label());
			this->TypeTextBox = (gcnew System::Windows::Forms::TextBox());
			this->MainTab = (gcnew System::Windows::Forms::TabPage());
			this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
			this->SaveGraphvizButton = (gcnew System::Windows::Forms::Button());
			this->WriteGraphvizBtn = (gcnew System::Windows::Forms::Button());
			this->WriteCnf = (gcnew System::Windows::Forms::Button());
			this->SolvingTimeLbl = (gcnew System::Windows::Forms::Label());
			this->ScissoringSwotcher = (gcnew System::Windows::Forms::GroupBox());
			this->AtScissoring = (gcnew System::Windows::Forms::RadioButton());
			this->PostScissoring = (gcnew System::Windows::Forms::RadioButton());
			this->SolvingTimeNameLbl = (gcnew System::Windows::Forms::Label());
			this->StatusNameLbl = (gcnew System::Windows::Forms::Label());
			this->GenTimeLbl = (gcnew System::Windows::Forms::Label());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->CalculateButton = (gcnew System::Windows::Forms::Button());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->StatusLbl = (gcnew System::Windows::Forms::Label());
			this->GenTimeNameLbl = (gcnew System::Windows::Forms::Label());
			this->CNFTextBox = (gcnew System::Windows::Forms::RichTextBox());
			this->LoadAndCalculate = (gcnew System::Windows::Forms::Button());
			this->SystemTab = (gcnew System::Windows::Forms::TabControl());
			this->BlockingTab = (gcnew System::Windows::Forms::TabPage());
			this->TimeoutTxtBox = (gcnew System::Windows::Forms::TextBox());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->showPatchesBtn = (gcnew System::Windows::Forms::Button());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->AllPcondNamesLB = (gcnew System::Windows::Forms::ListBox());
			this->PcondsToBlockLB = (gcnew System::Windows::Forms::ListBox());
			this->AddOrAttackToBlockBtn = (gcnew System::Windows::Forms::Button());
			this->DeleteOrAttackFromBlockBtn = (gcnew System::Windows::Forms::Button());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->AllFactsNamesLB = (gcnew System::Windows::Forms::ListBox());
			this->FactsNamesToBlockLB = (gcnew System::Windows::Forms::ListBox());
			this->AddFactToBlockBtn = (gcnew System::Windows::Forms::Button());
			this->DeleteFactFromBlockBtn = (gcnew System::Windows::Forms::Button());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->UpBlockingBoundaryTxt = (gcnew System::Windows::Forms::TextBox());
			this->LowBlockingBoundaryTxt = (gcnew System::Windows::Forms::TextBox());
			this->NumberOfStepsTxt = (gcnew System::Windows::Forms::TextBox());
			this->WriteGraphvizBlockingBtn = (gcnew System::Windows::Forms::Button());
			this->GetNamesToBlockBtn = (gcnew System::Windows::Forms::Button());
			this->CalcBlocksBtn = (gcnew System::Windows::Forms::Button());
			this->TestingTab = (gcnew System::Windows::Forms::TabPage());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->TimeoutTestTxt = (gcnew System::Windows::Forms::TextBox());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->TestTimesBtn = (gcnew System::Windows::Forms::Button());
			this->TestingBox = (gcnew System::Windows::Forms::RichTextBox());
			this->TestPatchesBtn = (gcnew System::Windows::Forms::Button());
			this->saveFileDialog = (gcnew System::Windows::Forms::SaveFileDialog());
			this->opentTestFiles = (gcnew System::Windows::Forms::OpenFileDialog());
			this->menuStrip1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataSet1))->BeginInit();
			this->AttacksTab->SuspendLayout();
			this->ANDAttacksBox->SuspendLayout();
			this->groupBox1->SuspendLayout();
			this->ANDAttackSignatureBox->SuspendLayout();
			this->ORAttacksBox->SuspendLayout();
			this->ORAttackSignatureBox->SuspendLayout();
			this->FactsTab->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer2))->BeginInit();
			this->splitContainer2->Panel1->SuspendLayout();
			this->splitContainer2->Panel2->SuspendLayout();
			this->splitContainer2->SuspendLayout();
			this->FactsProtoBox->SuspendLayout();
			this->FactSignatureBox->SuspendLayout();
			this->FactsBox->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer1))->BeginInit();
			this->splitContainer1->Panel1->SuspendLayout();
			this->splitContainer1->Panel2->SuspendLayout();
			this->splitContainer1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->AllFactsView))->BeginInit();
			this->TypesTab->SuspendLayout();
			this->MainTab->SuspendLayout();
			this->groupBox5->SuspendLayout();
			this->ScissoringSwotcher->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->SystemTab->SuspendLayout();
			this->BlockingTab->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->TestingTab->SuspendLayout();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->fileToolStripMenuItem,
					this->aboutToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(870, 24);
			this->menuStrip1->TabIndex = 1;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {
				this->newToolStripMenuItem,
					this->openToolStripMenuItem, this->saveToolStripMenuItem, this->importToolStripMenuItem
			});
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(37, 20);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// newToolStripMenuItem
			// 
			this->newToolStripMenuItem->Name = L"newToolStripMenuItem";
			this->newToolStripMenuItem->Size = System::Drawing::Size(110, 22);
			this->newToolStripMenuItem->Text = L"New";
			this->newToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainForm::newToolStripMenuItem_Click);
			// 
			// openToolStripMenuItem
			// 
			this->openToolStripMenuItem->Name = L"openToolStripMenuItem";
			this->openToolStripMenuItem->Size = System::Drawing::Size(110, 22);
			this->openToolStripMenuItem->Text = L"Open";
			this->openToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainForm::openToolStripMenuItem_Click);
			// 
			// saveToolStripMenuItem
			// 
			this->saveToolStripMenuItem->Name = L"saveToolStripMenuItem";
			this->saveToolStripMenuItem->Size = System::Drawing::Size(110, 22);
			this->saveToolStripMenuItem->Text = L"Save";
			this->saveToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainForm::saveToolStripMenuItem_Click);
			// 
			// importToolStripMenuItem
			// 
			this->importToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->opt1ToolStripMenuItem });
			this->importToolStripMenuItem->Enabled = false;
			this->importToolStripMenuItem->Name = L"importToolStripMenuItem";
			this->importToolStripMenuItem->Size = System::Drawing::Size(110, 22);
			this->importToolStripMenuItem->Text = L"Import";
			// 
			// opt1ToolStripMenuItem
			// 
			this->opt1ToolStripMenuItem->Name = L"opt1ToolStripMenuItem";
			this->opt1ToolStripMenuItem->Size = System::Drawing::Size(100, 22);
			this->opt1ToolStripMenuItem->Text = L"Opt1";
			// 
			// aboutToolStripMenuItem
			// 
			this->aboutToolStripMenuItem->Name = L"aboutToolStripMenuItem";
			this->aboutToolStripMenuItem->Size = System::Drawing::Size(52, 20);
			this->aboutToolStripMenuItem->Text = L"About";
			// 
			// openFile
			// 
			this->openFile->Filter = L"ZIP archive|*.zip";
			// 
			// saveGraphvizFile
			// 
			this->saveGraphvizFile->FileName = L"Attack graph";
			this->saveGraphvizFile->Filter = L"Graphviz file|*.dot";
			// 
			// dataSet1
			// 
			this->dataSet1->DataSetName = L"NewDataSet";
			// 
			// AttacksTab
			// 
			this->AttacksTab->Controls->Add(this->ANDAttacksBox);
			this->AttacksTab->Controls->Add(this->ORAttacksBox);
			this->AttacksTab->Controls->Add(this->AndFromOrBtn);
			this->AttacksTab->Location = System::Drawing::Point(4, 22);
			this->AttacksTab->Name = L"AttacksTab";
			this->AttacksTab->Size = System::Drawing::Size(862, 450);
			this->AttacksTab->TabIndex = 2;
			this->AttacksTab->Text = L"Attacks";
			this->AttacksTab->UseVisualStyleBackColor = true;
			// 
			// ANDAttacksBox
			// 
			this->ANDAttacksBox->Controls->Add(this->DeleteAndAttackBtn);
			this->ANDAttacksBox->Controls->Add(this->label6);
			this->ANDAttacksBox->Controls->Add(this->SaveANDAttackBtn);
			this->ANDAttacksBox->Controls->Add(this->groupBox1);
			this->ANDAttacksBox->Controls->Add(this->ANDAttackNameLbl);
			this->ANDAttacksBox->Controls->Add(this->AttackToPcondCmb);
			this->ANDAttacksBox->Controls->Add(this->ANDAttackSignatureBox);
			this->ANDAttacksBox->Controls->Add(this->AttackNames);
			this->ANDAttacksBox->Location = System::Drawing::Point(311, 3);
			this->ANDAttacksBox->Name = L"ANDAttacksBox";
			this->ANDAttacksBox->Size = System::Drawing::Size(543, 441);
			this->ANDAttacksBox->TabIndex = 4;
			this->ANDAttacksBox->TabStop = false;
			this->ANDAttacksBox->Text = L"Attacks";
			// 
			// DeleteAndAttackBtn
			// 
			this->DeleteAndAttackBtn->Location = System::Drawing::Point(167, 406);
			this->DeleteAndAttackBtn->Name = L"DeleteAndAttackBtn";
			this->DeleteAndAttackBtn->Size = System::Drawing::Size(218, 23);
			this->DeleteAndAttackBtn->TabIndex = 30;
			this->DeleteAndAttackBtn->Text = L"Delete attack";
			this->DeleteAndAttackBtn->UseVisualStyleBackColor = true;
			this->DeleteAndAttackBtn->Click += gcnew System::EventHandler(this, &MainForm::DeleteAttackBtn_Click);
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(13, 47);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(71, 13);
			this->label6->TabIndex = 16;
			this->label6->Text = L"Postcondition";
			// 
			// SaveANDAttackBtn
			// 
			this->SaveANDAttackBtn->Location = System::Drawing::Point(167, 373);
			this->SaveANDAttackBtn->Name = L"SaveANDAttackBtn";
			this->SaveANDAttackBtn->Size = System::Drawing::Size(218, 23);
			this->SaveANDAttackBtn->TabIndex = 29;
			this->SaveANDAttackBtn->Text = L"Save attack";
			this->SaveANDAttackBtn->UseVisualStyleBackColor = true;
			this->SaveANDAttackBtn->Click += gcnew System::EventHandler(this, &MainForm::SaveAttackBtn_Click);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->ANDAttackUpdateFactBtn);
			this->groupBox1->Controls->Add(this->label7);
			this->groupBox1->Controls->Add(this->ANDAttackDeleteFactBtn);
			this->groupBox1->Controls->Add(this->ANDAttackAddFactDtn);
			this->groupBox1->Controls->Add(this->AttackFactValuesLv);
			this->groupBox1->Controls->Add(this->AttackFactNamesLv);
			this->groupBox1->Controls->Add(this->AttackFactNamesCmb);
			this->groupBox1->Controls->Add(this->AttackFactLb);
			this->groupBox1->Location = System::Drawing::Point(287, 18);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(247, 346);
			this->groupBox1->TabIndex = 15;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Preconditions";
			// 
			// ANDAttackUpdateFactBtn
			// 
			this->ANDAttackUpdateFactBtn->Location = System::Drawing::Point(23, 179);
			this->ANDAttackUpdateFactBtn->Name = L"ANDAttackUpdateFactBtn";
			this->ANDAttackUpdateFactBtn->Size = System::Drawing::Size(207, 23);
			this->ANDAttackUpdateFactBtn->TabIndex = 26;
			this->ANDAttackUpdateFactBtn->Text = L"Update Precondition";
			this->ANDAttackUpdateFactBtn->UseVisualStyleBackColor = true;
			this->ANDAttackUpdateFactBtn->Click += gcnew System::EventHandler(this, &MainForm::AttackUpdateFactBtn_Click);
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(20, 25);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(35, 13);
			this->label7->TabIndex = 19;
			this->label7->Text = L"Name";
			// 
			// ANDAttackDeleteFactBtn
			// 
			this->ANDAttackDeleteFactBtn->Location = System::Drawing::Point(23, 312);
			this->ANDAttackDeleteFactBtn->Name = L"ANDAttackDeleteFactBtn";
			this->ANDAttackDeleteFactBtn->Size = System::Drawing::Size(207, 23);
			this->ANDAttackDeleteFactBtn->TabIndex = 28;
			this->ANDAttackDeleteFactBtn->Text = L"Delete Precondition";
			this->ANDAttackDeleteFactBtn->UseVisualStyleBackColor = true;
			this->ANDAttackDeleteFactBtn->Click += gcnew System::EventHandler(this, &MainForm::AttackDeleteFactBtn_Click);
			// 
			// ANDAttackAddFactDtn
			// 
			this->ANDAttackAddFactDtn->Location = System::Drawing::Point(23, 150);
			this->ANDAttackAddFactDtn->Name = L"ANDAttackAddFactDtn";
			this->ANDAttackAddFactDtn->Size = System::Drawing::Size(207, 23);
			this->ANDAttackAddFactDtn->TabIndex = 25;
			this->ANDAttackAddFactDtn->Text = L"Add Precondition";
			this->ANDAttackAddFactDtn->UseVisualStyleBackColor = true;
			this->ANDAttackAddFactDtn->Click += gcnew System::EventHandler(this, &MainForm::AttackAddFactBtn_Click);
			// 
			// AttackFactValuesLv
			// 
			this->AttackFactValuesLv->Columns->AddRange(gcnew cli::array< System::Windows::Forms::ColumnHeader^  >(1) { this->columnHeader2 });
			this->AttackFactValuesLv->GridLines = true;
			this->AttackFactValuesLv->HideSelection = false;
			this->AttackFactValuesLv->LabelEdit = true;
			this->AttackFactValuesLv->Location = System::Drawing::Point(134, 51);
			this->AttackFactValuesLv->MultiSelect = false;
			this->AttackFactValuesLv->Name = L"AttackFactValuesLv";
			this->AttackFactValuesLv->Size = System::Drawing::Size(96, 93);
			this->AttackFactValuesLv->TabIndex = 24;
			this->AttackFactValuesLv->UseCompatibleStateImageBehavior = false;
			this->AttackFactValuesLv->View = System::Windows::Forms::View::Details;
			this->AttackFactValuesLv->SelectedIndexChanged += gcnew System::EventHandler(this, &MainForm::AttackFactValuesLv_SelectedIndexChanged);
			// 
			// columnHeader2
			// 
			this->columnHeader2->Text = L"Value";
			// 
			// AttackFactNamesLv
			// 
			this->AttackFactNamesLv->Columns->AddRange(gcnew cli::array< System::Windows::Forms::ColumnHeader^  >(1) { this->columnHeader1 });
			this->AttackFactNamesLv->GridLines = true;
			this->AttackFactNamesLv->HideSelection = false;
			this->AttackFactNamesLv->Location = System::Drawing::Point(23, 51);
			this->AttackFactNamesLv->MultiSelect = false;
			this->AttackFactNamesLv->Name = L"AttackFactNamesLv";
			this->AttackFactNamesLv->Size = System::Drawing::Size(92, 93);
			this->AttackFactNamesLv->TabIndex = 23;
			this->AttackFactNamesLv->UseCompatibleStateImageBehavior = false;
			this->AttackFactNamesLv->View = System::Windows::Forms::View::Details;
			// 
			// columnHeader1
			// 
			this->columnHeader1->Text = L"Name";
			// 
			// AttackFactNamesCmb
			// 
			this->AttackFactNamesCmb->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->AttackFactNamesCmb->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
			this->AttackFactNamesCmb->FormattingEnabled = true;
			this->AttackFactNamesCmb->Location = System::Drawing::Point(109, 22);
			this->AttackFactNamesCmb->Name = L"AttackFactNamesCmb";
			this->AttackFactNamesCmb->Size = System::Drawing::Size(121, 21);
			this->AttackFactNamesCmb->TabIndex = 22;
			this->AttackFactNamesCmb->SelectedIndexChanged += gcnew System::EventHandler(this, &MainForm::AttackFactNamesCmb_SelectedIndexChanged);
			// 
			// AttackFactLb
			// 
			this->AttackFactLb->FormattingEnabled = true;
			this->AttackFactLb->Location = System::Drawing::Point(23, 209);
			this->AttackFactLb->Name = L"AttackFactLb";
			this->AttackFactLb->Size = System::Drawing::Size(207, 95);
			this->AttackFactLb->TabIndex = 27;
			this->AttackFactLb->SelectedIndexChanged += gcnew System::EventHandler(this, &MainForm::AttackFactLb_SelectedIndexChanged);
			// 
			// ANDAttackNameLbl
			// 
			this->ANDAttackNameLbl->AutoSize = true;
			this->ANDAttackNameLbl->Location = System::Drawing::Point(13, 18);
			this->ANDAttackNameLbl->Name = L"ANDAttackNameLbl";
			this->ANDAttackNameLbl->Size = System::Drawing::Size(35, 13);
			this->ANDAttackNameLbl->TabIndex = 14;
			this->ANDAttackNameLbl->Text = L"Name";
			// 
			// AttackToPcondCmb
			// 
			this->AttackToPcondCmb->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->AttackToPcondCmb->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
			this->AttackToPcondCmb->FormattingEnabled = true;
			this->AttackToPcondCmb->Location = System::Drawing::Point(113, 42);
			this->AttackToPcondCmb->Name = L"AttackToPcondCmb";
			this->AttackToPcondCmb->Size = System::Drawing::Size(121, 21);
			this->AttackToPcondCmb->TabIndex = 12;
			// 
			// ANDAttackSignatureBox
			// 
			this->ANDAttackSignatureBox->Controls->Add(this->UpdateANDVariableBtn);
			this->ANDAttackSignatureBox->Controls->Add(this->label5);
			this->ANDAttackSignatureBox->Controls->Add(this->AttackSignValueTxt);
			this->ANDAttackSignatureBox->Controls->Add(this->AttackSignLV);
			this->ANDAttackSignatureBox->Controls->Add(this->DeleteANDVariableBtn);
			this->ANDAttackSignatureBox->Controls->Add(this->AddANDVariableBtn);
			this->ANDAttackSignatureBox->Controls->Add(this->ANDDownVariableBtn);
			this->ANDAttackSignatureBox->Controls->Add(this->ANDUpVariableBtn);
			this->ANDAttackSignatureBox->Controls->Add(this->label9);
			this->ANDAttackSignatureBox->Controls->Add(this->label10);
			this->ANDAttackSignatureBox->Controls->Add(this->AttackSignTypeCmb);
			this->ANDAttackSignatureBox->Controls->Add(this->AttackSignNameTxt);
			this->ANDAttackSignatureBox->Location = System::Drawing::Point(6, 64);
			this->ANDAttackSignatureBox->Name = L"ANDAttackSignatureBox";
			this->ANDAttackSignatureBox->Size = System::Drawing::Size(246, 300);
			this->ANDAttackSignatureBox->TabIndex = 12;
			this->ANDAttackSignatureBox->TabStop = false;
			this->ANDAttackSignatureBox->Text = L"Signature";
			// 
			// UpdateANDVariableBtn
			// 
			this->UpdateANDVariableBtn->Location = System::Drawing::Point(10, 127);
			this->UpdateANDVariableBtn->Name = L"UpdateANDVariableBtn";
			this->UpdateANDVariableBtn->Size = System::Drawing::Size(218, 23);
			this->UpdateANDVariableBtn->TabIndex = 17;
			this->UpdateANDVariableBtn->Text = L"Update variable";
			this->UpdateANDVariableBtn->UseVisualStyleBackColor = true;
			this->UpdateANDVariableBtn->Click += gcnew System::EventHandler(this, &MainForm::UpdateAttackVariableBtn_Click);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(7, 79);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(34, 13);
			this->label5->TabIndex = 15;
			this->label5->Text = L"Value";
			// 
			// AttackSignValueTxt
			// 
			this->AttackSignValueTxt->Location = System::Drawing::Point(107, 72);
			this->AttackSignValueTxt->Name = L"AttackSignValueTxt";
			this->AttackSignValueTxt->Size = System::Drawing::Size(121, 20);
			this->AttackSignValueTxt->TabIndex = 15;
			// 
			// AttackSignLV
			// 
			this->AttackSignLV->Columns->AddRange(gcnew cli::array< System::Windows::Forms::ColumnHeader^  >(3) {
				this->ANDSignType, this->ANDSignName,
					this->ANDSignValue
			});
			this->AttackSignLV->FullRowSelect = true;
			this->AttackSignLV->GridLines = true;
			this->AttackSignLV->HideSelection = false;
			this->AttackSignLV->Location = System::Drawing::Point(10, 157);
			this->AttackSignLV->MultiSelect = false;
			this->AttackSignLV->Name = L"AttackSignLV";
			this->AttackSignLV->Size = System::Drawing::Size(178, 105);
			this->AttackSignLV->TabIndex = 18;
			this->AttackSignLV->UseCompatibleStateImageBehavior = false;
			this->AttackSignLV->View = System::Windows::Forms::View::Details;
			this->AttackSignLV->SelectedIndexChanged += gcnew System::EventHandler(this, &MainForm::AttackSignLV_SelectedIndexChanged);
			// 
			// ANDSignType
			// 
			this->ANDSignType->Text = L"Type";
			this->ANDSignType->Width = 55;
			// 
			// ANDSignName
			// 
			this->ANDSignName->Text = L"Name";
			this->ANDSignName->Width = 50;
			// 
			// ANDSignValue
			// 
			this->ANDSignValue->Text = L"Value";
			this->ANDSignValue->Width = 50;
			// 
			// DeleteANDVariableBtn
			// 
			this->DeleteANDVariableBtn->Location = System::Drawing::Point(10, 268);
			this->DeleteANDVariableBtn->Name = L"DeleteANDVariableBtn";
			this->DeleteANDVariableBtn->Size = System::Drawing::Size(218, 23);
			this->DeleteANDVariableBtn->TabIndex = 21;
			this->DeleteANDVariableBtn->Text = L"Delete variable";
			this->DeleteANDVariableBtn->UseVisualStyleBackColor = true;
			this->DeleteANDVariableBtn->Click += gcnew System::EventHandler(this, &MainForm::DeleteAttackVariableBtn_Click);
			// 
			// AddANDVariableBtn
			// 
			this->AddANDVariableBtn->Location = System::Drawing::Point(10, 99);
			this->AddANDVariableBtn->Name = L"AddANDVariableBtn";
			this->AddANDVariableBtn->Size = System::Drawing::Size(218, 23);
			this->AddANDVariableBtn->TabIndex = 16;
			this->AddANDVariableBtn->Text = L"Add variable";
			this->AddANDVariableBtn->UseVisualStyleBackColor = true;
			this->AddANDVariableBtn->Click += gcnew System::EventHandler(this, &MainForm::AddAttackVariableBtn_Click);
			// 
			// ANDDownVariableBtn
			// 
			this->ANDDownVariableBtn->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			this->ANDDownVariableBtn->Location = System::Drawing::Point(194, 235);
			this->ANDDownVariableBtn->Name = L"ANDDownVariableBtn";
			this->ANDDownVariableBtn->Size = System::Drawing::Size(34, 27);
			this->ANDDownVariableBtn->TabIndex = 20;
			this->ANDDownVariableBtn->Text = L"↓";
			this->ANDDownVariableBtn->UseVisualStyleBackColor = true;
			this->ANDDownVariableBtn->Click += gcnew System::EventHandler(this, &MainForm::ANDDownVariableBtn_Click);
			// 
			// ANDUpVariableBtn
			// 
			this->ANDUpVariableBtn->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			this->ANDUpVariableBtn->Location = System::Drawing::Point(194, 157);
			this->ANDUpVariableBtn->Name = L"ANDUpVariableBtn";
			this->ANDUpVariableBtn->Size = System::Drawing::Size(34, 27);
			this->ANDUpVariableBtn->TabIndex = 19;
			this->ANDUpVariableBtn->Text = L"↑";
			this->ANDUpVariableBtn->UseVisualStyleBackColor = true;
			this->ANDUpVariableBtn->Click += gcnew System::EventHandler(this, &MainForm::ANDUpVariableBtn_Click);
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(7, 52);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(35, 13);
			this->label9->TabIndex = 5;
			this->label9->Text = L"Name";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(7, 24);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(31, 13);
			this->label10->TabIndex = 4;
			this->label10->Text = L"Type";
			// 
			// AttackSignTypeCmb
			// 
			this->AttackSignTypeCmb->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->AttackSignTypeCmb->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
			this->AttackSignTypeCmb->FormattingEnabled = true;
			this->AttackSignTypeCmb->Location = System::Drawing::Point(107, 17);
			this->AttackSignTypeCmb->Name = L"AttackSignTypeCmb";
			this->AttackSignTypeCmb->Size = System::Drawing::Size(121, 21);
			this->AttackSignTypeCmb->TabIndex = 13;
			// 
			// AttackSignNameTxt
			// 
			this->AttackSignNameTxt->Location = System::Drawing::Point(107, 45);
			this->AttackSignNameTxt->Name = L"AttackSignNameTxt";
			this->AttackSignNameTxt->Size = System::Drawing::Size(121, 20);
			this->AttackSignNameTxt->TabIndex = 14;
			// 
			// AttackNames
			// 
			this->AttackNames->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->AttackNames->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
			this->AttackNames->FormattingEnabled = true;
			this->AttackNames->Location = System::Drawing::Point(113, 15);
			this->AttackNames->Name = L"AttackNames";
			this->AttackNames->Size = System::Drawing::Size(121, 21);
			this->AttackNames->TabIndex = 11;
			this->AttackNames->SelectedIndexChanged += gcnew System::EventHandler(this, &MainForm::AttackNames_SelectedIndexChanged);
			// 
			// ORAttacksBox
			// 
			this->ORAttacksBox->Controls->Add(this->DeleteORAttackBtn);
			this->ORAttacksBox->Controls->Add(this->ORAttackNameLbl);
			this->ORAttacksBox->Controls->Add(this->ORAttackSignatureBox);
			this->ORAttacksBox->Controls->Add(this->SaveORAttackBtn);
			this->ORAttacksBox->Controls->Add(this->PcondNames);
			this->ORAttacksBox->Location = System::Drawing::Point(3, 3);
			this->ORAttacksBox->Name = L"ORAttacksBox";
			this->ORAttacksBox->Size = System::Drawing::Size(268, 441);
			this->ORAttacksBox->TabIndex = 3;
			this->ORAttacksBox->TabStop = false;
			this->ORAttacksBox->Text = L"Postconditions";
			// 
			// DeleteORAttackBtn
			// 
			this->DeleteORAttackBtn->Location = System::Drawing::Point(23, 406);
			this->DeleteORAttackBtn->Name = L"DeleteORAttackBtn";
			this->DeleteORAttackBtn->Size = System::Drawing::Size(222, 23);
			this->DeleteORAttackBtn->TabIndex = 9;
			this->DeleteORAttackBtn->Text = L"Delete postcondition";
			this->DeleteORAttackBtn->UseVisualStyleBackColor = true;
			this->DeleteORAttackBtn->Click += gcnew System::EventHandler(this, &MainForm::DeletePostcondBtn_Click);
			// 
			// ORAttackNameLbl
			// 
			this->ORAttackNameLbl->AutoSize = true;
			this->ORAttackNameLbl->Location = System::Drawing::Point(20, 23);
			this->ORAttackNameLbl->Name = L"ORAttackNameLbl";
			this->ORAttackNameLbl->Size = System::Drawing::Size(71, 13);
			this->ORAttackNameLbl->TabIndex = 5;
			this->ORAttackNameLbl->Text = L"Postcondition";
			// 
			// ORAttackSignatureBox
			// 
			this->ORAttackSignatureBox->Controls->Add(this->PcondSignLV);
			this->ORAttackSignatureBox->Controls->Add(this->DeleteORVariableBtn);
			this->ORAttackSignatureBox->Controls->Add(this->AddORVariableBtn);
			this->ORAttackSignatureBox->Controls->Add(this->ORDownVariableBtn);
			this->ORAttackSignatureBox->Controls->Add(this->ORUpVariableBtn);
			this->ORAttackSignatureBox->Controls->Add(this->OrAttackVarNameLbl);
			this->ORAttackSignatureBox->Controls->Add(this->OrAttackValTypeLbl);
			this->ORAttackSignatureBox->Controls->Add(this->PcondSignTypeCmb);
			this->ORAttackSignatureBox->Controls->Add(this->OrAttackSignNameTxt);
			this->ORAttackSignatureBox->Location = System::Drawing::Point(6, 69);
			this->ORAttackSignatureBox->Name = L"ORAttackSignatureBox";
			this->ORAttackSignatureBox->Size = System::Drawing::Size(246, 295);
			this->ORAttackSignatureBox->TabIndex = 4;
			this->ORAttackSignatureBox->TabStop = false;
			this->ORAttackSignatureBox->Text = L"Signature";
			// 
			// PcondSignLV
			// 
			this->PcondSignLV->Columns->AddRange(gcnew cli::array< System::Windows::Forms::ColumnHeader^  >(2) { this->ORTypeColumn, this->OrNameColumn });
			this->PcondSignLV->FullRowSelect = true;
			this->PcondSignLV->GridLines = true;
			this->PcondSignLV->HideSelection = false;
			this->PcondSignLV->Location = System::Drawing::Point(12, 127);
			this->PcondSignLV->MultiSelect = false;
			this->PcondSignLV->Name = L"PcondSignLV";
			this->PcondSignLV->Size = System::Drawing::Size(158, 118);
			this->PcondSignLV->TabIndex = 4;
			this->PcondSignLV->UseCompatibleStateImageBehavior = false;
			this->PcondSignLV->View = System::Windows::Forms::View::Details;
			// 
			// ORTypeColumn
			// 
			this->ORTypeColumn->Text = L"Type";
			this->ORTypeColumn->Width = 55;
			// 
			// OrNameColumn
			// 
			this->OrNameColumn->Text = L"Name";
			this->OrNameColumn->Width = 55;
			// 
			// DeleteORVariableBtn
			// 
			this->DeleteORVariableBtn->Location = System::Drawing::Point(12, 263);
			this->DeleteORVariableBtn->Name = L"DeleteORVariableBtn";
			this->DeleteORVariableBtn->Size = System::Drawing::Size(222, 23);
			this->DeleteORVariableBtn->TabIndex = 7;
			this->DeleteORVariableBtn->Text = L"Delete variable";
			this->DeleteORVariableBtn->UseVisualStyleBackColor = true;
			this->DeleteORVariableBtn->Click += gcnew System::EventHandler(this, &MainForm::DeletePostcondVariableBtn_Click);
			// 
			// AddORVariableBtn
			// 
			this->AddORVariableBtn->Location = System::Drawing::Point(12, 89);
			this->AddORVariableBtn->Name = L"AddORVariableBtn";
			this->AddORVariableBtn->Size = System::Drawing::Size(222, 23);
			this->AddORVariableBtn->TabIndex = 3;
			this->AddORVariableBtn->Text = L"Add variable";
			this->AddORVariableBtn->UseVisualStyleBackColor = true;
			this->AddORVariableBtn->Click += gcnew System::EventHandler(this, &MainForm::AddPostcondVariableBtn_Click);
			// 
			// ORDownVariableBtn
			// 
			this->ORDownVariableBtn->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			this->ORDownVariableBtn->Location = System::Drawing::Point(199, 218);
			this->ORDownVariableBtn->Name = L"ORDownVariableBtn";
			this->ORDownVariableBtn->Size = System::Drawing::Size(35, 27);
			this->ORDownVariableBtn->TabIndex = 6;
			this->ORDownVariableBtn->Text = L"↓";
			this->ORDownVariableBtn->UseVisualStyleBackColor = true;
			this->ORDownVariableBtn->Click += gcnew System::EventHandler(this, &MainForm::ORDownVariableBtn_Click);
			// 
			// ORUpVariableBtn
			// 
			this->ORUpVariableBtn->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			this->ORUpVariableBtn->Location = System::Drawing::Point(199, 127);
			this->ORUpVariableBtn->Name = L"ORUpVariableBtn";
			this->ORUpVariableBtn->Size = System::Drawing::Size(35, 27);
			this->ORUpVariableBtn->TabIndex = 5;
			this->ORUpVariableBtn->Text = L"↑";
			this->ORUpVariableBtn->UseVisualStyleBackColor = true;
			this->ORUpVariableBtn->Click += gcnew System::EventHandler(this, &MainForm::ORUpVariableBtn_Click);
			// 
			// OrAttackVarNameLbl
			// 
			this->OrAttackVarNameLbl->AutoSize = true;
			this->OrAttackVarNameLbl->Location = System::Drawing::Point(9, 63);
			this->OrAttackVarNameLbl->Name = L"OrAttackVarNameLbl";
			this->OrAttackVarNameLbl->Size = System::Drawing::Size(35, 13);
			this->OrAttackVarNameLbl->TabIndex = 5;
			this->OrAttackVarNameLbl->Text = L"Name";
			// 
			// OrAttackValTypeLbl
			// 
			this->OrAttackValTypeLbl->AutoSize = true;
			this->OrAttackValTypeLbl->Location = System::Drawing::Point(9, 30);
			this->OrAttackValTypeLbl->Name = L"OrAttackValTypeLbl";
			this->OrAttackValTypeLbl->Size = System::Drawing::Size(31, 13);
			this->OrAttackValTypeLbl->TabIndex = 4;
			this->OrAttackValTypeLbl->Text = L"Type";
			// 
			// PcondSignTypeCmb
			// 
			this->PcondSignTypeCmb->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->PcondSignTypeCmb->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
			this->PcondSignTypeCmb->FormattingEnabled = true;
			this->PcondSignTypeCmb->Location = System::Drawing::Point(102, 22);
			this->PcondSignTypeCmb->Name = L"PcondSignTypeCmb";
			this->PcondSignTypeCmb->Size = System::Drawing::Size(132, 21);
			this->PcondSignTypeCmb->TabIndex = 1;
			// 
			// OrAttackSignNameTxt
			// 
			this->OrAttackSignNameTxt->Location = System::Drawing::Point(102, 56);
			this->OrAttackSignNameTxt->Name = L"OrAttackSignNameTxt";
			this->OrAttackSignNameTxt->Size = System::Drawing::Size(132, 20);
			this->OrAttackSignNameTxt->TabIndex = 2;
			// 
			// SaveORAttackBtn
			// 
			this->SaveORAttackBtn->Location = System::Drawing::Point(23, 373);
			this->SaveORAttackBtn->Name = L"SaveORAttackBtn";
			this->SaveORAttackBtn->Size = System::Drawing::Size(222, 23);
			this->SaveORAttackBtn->TabIndex = 8;
			this->SaveORAttackBtn->Text = L"Save postcondition";
			this->SaveORAttackBtn->UseVisualStyleBackColor = true;
			this->SaveORAttackBtn->Click += gcnew System::EventHandler(this, &MainForm::SavePostcondkBtn_Click);
			// 
			// PcondNames
			// 
			this->PcondNames->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->PcondNames->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
			this->PcondNames->FormattingEnabled = true;
			this->PcondNames->Location = System::Drawing::Point(113, 20);
			this->PcondNames->Name = L"PcondNames";
			this->PcondNames->Size = System::Drawing::Size(132, 21);
			this->PcondNames->TabIndex = 0;
			this->PcondNames->SelectedIndexChanged += gcnew System::EventHandler(this, &MainForm::PostcondNames_SelectedIndexChanged);
			// 
			// AndFromOrBtn
			// 
			this->AndFromOrBtn->Location = System::Drawing::Point(277, 164);
			this->AndFromOrBtn->Name = L"AndFromOrBtn";
			this->AndFromOrBtn->Size = System::Drawing::Size(28, 81);
			this->AndFromOrBtn->TabIndex = 10;
			this->AndFromOrBtn->Text = L">>";
			this->AndFromOrBtn->UseVisualStyleBackColor = true;
			this->AndFromOrBtn->Click += gcnew System::EventHandler(this, &MainForm::AttackFromPostcondBtn_Click);
			// 
			// FactsTab
			// 
			this->FactsTab->Controls->Add(this->splitContainer2);
			this->FactsTab->Location = System::Drawing::Point(4, 22);
			this->FactsTab->Name = L"FactsTab";
			this->FactsTab->Padding = System::Windows::Forms::Padding(3);
			this->FactsTab->Size = System::Drawing::Size(862, 450);
			this->FactsTab->TabIndex = 1;
			this->FactsTab->Text = L"Facts";
			this->FactsTab->UseVisualStyleBackColor = true;
			// 
			// splitContainer2
			// 
			this->splitContainer2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->splitContainer2->FixedPanel = System::Windows::Forms::FixedPanel::Panel1;
			this->splitContainer2->Location = System::Drawing::Point(3, 3);
			this->splitContainer2->Name = L"splitContainer2";
			// 
			// splitContainer2.Panel1
			// 
			this->splitContainer2->Panel1->Controls->Add(this->FactsProtoBox);
			// 
			// splitContainer2.Panel2
			// 
			this->splitContainer2->Panel2->Controls->Add(this->FactsBox);
			this->splitContainer2->Size = System::Drawing::Size(856, 444);
			this->splitContainer2->SplitterDistance = 269;
			this->splitContainer2->TabIndex = 14;
			this->splitContainer2->TabStop = false;
			// 
			// FactsProtoBox
			// 
			this->FactsProtoBox->Controls->Add(this->DeleteFactBtn);
			this->FactsProtoBox->Controls->Add(this->FactProtoNameCmBox);
			this->FactsProtoBox->Controls->Add(this->label2);
			this->FactsProtoBox->Controls->Add(this->FactSignatureBox);
			this->FactsProtoBox->Controls->Add(this->SaveFactBtn);
			this->FactsProtoBox->Dock = System::Windows::Forms::DockStyle::Fill;
			this->FactsProtoBox->Location = System::Drawing::Point(0, 0);
			this->FactsProtoBox->Name = L"FactsProtoBox";
			this->FactsProtoBox->Size = System::Drawing::Size(269, 444);
			this->FactsProtoBox->TabIndex = 0;
			this->FactsProtoBox->TabStop = false;
			this->FactsProtoBox->Text = L"Facts templates";
			// 
			// DeleteFactBtn
			// 
			this->DeleteFactBtn->Location = System::Drawing::Point(23, 399);
			this->DeleteFactBtn->Name = L"DeleteFactBtn";
			this->DeleteFactBtn->Size = System::Drawing::Size(223, 23);
			this->DeleteFactBtn->TabIndex = 13;
			this->DeleteFactBtn->Text = L"Delete Fact Prototype";
			this->DeleteFactBtn->UseVisualStyleBackColor = true;
			this->DeleteFactBtn->Click += gcnew System::EventHandler(this, &MainForm::DeleteFactBtn_Click);
			// 
			// FactProtoNameCmBox
			// 
			this->FactProtoNameCmBox->FormattingEnabled = true;
			this->FactProtoNameCmBox->Location = System::Drawing::Point(113, 20);
			this->FactProtoNameCmBox->Name = L"FactProtoNameCmBox";
			this->FactProtoNameCmBox->Size = System::Drawing::Size(132, 21);
			this->FactProtoNameCmBox->TabIndex = 1;
			this->FactProtoNameCmBox->SelectedIndexChanged += gcnew System::EventHandler(this, &MainForm::FactProtoNameCmBox_SelectedIndexChanged);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(20, 23);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(57, 13);
			this->label2->TabIndex = 15;
			this->label2->Text = L"Fact name";
			// 
			// FactSignatureBox
			// 
			this->FactSignatureBox->Controls->Add(this->FactSignatureLV);
			this->FactSignatureBox->Controls->Add(this->FactDownVariableBtn);
			this->FactSignatureBox->Controls->Add(this->FactUpVariableBtn);
			this->FactSignatureBox->Controls->Add(this->label4);
			this->FactSignatureBox->Controls->Add(this->label3);
			this->FactSignatureBox->Controls->Add(this->label8);
			this->FactSignatureBox->Controls->Add(this->FactVariableTypesCmBox);
			this->FactSignatureBox->Controls->Add(this->FactVariableNameTxt);
			this->FactSignatureBox->Controls->Add(this->FactVariableGroupTxt);
			this->FactSignatureBox->Controls->Add(this->DeleteFactVariableBtn);
			this->FactSignatureBox->Controls->Add(this->AddFactVariableBtn);
			this->FactSignatureBox->Location = System::Drawing::Point(11, 49);
			this->FactSignatureBox->Name = L"FactSignatureBox";
			this->FactSignatureBox->Size = System::Drawing::Size(246, 298);
			this->FactSignatureBox->TabIndex = 2;
			this->FactSignatureBox->TabStop = false;
			this->FactSignatureBox->Text = L"Signature";
			// 
			// FactSignatureLV
			// 
			this->FactSignatureLV->Columns->AddRange(gcnew cli::array< System::Windows::Forms::ColumnHeader^  >(3) {
				this->FactTypeColumn,
					this->FactNameColumn, this->FactGroupColumn
			});
			this->FactSignatureLV->FullRowSelect = true;
			this->FactSignatureLV->GridLines = true;
			this->FactSignatureLV->HideSelection = false;
			this->FactSignatureLV->Location = System::Drawing::Point(12, 130);
			this->FactSignatureLV->MultiSelect = false;
			this->FactSignatureLV->Name = L"FactSignatureLV";
			this->FactSignatureLV->Size = System::Drawing::Size(189, 131);
			this->FactSignatureLV->TabIndex = 7;
			this->FactSignatureLV->UseCompatibleStateImageBehavior = false;
			this->FactSignatureLV->View = System::Windows::Forms::View::Details;
			// 
			// FactTypeColumn
			// 
			this->FactTypeColumn->Text = L"Type";
			this->FactTypeColumn->Width = 55;
			// 
			// FactNameColumn
			// 
			this->FactNameColumn->Text = L"Name";
			this->FactNameColumn->Width = 69;
			// 
			// FactGroupColumn
			// 
			this->FactGroupColumn->Text = L"Group";
			this->FactGroupColumn->Width = 41;
			// 
			// FactDownVariableBtn
			// 
			this->FactDownVariableBtn->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			this->FactDownVariableBtn->Location = System::Drawing::Point(209, 229);
			this->FactDownVariableBtn->Name = L"FactDownVariableBtn";
			this->FactDownVariableBtn->Size = System::Drawing::Size(26, 32);
			this->FactDownVariableBtn->TabIndex = 9;
			this->FactDownVariableBtn->Text = L"↓";
			this->FactDownVariableBtn->UseVisualStyleBackColor = true;
			this->FactDownVariableBtn->Click += gcnew System::EventHandler(this, &MainForm::FactDownVariableBtn_Click);
			// 
			// FactUpVariableBtn
			// 
			this->FactUpVariableBtn->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			this->FactUpVariableBtn->Location = System::Drawing::Point(209, 130);
			this->FactUpVariableBtn->Name = L"FactUpVariableBtn";
			this->FactUpVariableBtn->Size = System::Drawing::Size(26, 32);
			this->FactUpVariableBtn->TabIndex = 8;
			this->FactUpVariableBtn->Text = L"↑";
			this->FactUpVariableBtn->UseVisualStyleBackColor = true;
			this->FactUpVariableBtn->Click += gcnew System::EventHandler(this, &MainForm::FactUpVariableBtn_Click);
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(9, 78);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(82, 13);
			this->label4->TabIndex = 9;
			this->label4->Text = L"Group (optional)";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(9, 50);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(35, 13);
			this->label3->TabIndex = 8;
			this->label3->Text = L"Name";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(9, 22);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(31, 13);
			this->label8->TabIndex = 7;
			this->label8->Text = L"Type";
			// 
			// FactVariableTypesCmBox
			// 
			this->FactVariableTypesCmBox->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->FactVariableTypesCmBox->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
			this->FactVariableTypesCmBox->FormattingEnabled = true;
			this->FactVariableTypesCmBox->Location = System::Drawing::Point(102, 19);
			this->FactVariableTypesCmBox->Name = L"FactVariableTypesCmBox";
			this->FactVariableTypesCmBox->Size = System::Drawing::Size(133, 21);
			this->FactVariableTypesCmBox->TabIndex = 3;
			// 
			// FactVariableNameTxt
			// 
			this->FactVariableNameTxt->Location = System::Drawing::Point(102, 47);
			this->FactVariableNameTxt->Name = L"FactVariableNameTxt";
			this->FactVariableNameTxt->Size = System::Drawing::Size(133, 20);
			this->FactVariableNameTxt->TabIndex = 4;
			// 
			// FactVariableGroupTxt
			// 
			this->FactVariableGroupTxt->Location = System::Drawing::Point(102, 73);
			this->FactVariableGroupTxt->Name = L"FactVariableGroupTxt";
			this->FactVariableGroupTxt->Size = System::Drawing::Size(133, 20);
			this->FactVariableGroupTxt->TabIndex = 5;
			// 
			// DeleteFactVariableBtn
			// 
			this->DeleteFactVariableBtn->Location = System::Drawing::Point(12, 267);
			this->DeleteFactVariableBtn->Name = L"DeleteFactVariableBtn";
			this->DeleteFactVariableBtn->Size = System::Drawing::Size(223, 23);
			this->DeleteFactVariableBtn->TabIndex = 10;
			this->DeleteFactVariableBtn->Text = L"Delete variable";
			this->DeleteFactVariableBtn->UseVisualStyleBackColor = true;
			this->DeleteFactVariableBtn->Click += gcnew System::EventHandler(this, &MainForm::DeleteFactVariableBtn_Click);
			// 
			// AddFactVariableBtn
			// 
			this->AddFactVariableBtn->Location = System::Drawing::Point(12, 101);
			this->AddFactVariableBtn->Name = L"AddFactVariableBtn";
			this->AddFactVariableBtn->Size = System::Drawing::Size(223, 23);
			this->AddFactVariableBtn->TabIndex = 6;
			this->AddFactVariableBtn->Text = L"Add variable";
			this->AddFactVariableBtn->UseVisualStyleBackColor = true;
			this->AddFactVariableBtn->Click += gcnew System::EventHandler(this, &MainForm::AddFactVariableBtn_Click);
			// 
			// SaveFactBtn
			// 
			this->SaveFactBtn->Location = System::Drawing::Point(23, 361);
			this->SaveFactBtn->Name = L"SaveFactBtn";
			this->SaveFactBtn->Size = System::Drawing::Size(223, 23);
			this->SaveFactBtn->TabIndex = 11;
			this->SaveFactBtn->Text = L"Save Fact Prototype";
			this->SaveFactBtn->UseVisualStyleBackColor = true;
			this->SaveFactBtn->Click += gcnew System::EventHandler(this, &MainForm::SaveFactBtn_Click);
			// 
			// FactsBox
			// 
			this->FactsBox->BackColor = System::Drawing::Color::Transparent;
			this->FactsBox->Controls->Add(this->splitContainer1);
			this->FactsBox->Dock = System::Windows::Forms::DockStyle::Fill;
			this->FactsBox->Location = System::Drawing::Point(0, 0);
			this->FactsBox->Name = L"FactsBox";
			this->FactsBox->Size = System::Drawing::Size(583, 444);
			this->FactsBox->TabIndex = 14;
			this->FactsBox->TabStop = false;
			this->FactsBox->Text = L"Facts";
			// 
			// splitContainer1
			// 
			this->splitContainer1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->splitContainer1->FixedPanel = System::Windows::Forms::FixedPanel::Panel1;
			this->splitContainer1->Location = System::Drawing::Point(3, 16);
			this->splitContainer1->Name = L"splitContainer1";
			this->splitContainer1->Orientation = System::Windows::Forms::Orientation::Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this->splitContainer1->Panel1->Controls->Add(this->label1);
			this->splitContainer1->Panel1->Controls->Add(this->UpdateFacts);
			this->splitContainer1->Panel1->Controls->Add(this->FactsNamesCmBox);
			this->splitContainer1->Panel1->Controls->Add(this->ShowFactBtn);
			this->splitContainer1->Panel1->Controls->Add(this->DeleteDBFactBtn);
			// 
			// splitContainer1.Panel2
			// 
			this->splitContainer1->Panel2->Controls->Add(this->AllFactsView);
			this->splitContainer1->Size = System::Drawing::Size(577, 425);
			this->splitContainer1->SplitterDistance = 36;
			this->splitContainer1->TabIndex = 13;
			this->splitContainer1->TabStop = false;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label1->Location = System::Drawing::Point(193, 25);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(187, 12);
			this->label1->TabIndex = 9;
			this->label1->Text = L"Tip: don\'t forget to update facts after entering";
			// 
			// UpdateFacts
			// 
			this->UpdateFacts->Location = System::Drawing::Point(276, 2);
			this->UpdateFacts->Name = L"UpdateFacts";
			this->UpdateFacts->Size = System::Drawing::Size(96, 23);
			this->UpdateFacts->TabIndex = 16;
			this->UpdateFacts->Text = L"Update Facts";
			this->UpdateFacts->UseVisualStyleBackColor = true;
			this->UpdateFacts->Click += gcnew System::EventHandler(this, &MainForm::Update_Fact_Table_Click);
			// 
			// FactsNamesCmBox
			// 
			this->FactsNamesCmBox->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->FactsNamesCmBox->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
			this->FactsNamesCmBox->FormattingEnabled = true;
			this->FactsNamesCmBox->Location = System::Drawing::Point(19, 4);
			this->FactsNamesCmBox->Name = L"FactsNamesCmBox";
			this->FactsNamesCmBox->Size = System::Drawing::Size(170, 21);
			this->FactsNamesCmBox->Sorted = true;
			this->FactsNamesCmBox->TabIndex = 14;
			// 
			// ShowFactBtn
			// 
			this->ShowFactBtn->Location = System::Drawing::Point(195, 2);
			this->ShowFactBtn->Name = L"ShowFactBtn";
			this->ShowFactBtn->Size = System::Drawing::Size(75, 23);
			this->ShowFactBtn->TabIndex = 15;
			this->ShowFactBtn->Text = L"Show";
			this->ShowFactBtn->UseVisualStyleBackColor = true;
			this->ShowFactBtn->Click += gcnew System::EventHandler(this, &MainForm::Show_fact_table_Click);
			// 
			// DeleteDBFactBtn
			// 
			this->DeleteDBFactBtn->Location = System::Drawing::Point(378, 2);
			this->DeleteDBFactBtn->Name = L"DeleteDBFactBtn";
			this->DeleteDBFactBtn->Size = System::Drawing::Size(91, 23);
			this->DeleteDBFactBtn->TabIndex = 17;
			this->DeleteDBFactBtn->Text = L"Delete DB Fact";
			this->DeleteDBFactBtn->UseVisualStyleBackColor = true;
			this->DeleteDBFactBtn->Click += gcnew System::EventHandler(this, &MainForm::DeleteFact_Click);
			// 
			// AllFactsView
			// 
			this->AllFactsView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->AllFactsView->Dock = System::Windows::Forms::DockStyle::Fill;
			this->AllFactsView->Location = System::Drawing::Point(0, 0);
			this->AllFactsView->Name = L"AllFactsView";
			this->AllFactsView->Size = System::Drawing::Size(577, 385);
			this->AllFactsView->TabIndex = 18;
			// 
			// TypesTab
			// 
			this->TypesTab->Controls->Add(this->DeleteTypeBtn);
			this->TypesTab->Controls->Add(this->UpdateTypeBtn);
			this->TypesTab->Controls->Add(this->TypesLbx);
			this->TypesTab->Controls->Add(this->AddTypeBtn);
			this->TypesTab->Controls->Add(this->TypeNameLb);
			this->TypesTab->Controls->Add(this->TypeTextBox);
			this->TypesTab->Location = System::Drawing::Point(4, 22);
			this->TypesTab->Name = L"TypesTab";
			this->TypesTab->Padding = System::Windows::Forms::Padding(3);
			this->TypesTab->Size = System::Drawing::Size(862, 450);
			this->TypesTab->TabIndex = 0;
			this->TypesTab->Text = L"Types";
			this->TypesTab->UseVisualStyleBackColor = true;
			// 
			// DeleteTypeBtn
			// 
			this->DeleteTypeBtn->Location = System::Drawing::Point(396, 56);
			this->DeleteTypeBtn->Name = L"DeleteTypeBtn";
			this->DeleteTypeBtn->Size = System::Drawing::Size(75, 23);
			this->DeleteTypeBtn->TabIndex = 4;
			this->DeleteTypeBtn->Text = L"Delete Type";
			this->DeleteTypeBtn->UseVisualStyleBackColor = true;
			this->DeleteTypeBtn->Click += gcnew System::EventHandler(this, &MainForm::DeleteTypeBtn_Click);
			// 
			// UpdateTypeBtn
			// 
			this->UpdateTypeBtn->Location = System::Drawing::Point(477, 56);
			this->UpdateTypeBtn->Name = L"UpdateTypeBtn";
			this->UpdateTypeBtn->Size = System::Drawing::Size(86, 23);
			this->UpdateTypeBtn->TabIndex = 3;
			this->UpdateTypeBtn->Text = L"Update Type";
			this->UpdateTypeBtn->UseVisualStyleBackColor = true;
			this->UpdateTypeBtn->Click += gcnew System::EventHandler(this, &MainForm::UpdateTypeBtn_Click);
			// 
			// TypesLbx
			// 
			this->TypesLbx->FormattingEnabled = true;
			this->TypesLbx->Location = System::Drawing::Point(315, 85);
			this->TypesLbx->Name = L"TypesLbx";
			this->TypesLbx->Size = System::Drawing::Size(248, 290);
			this->TypesLbx->TabIndex = 5;
			// 
			// AddTypeBtn
			// 
			this->AddTypeBtn->Location = System::Drawing::Point(315, 56);
			this->AddTypeBtn->Name = L"AddTypeBtn";
			this->AddTypeBtn->Size = System::Drawing::Size(75, 23);
			this->AddTypeBtn->TabIndex = 2;
			this->AddTypeBtn->Text = L"Add Type";
			this->AddTypeBtn->UseVisualStyleBackColor = true;
			this->AddTypeBtn->Click += gcnew System::EventHandler(this, &MainForm::AddTypeBtn_Click);
			// 
			// TypeNameLb
			// 
			this->TypeNameLb->AutoSize = true;
			this->TypeNameLb->Location = System::Drawing::Point(312, 33);
			this->TypeNameLb->Name = L"TypeNameLb";
			this->TypeNameLb->Size = System::Drawing::Size(60, 13);
			this->TypeNameLb->TabIndex = 1;
			this->TypeNameLb->Text = L"Type name";
			// 
			// TypeTextBox
			// 
			this->TypeTextBox->Location = System::Drawing::Point(404, 30);
			this->TypeTextBox->Name = L"TypeTextBox";
			this->TypeTextBox->Size = System::Drawing::Size(159, 20);
			this->TypeTextBox->TabIndex = 1;
			// 
			// MainTab
			// 
			this->MainTab->Controls->Add(this->groupBox5);
			this->MainTab->Controls->Add(this->WriteCnf);
			this->MainTab->Controls->Add(this->SolvingTimeLbl);
			this->MainTab->Controls->Add(this->ScissoringSwotcher);
			this->MainTab->Controls->Add(this->SolvingTimeNameLbl);
			this->MainTab->Controls->Add(this->StatusNameLbl);
			this->MainTab->Controls->Add(this->GenTimeLbl);
			this->MainTab->Controls->Add(this->groupBox2);
			this->MainTab->Controls->Add(this->StatusLbl);
			this->MainTab->Controls->Add(this->GenTimeNameLbl);
			this->MainTab->Controls->Add(this->CNFTextBox);
			this->MainTab->Controls->Add(this->LoadAndCalculate);
			this->MainTab->Location = System::Drawing::Point(4, 22);
			this->MainTab->Name = L"MainTab";
			this->MainTab->Size = System::Drawing::Size(862, 450);
			this->MainTab->TabIndex = 3;
			this->MainTab->Text = L"Main";
			this->MainTab->UseVisualStyleBackColor = true;
			// 
			// groupBox5
			// 
			this->groupBox5->Controls->Add(this->SaveGraphvizButton);
			this->groupBox5->Controls->Add(this->WriteGraphvizBtn);
			this->groupBox5->Location = System::Drawing::Point(297, 14);
			this->groupBox5->Name = L"groupBox5";
			this->groupBox5->Size = System::Drawing::Size(186, 100);
			this->groupBox5->TabIndex = 17;
			this->groupBox5->TabStop = false;
			this->groupBox5->Text = L"Graphviz menu";
			// 
			// SaveGraphvizButton
			// 
			this->SaveGraphvizButton->AutoSize = true;
			this->SaveGraphvizButton->Location = System::Drawing::Point(6, 27);
			this->SaveGraphvizButton->Name = L"SaveGraphvizButton";
			this->SaveGraphvizButton->Size = System::Drawing::Size(174, 23);
			this->SaveGraphvizButton->TabIndex = 2;
			this->SaveGraphvizButton->Text = L"Save Graphviz File";
			this->SaveGraphvizButton->UseVisualStyleBackColor = true;
			this->SaveGraphvizButton->Click += gcnew System::EventHandler(this, &MainForm::SaveGraphvizButton_Click);
			// 
			// WriteGraphvizBtn
			// 
			this->WriteGraphvizBtn->Location = System::Drawing::Point(6, 56);
			this->WriteGraphvizBtn->Name = L"WriteGraphvizBtn";
			this->WriteGraphvizBtn->Size = System::Drawing::Size(174, 23);
			this->WriteGraphvizBtn->TabIndex = 11;
			this->WriteGraphvizBtn->Text = L"Write Graphviz file";
			this->WriteGraphvizBtn->UseVisualStyleBackColor = true;
			this->WriteGraphvizBtn->Click += gcnew System::EventHandler(this, &MainForm::WriteGraphvizBtn_Click);
			// 
			// WriteCnf
			// 
			this->WriteCnf->Location = System::Drawing::Point(18, 230);
			this->WriteCnf->Name = L"WriteCnf";
			this->WriteCnf->Size = System::Drawing::Size(75, 23);
			this->WriteCnf->TabIndex = 3;
			this->WriteCnf->Text = L"WriteCnf";
			this->WriteCnf->UseVisualStyleBackColor = true;
			this->WriteCnf->Visible = false;
			this->WriteCnf->Click += gcnew System::EventHandler(this, &MainForm::WriteCnf_Click);
			// 
			// SolvingTimeLbl
			// 
			this->SolvingTimeLbl->AutoSize = true;
			this->SolvingTimeLbl->Location = System::Drawing::Point(240, 75);
			this->SolvingTimeLbl->Name = L"SolvingTimeLbl";
			this->SolvingTimeLbl->Size = System::Drawing::Size(22, 13);
			this->SolvingTimeLbl->TabIndex = 10;
			this->SolvingTimeLbl->Text = L"0.0";
			// 
			// ScissoringSwotcher
			// 
			this->ScissoringSwotcher->Controls->Add(this->AtScissoring);
			this->ScissoringSwotcher->Controls->Add(this->PostScissoring);
			this->ScissoringSwotcher->Location = System::Drawing::Point(18, 130);
			this->ScissoringSwotcher->Name = L"ScissoringSwotcher";
			this->ScissoringSwotcher->Size = System::Drawing::Size(190, 68);
			this->ScissoringSwotcher->TabIndex = 15;
			this->ScissoringSwotcher->TabStop = false;
			this->ScissoringSwotcher->Text = L"Select scissoring type";
			// 
			// AtScissoring
			// 
			this->AtScissoring->AutoSize = true;
			this->AtScissoring->Location = System::Drawing::Point(19, 43);
			this->AtScissoring->Name = L"AtScissoring";
			this->AtScissoring->Size = System::Drawing::Size(107, 17);
			this->AtScissoring->TabIndex = 1;
			this->AtScissoring->Text = L"Attack Scissoring";
			this->AtScissoring->UseVisualStyleBackColor = true;
			this->AtScissoring->CheckedChanged += gcnew System::EventHandler(this, &MainForm::AtScissoring_CheckedChanged);
			// 
			// PostScissoring
			// 
			this->PostScissoring->AutoSize = true;
			this->PostScissoring->Checked = true;
			this->PostScissoring->Location = System::Drawing::Point(19, 20);
			this->PostScissoring->Name = L"PostScissoring";
			this->PostScissoring->Size = System::Drawing::Size(138, 17);
			this->PostScissoring->TabIndex = 0;
			this->PostScissoring->TabStop = true;
			this->PostScissoring->Text = L"Postcondition scissoring";
			this->PostScissoring->UseVisualStyleBackColor = true;
			this->PostScissoring->CheckedChanged += gcnew System::EventHandler(this, &MainForm::PostScissoring_CheckedChanged);
			// 
			// SolvingTimeNameLbl
			// 
			this->SolvingTimeNameLbl->AutoSize = true;
			this->SolvingTimeNameLbl->Location = System::Drawing::Point(150, 75);
			this->SolvingTimeNameLbl->Name = L"SolvingTimeNameLbl";
			this->SolvingTimeNameLbl->Size = System::Drawing::Size(71, 13);
			this->SolvingTimeNameLbl->TabIndex = 9;
			this->SolvingTimeNameLbl->Text = L"Solving Time:";
			// 
			// StatusNameLbl
			// 
			this->StatusNameLbl->AutoSize = true;
			this->StatusNameLbl->Location = System::Drawing::Point(150, 44);
			this->StatusNameLbl->Name = L"StatusNameLbl";
			this->StatusNameLbl->Size = System::Drawing::Size(40, 13);
			this->StatusNameLbl->TabIndex = 5;
			this->StatusNameLbl->Text = L"Status:";
			// 
			// GenTimeLbl
			// 
			this->GenTimeLbl->AutoSize = true;
			this->GenTimeLbl->Location = System::Drawing::Point(240, 59);
			this->GenTimeLbl->Name = L"GenTimeLbl";
			this->GenTimeLbl->Size = System::Drawing::Size(22, 13);
			this->GenTimeLbl->TabIndex = 8;
			this->GenTimeLbl->Text = L"0.0";
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->CalculateButton);
			this->groupBox2->Controls->Add(this->button1);
			this->groupBox2->Location = System::Drawing::Point(18, 14);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(126, 101);
			this->groupBox2->TabIndex = 12;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Build Attack Graph";
			// 
			// CalculateButton
			// 
			this->CalculateButton->Location = System::Drawing::Point(6, 27);
			this->CalculateButton->Name = L"CalculateButton";
			this->CalculateButton->Size = System::Drawing::Size(114, 23);
			this->CalculateButton->TabIndex = 1;
			this->CalculateButton->Text = L"With SAT";
			this->CalculateButton->UseVisualStyleBackColor = true;
			this->CalculateButton->Click += gcnew System::EventHandler(this, &MainForm::SAT_AG_build_Click);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(7, 56);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(113, 23);
			this->button1->TabIndex = 13;
			this->button1->Text = L"With C++";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MainForm::Plus_AG_build_Click);
			// 
			// StatusLbl
			// 
			this->StatusLbl->AutoSize = true;
			this->StatusLbl->Location = System::Drawing::Point(240, 44);
			this->StatusLbl->Name = L"StatusLbl";
			this->StatusLbl->Size = System::Drawing::Size(33, 13);
			this->StatusLbl->TabIndex = 6;
			this->StatusLbl->Text = L"None";
			// 
			// GenTimeNameLbl
			// 
			this->GenTimeNameLbl->AutoSize = true;
			this->GenTimeNameLbl->Location = System::Drawing::Point(150, 59);
			this->GenTimeNameLbl->Name = L"GenTimeNameLbl";
			this->GenTimeNameLbl->Size = System::Drawing::Size(88, 13);
			this->GenTimeNameLbl->TabIndex = 7;
			this->GenTimeNameLbl->Text = L"Generation Time:";
			// 
			// CNFTextBox
			// 
			this->CNFTextBox->Location = System::Drawing::Point(490, 14);
			this->CNFTextBox->Name = L"CNFTextBox";
			this->CNFTextBox->Size = System::Drawing::Size(354, 417);
			this->CNFTextBox->TabIndex = 11;
			this->CNFTextBox->Text = L"";
			// 
			// LoadAndCalculate
			// 
			this->LoadAndCalculate->Location = System::Drawing::Point(18, 280);
			this->LoadAndCalculate->Name = L"LoadAndCalculate";
			this->LoadAndCalculate->Size = System::Drawing::Size(117, 23);
			this->LoadAndCalculate->TabIndex = 4;
			this->LoadAndCalculate->Text = L"Load And Calculate";
			this->LoadAndCalculate->UseVisualStyleBackColor = true;
			this->LoadAndCalculate->Visible = false;
			// 
			// SystemTab
			// 
			this->SystemTab->Controls->Add(this->MainTab);
			this->SystemTab->Controls->Add(this->TypesTab);
			this->SystemTab->Controls->Add(this->FactsTab);
			this->SystemTab->Controls->Add(this->AttacksTab);
			this->SystemTab->Controls->Add(this->BlockingTab);
			this->SystemTab->Controls->Add(this->TestingTab);
			this->SystemTab->Dock = System::Windows::Forms::DockStyle::Fill;
			this->SystemTab->ItemSize = System::Drawing::Size(42, 18);
			this->SystemTab->Location = System::Drawing::Point(0, 24);
			this->SystemTab->Name = L"SystemTab";
			this->SystemTab->SelectedIndex = 0;
			this->SystemTab->Size = System::Drawing::Size(870, 476);
			this->SystemTab->TabIndex = 0;
			// 
			// BlockingTab
			// 
			this->BlockingTab->Controls->Add(this->TimeoutTxtBox);
			this->BlockingTab->Controls->Add(this->label14);
			this->BlockingTab->Controls->Add(this->showPatchesBtn);
			this->BlockingTab->Controls->Add(this->groupBox4);
			this->BlockingTab->Controls->Add(this->groupBox3);
			this->BlockingTab->Controls->Add(this->label13);
			this->BlockingTab->Controls->Add(this->label12);
			this->BlockingTab->Controls->Add(this->label11);
			this->BlockingTab->Controls->Add(this->UpBlockingBoundaryTxt);
			this->BlockingTab->Controls->Add(this->LowBlockingBoundaryTxt);
			this->BlockingTab->Controls->Add(this->NumberOfStepsTxt);
			this->BlockingTab->Controls->Add(this->WriteGraphvizBlockingBtn);
			this->BlockingTab->Controls->Add(this->GetNamesToBlockBtn);
			this->BlockingTab->Controls->Add(this->CalcBlocksBtn);
			this->BlockingTab->Location = System::Drawing::Point(4, 22);
			this->BlockingTab->Name = L"BlockingTab";
			this->BlockingTab->Size = System::Drawing::Size(862, 450);
			this->BlockingTab->TabIndex = 4;
			this->BlockingTab->Text = L"Blocking";
			this->BlockingTab->UseVisualStyleBackColor = true;
			// 
			// TimeoutTxtBox
			// 
			this->TimeoutTxtBox->Location = System::Drawing::Point(411, 381);
			this->TimeoutTxtBox->Name = L"TimeoutTxtBox";
			this->TimeoutTxtBox->Size = System::Drawing::Size(83, 20);
			this->TimeoutTxtBox->TabIndex = 22;
			this->TimeoutTxtBox->Text = L"0";
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(330, 383);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(71, 13);
			this->label14->TabIndex = 21;
			this->label14->Text = L"Timeout (sec)";
			// 
			// showPatchesBtn
			// 
			this->showPatchesBtn->Location = System::Drawing::Point(705, 412);
			this->showPatchesBtn->Name = L"showPatchesBtn";
			this->showPatchesBtn->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->showPatchesBtn->Size = System::Drawing::Size(138, 23);
			this->showPatchesBtn->TabIndex = 20;
			this->showPatchesBtn->Text = L"Show patches";
			this->showPatchesBtn->UseVisualStyleBackColor = true;
			this->showPatchesBtn->Click += gcnew System::EventHandler(this, &MainForm::showPatchesBtn_Click);
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->AllPcondNamesLB);
			this->groupBox4->Controls->Add(this->PcondsToBlockLB);
			this->groupBox4->Controls->Add(this->AddOrAttackToBlockBtn);
			this->groupBox4->Controls->Add(this->DeleteOrAttackFromBlockBtn);
			this->groupBox4->Location = System::Drawing::Point(436, 51);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Size = System::Drawing::Size(423, 316);
			this->groupBox4->TabIndex = 19;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L"Postconditions";
			// 
			// AllPcondNamesLB
			// 
			this->AllPcondNamesLB->FormattingEnabled = true;
			this->AllPcondNamesLB->Location = System::Drawing::Point(6, 19);
			this->AllPcondNamesLB->Name = L"AllPcondNamesLB";
			this->AllPcondNamesLB->SelectionMode = System::Windows::Forms::SelectionMode::MultiExtended;
			this->AllPcondNamesLB->Size = System::Drawing::Size(169, 290);
			this->AllPcondNamesLB->TabIndex = 6;
			// 
			// PcondsToBlockLB
			// 
			this->PcondsToBlockLB->FormattingEnabled = true;
			this->PcondsToBlockLB->Location = System::Drawing::Point(244, 19);
			this->PcondsToBlockLB->Name = L"PcondsToBlockLB";
			this->PcondsToBlockLB->Size = System::Drawing::Size(174, 290);
			this->PcondsToBlockLB->TabIndex = 7;
			// 
			// AddOrAttackToBlockBtn
			// 
			this->AddOrAttackToBlockBtn->Location = System::Drawing::Point(185, 131);
			this->AddOrAttackToBlockBtn->Name = L"AddOrAttackToBlockBtn";
			this->AddOrAttackToBlockBtn->Size = System::Drawing::Size(53, 23);
			this->AddOrAttackToBlockBtn->TabIndex = 8;
			this->AddOrAttackToBlockBtn->Text = L">>";
			this->AddOrAttackToBlockBtn->UseVisualStyleBackColor = true;
			this->AddOrAttackToBlockBtn->Click += gcnew System::EventHandler(this, &MainForm::AddPostcondToBlockBtn_Click);
			// 
			// DeleteOrAttackFromBlockBtn
			// 
			this->DeleteOrAttackFromBlockBtn->Location = System::Drawing::Point(185, 160);
			this->DeleteOrAttackFromBlockBtn->Name = L"DeleteOrAttackFromBlockBtn";
			this->DeleteOrAttackFromBlockBtn->Size = System::Drawing::Size(53, 23);
			this->DeleteOrAttackFromBlockBtn->TabIndex = 10;
			this->DeleteOrAttackFromBlockBtn->Text = L"<<";
			this->DeleteOrAttackFromBlockBtn->UseVisualStyleBackColor = true;
			this->DeleteOrAttackFromBlockBtn->Click += gcnew System::EventHandler(this, &MainForm::DeletePostcondkFromBlockBtn_Click);
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->AllFactsNamesLB);
			this->groupBox3->Controls->Add(this->FactsNamesToBlockLB);
			this->groupBox3->Controls->Add(this->AddFactToBlockBtn);
			this->groupBox3->Controls->Add(this->DeleteFactFromBlockBtn);
			this->groupBox3->Location = System::Drawing::Point(8, 51);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(429, 316);
			this->groupBox3->TabIndex = 18;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"Facts";
			// 
			// AllFactsNamesLB
			// 
			this->AllFactsNamesLB->FormattingEnabled = true;
			this->AllFactsNamesLB->Location = System::Drawing::Point(6, 19);
			this->AllFactsNamesLB->Name = L"AllFactsNamesLB";
			this->AllFactsNamesLB->SelectionMode = System::Windows::Forms::SelectionMode::MultiExtended;
			this->AllFactsNamesLB->Size = System::Drawing::Size(178, 290);
			this->AllFactsNamesLB->TabIndex = 4;
			// 
			// FactsNamesToBlockLB
			// 
			this->FactsNamesToBlockLB->FormattingEnabled = true;
			this->FactsNamesToBlockLB->Location = System::Drawing::Point(248, 19);
			this->FactsNamesToBlockLB->Name = L"FactsNamesToBlockLB";
			this->FactsNamesToBlockLB->Size = System::Drawing::Size(168, 290);
			this->FactsNamesToBlockLB->TabIndex = 5;
			// 
			// AddFactToBlockBtn
			// 
			this->AddFactToBlockBtn->Location = System::Drawing::Point(190, 131);
			this->AddFactToBlockBtn->Name = L"AddFactToBlockBtn";
			this->AddFactToBlockBtn->Size = System::Drawing::Size(53, 23);
			this->AddFactToBlockBtn->TabIndex = 2;
			this->AddFactToBlockBtn->Text = L" >>";
			this->AddFactToBlockBtn->UseVisualStyleBackColor = true;
			this->AddFactToBlockBtn->Click += gcnew System::EventHandler(this, &MainForm::AddFactToBlockBtn_Click);
			// 
			// DeleteFactFromBlockBtn
			// 
			this->DeleteFactFromBlockBtn->Location = System::Drawing::Point(190, 160);
			this->DeleteFactFromBlockBtn->Name = L"DeleteFactFromBlockBtn";
			this->DeleteFactFromBlockBtn->Size = System::Drawing::Size(53, 23);
			this->DeleteFactFromBlockBtn->TabIndex = 9;
			this->DeleteFactFromBlockBtn->Text = L"<<";
			this->DeleteFactFromBlockBtn->UseVisualStyleBackColor = true;
			this->DeleteFactFromBlockBtn->Click += gcnew System::EventHandler(this, &MainForm::DeleteFactFromBlockBtn_Click);
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(164, 409);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(84, 13);
			this->label13->TabIndex = 17;
			this->label13->Text = L"Upper Boundary";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(164, 383);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(84, 13);
			this->label12->TabIndex = 16;
			this->label12->Text = L"Lower Boundary";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(11, 383);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(88, 13);
			this->label11->TabIndex = 15;
			this->label11->Text = L"Number Of Steps";
			// 
			// UpBlockingBoundaryTxt
			// 
			this->UpBlockingBoundaryTxt->Location = System::Drawing::Point(254, 409);
			this->UpBlockingBoundaryTxt->Name = L"UpBlockingBoundaryTxt";
			this->UpBlockingBoundaryTxt->Size = System::Drawing::Size(37, 20);
			this->UpBlockingBoundaryTxt->TabIndex = 14;
			this->UpBlockingBoundaryTxt->Text = L"0";
			// 
			// LowBlockingBoundaryTxt
			// 
			this->LowBlockingBoundaryTxt->Location = System::Drawing::Point(254, 380);
			this->LowBlockingBoundaryTxt->Name = L"LowBlockingBoundaryTxt";
			this->LowBlockingBoundaryTxt->Size = System::Drawing::Size(37, 20);
			this->LowBlockingBoundaryTxt->TabIndex = 13;
			this->LowBlockingBoundaryTxt->Text = L"1";
			// 
			// NumberOfStepsTxt
			// 
			this->NumberOfStepsTxt->Location = System::Drawing::Point(105, 380);
			this->NumberOfStepsTxt->Name = L"NumberOfStepsTxt";
			this->NumberOfStepsTxt->Size = System::Drawing::Size(37, 20);
			this->NumberOfStepsTxt->TabIndex = 12;
			this->NumberOfStepsTxt->Text = L"1";
			// 
			// WriteGraphvizBlockingBtn
			// 
			this->WriteGraphvizBlockingBtn->Location = System::Drawing::Point(705, 383);
			this->WriteGraphvizBlockingBtn->Name = L"WriteGraphvizBlockingBtn";
			this->WriteGraphvizBlockingBtn->Size = System::Drawing::Size(138, 23);
			this->WriteGraphvizBlockingBtn->TabIndex = 11;
			this->WriteGraphvizBlockingBtn->Text = L"Write Graphviz File";
			this->WriteGraphvizBlockingBtn->UseVisualStyleBackColor = true;
			this->WriteGraphvizBlockingBtn->Click += gcnew System::EventHandler(this, &MainForm::WriteGraphvizBlockingBtn_Click);
			// 
			// GetNamesToBlockBtn
			// 
			this->GetNamesToBlockBtn->Location = System::Drawing::Point(14, 15);
			this->GetNamesToBlockBtn->Name = L"GetNamesToBlockBtn";
			this->GetNamesToBlockBtn->Size = System::Drawing::Size(75, 23);
			this->GetNamesToBlockBtn->TabIndex = 3;
			this->GetNamesToBlockBtn->Text = L"Get names";
			this->GetNamesToBlockBtn->UseVisualStyleBackColor = true;
			this->GetNamesToBlockBtn->Click += gcnew System::EventHandler(this, &MainForm::GetNamesToBlockBtn_Click);
			// 
			// CalcBlocksBtn
			// 
			this->CalcBlocksBtn->Location = System::Drawing::Point(561, 383);
			this->CalcBlocksBtn->Name = L"CalcBlocksBtn";
			this->CalcBlocksBtn->Size = System::Drawing::Size(138, 23);
			this->CalcBlocksBtn->TabIndex = 0;
			this->CalcBlocksBtn->Text = L"Calculate";
			this->CalcBlocksBtn->UseVisualStyleBackColor = true;
			this->CalcBlocksBtn->Click += gcnew System::EventHandler(this, &MainForm::CalcBlocksBtn_Click);
			// 
			// TestingTab
			// 
			this->TestingTab->Controls->Add(this->label16);
			this->TestingTab->Controls->Add(this->TimeoutTestTxt);
			this->TestingTab->Controls->Add(this->label15);
			this->TestingTab->Controls->Add(this->textBox1);
			this->TestingTab->Controls->Add(this->TestTimesBtn);
			this->TestingTab->Controls->Add(this->TestingBox);
			this->TestingTab->Controls->Add(this->TestPatchesBtn);
			this->TestingTab->Location = System::Drawing::Point(4, 22);
			this->TestingTab->Name = L"TestingTab";
			this->TestingTab->Size = System::Drawing::Size(862, 450);
			this->TestingTab->TabIndex = 5;
			this->TestingTab->Text = L"For Testing";
			this->TestingTab->UseVisualStyleBackColor = true;
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(657, 6);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(71, 13);
			this->label16->TabIndex = 6;
			this->label16->Text = L"Timeout (sec)";
			// 
			// TimeoutTestTxt
			// 
			this->TimeoutTestTxt->Location = System::Drawing::Point(735, 4);
			this->TimeoutTestTxt->Name = L"TimeoutTestTxt";
			this->TimeoutTestTxt->Size = System::Drawing::Size(100, 20);
			this->TimeoutTestTxt->TabIndex = 5;
			this->TimeoutTestTxt->Text = L"0";
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(448, 6);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(52, 13);
			this->label15->TabIndex = 4;
			this->label15->Text = L"Start from";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(508, 3);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(48, 20);
			this->textBox1->TabIndex = 3;
			this->textBox1->Text = L"1";
			// 
			// TestTimesBtn
			// 
			this->TestTimesBtn->Location = System::Drawing::Point(28, 28);
			this->TestTimesBtn->Name = L"TestTimesBtn";
			this->TestTimesBtn->Size = System::Drawing::Size(399, 144);
			this->TestTimesBtn->TabIndex = 2;
			this->TestTimesBtn->Text = L"Test times";
			this->TestTimesBtn->UseVisualStyleBackColor = true;
			this->TestTimesBtn->Click += gcnew System::EventHandler(this, &MainForm::TestTimesBtn_Click);
			// 
			// TestingBox
			// 
			this->TestingBox->Location = System::Drawing::Point(28, 192);
			this->TestingBox->Name = L"TestingBox";
			this->TestingBox->Size = System::Drawing::Size(808, 237);
			this->TestingBox->TabIndex = 1;
			this->TestingBox->Text = L"";
			// 
			// TestPatchesBtn
			// 
			this->TestPatchesBtn->Location = System::Drawing::Point(448, 28);
			this->TestPatchesBtn->Name = L"TestPatchesBtn";
			this->TestPatchesBtn->Size = System::Drawing::Size(388, 144);
			this->TestPatchesBtn->TabIndex = 0;
			this->TestPatchesBtn->Text = L"TEST PATCHES";
			this->TestPatchesBtn->UseVisualStyleBackColor = true;
			this->TestPatchesBtn->Click += gcnew System::EventHandler(this, &MainForm::TestPatchesBtn_Click);
			// 
			// saveFileDialog
			// 
			this->saveFileDialog->DefaultExt = L"zip";
			this->saveFileDialog->Filter = L"ZIP archive| *.zip";
			// 
			// opentTestFiles
			// 
			this->opentTestFiles->FileName = L"opentTestFiles";
			this->opentTestFiles->Multiselect = true;
			// 
			// MainForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(870, 500);
			this->Controls->Add(this->SystemTab);
			this->Controls->Add(this->menuStrip1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MaximizeBox = false;
			this->MinimumSize = System::Drawing::Size(875, 539);
			this->Name = L"MainForm";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"UnProVET";
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataSet1))->EndInit();
			this->AttacksTab->ResumeLayout(false);
			this->ANDAttacksBox->ResumeLayout(false);
			this->ANDAttacksBox->PerformLayout();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->ANDAttackSignatureBox->ResumeLayout(false);
			this->ANDAttackSignatureBox->PerformLayout();
			this->ORAttacksBox->ResumeLayout(false);
			this->ORAttacksBox->PerformLayout();
			this->ORAttackSignatureBox->ResumeLayout(false);
			this->ORAttackSignatureBox->PerformLayout();
			this->FactsTab->ResumeLayout(false);
			this->splitContainer2->Panel1->ResumeLayout(false);
			this->splitContainer2->Panel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer2))->EndInit();
			this->splitContainer2->ResumeLayout(false);
			this->FactsProtoBox->ResumeLayout(false);
			this->FactsProtoBox->PerformLayout();
			this->FactSignatureBox->ResumeLayout(false);
			this->FactSignatureBox->PerformLayout();
			this->FactsBox->ResumeLayout(false);
			this->splitContainer1->Panel1->ResumeLayout(false);
			this->splitContainer1->Panel1->PerformLayout();
			this->splitContainer1->Panel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer1))->EndInit();
			this->splitContainer1->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->AllFactsView))->EndInit();
			this->TypesTab->ResumeLayout(false);
			this->TypesTab->PerformLayout();
			this->MainTab->ResumeLayout(false);
			this->MainTab->PerformLayout();
			this->groupBox5->ResumeLayout(false);
			this->groupBox5->PerformLayout();
			this->ScissoringSwotcher->ResumeLayout(false);
			this->ScissoringSwotcher->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->SystemTab->ResumeLayout(false);
			this->BlockingTab->ResumeLayout(false);
			this->BlockingTab->PerformLayout();
			this->groupBox4->ResumeLayout(false);
			this->groupBox3->ResumeLayout(false);
			this->TestingTab->ResumeLayout(false);
			this->TestingTab->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void menuStrip1_ItemClicked(System::Object^  sender, System::Windows::Forms::ToolStripItemClickedEventArgs^  e) {
	}
	private: System::Void openToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void SAT_AG_build_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void SaveGraphvizButton_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void WriteCnf_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void Show_fact_table_Click(System::Object^  sender, System::EventArgs^  e);

	private: System::Void Update_Fact_Table_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void DeleteFact_Click(System::Object^  sender, System::EventArgs^  e);

	private: System::Void AddTypeBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void UpdateTypeBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void DeleteTypeBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void AddFactVariableBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void DeleteFactVariableBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void SaveFactBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void FactUpVariableBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void FactDownVariableBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void FactProtoNameCmBox_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void AddPostcondVariableBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void DeletePostcondVariableBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void DeletePostcondBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void AttackFromPostcondBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void AttackSignLV_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void DeleteAttackVariableBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void AddAttackVariableBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void UpdateAttackVariableBtn_Click(System::Object^  sender, System::EventArgs^  e);

	private: System::Void ORUpVariableBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void ORDownVariableBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void ANDUpVariableBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void ANDDownVariableBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void AttackFactNamesCmb_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void AttackFactValuesLv_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void AttackAddFactBtn_Click(System::Object^  sender, System::EventArgs^  e);

	private: System::Void PostcondNames_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void AttackNames_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void AttackFactLb_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void SavePostcondkBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void SaveAttackBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void AttackDeleteFactBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void AttackUpdateFactBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void DeleteAttackBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void DeleteFactBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void saveToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void Plus_AG_build_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void GetNamesToBlockBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void AddFactToBlockBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void DeleteFactFromBlockBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void AddPostcondToBlockBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void DeletePostcondkFromBlockBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void CalcBlocksBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void WriteGraphvizBlockingBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void GraphvizToTxtBoxCPP_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void WriteGraphvizBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void showPatchesBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void TestPatchesBtn_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void AtScissoring_CheckedChanged(System::Object^ sender, System::EventArgs^ e);
	private: System::Void PostScissoring_CheckedChanged(System::Object^ sender, System::EventArgs^ e);
	private: System::Void TestTimesBtn_Click(System::Object^ sender, System::EventArgs^ e);
	private: void clear_extract_folder();
	private: System::Void newToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);
};
}
