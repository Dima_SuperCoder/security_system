#pragma once
#include <vector>
#include <string>
#include <map>
#include <set>
#include "attack_rule.h"
#include "fact.h"
#include "sql_wrapper.h"
#include "Attack_Graph.h"
#include "minisat22_wrapper.h"


enum scisorring_modes
{
	postcondition_scissoring,
	attacks_scissoring
};

class security_system
{
public:
	security_system();

	void return_all_names(std::vector<std::string>& facts_names, std::vector<std::string>& or_attack_names, std::vector<std::string>& and_attack_names);
	
	void add_fact_template(std::string fact_template, signs s);
	bool add_fact_template(std::string fact_template, std::vector<std::string> in_types, std::vector<std::string> names, std::vector<std::string> groups);
	void add_postcond_template(std::string name, signs s);
	bool add_postcond_template(std::string name, std::vector<std::string> in_types, std::vector<std::string> names, std::vector<std::string> groups);	
	bool add_attack_template(std::string name, signs s, std::vector<fact> f, std::string postcond);
	
	std::string return_cnf();
	std::string solve_task_iter(sql_wrapper^ wrapper, double& gen_time, double& solve_time, bool test_calc);
	std::string solve_task_iter_for_patch(sql_wrapper^ wrapper, int steps, double& gen_time, double& solve_time, std::vector<int> attacks_to_block, bool test_calc, int& n, int m=0, double timeout=0.0);
	
	void set_steps(int s);
	void write_cnf(std::string filename);
	void write_gv_file_iterable(std::string output_file, sql_wrapper^ wrapper);
	void write_gv_file_patch(std::string output_file, sql_wrapper^ wrapper);

	void add_type(std::string t);
	void update_type(std::string old_val, std::string new_val);
	bool delete_type(std::string t); //change to int for different errors
	void set_types(std::map<std::string, std::shared_ptr<std::string>> t) { types = t; }
	std::vector<std::string> return_types_names();

	fact return_fact_template(std::string fact_name) { return all_facts_template[fact_name]; }
	fact return_postcondition_template(std::string attack_name) { return postconditions_template[attack_name]; }
	attack_rule return_attack_template(std::string attack_name) { return attacks_template[attack_name]; }

	std::shared_ptr<std::string> return_type_ptr(std::string type_name) { return types[type_name]; }

	bool delete_attack(std::string atack_name, std::string postcondition_name);
	bool delete_postcondition(std::string postcondition_name);
	bool delete_fact_template(std::string fact_name);
	
	std::map<std::string, fact> return_fact_template() { return fact_template; }
	std::map<std::string, fact> return_postcondition_template() { return postconditions_template; }
	std::map<std::string, attack_rule> return_attack_template() { return attacks_template; }

	std::vector<std::string> return_attack_for_postcondition(std::string postcondition_name);

	void solve_task_plus(sql_wrapper^ wrapper, double& gen_time, double& solve_time, bool test_calc);
	void write_gv_file_iterable_plus(std::string output_file, sql_wrapper^ wrapper);
	
	void add_patch_variable(int var);
	void delete_patch_variable(int var);

	void get_names_from_table(sql_wrapper^ wrapper, std::map<std::string, int> &facts, std::map<std::string, int> &attacks);
	void set_variables_for_patch(std::vector<int> p) { patch_variables_to_sort = p; }

	std::vector<int> get_patches();

	int get_numbers_to_patch(std::vector<int>& attacks_to_patch, std::vector<int> &facts_to_patch, sql_wrapper^ wrapper);

	void generate_ag_iterable(sql_wrapper^ wrapper);
	void generate_ag_plus(sql_wrapper^ wrapper);
	void generate_ag_patch(sql_wrapper^ wrapper);
	void save_gv_file(std::string filename);
	void write_gv_file(std::string &st);

	void drop_all_tables(sql_wrapper^ wrapper);

	void set_proceed(bool value);

	void set_scissoing_mode(scisorring_modes s) {
		current_scissoring_mode = s;
	};

private:

	void enumerate(sql_wrapper^ db);
	void encode_cnf_base();
	void encode_cnf_base_for_patch();
	void encode_cnf_iterable();
	void encode_cnf_for_patch(std::vector<int> attacks_to_block, int n, int m=0);

	void run_solver_iterable(double& time);
	void run_solver_single(double& time);
	void run_solver_single_for_patch(double& time, Problem &p, int& n, int m=0, double timeout=0.0);

	void enumerate_postconditions(sql_wrapper^ db);
	void enumerate_attacks(sql_wrapper^ db);
	void enumerate_attacks_by_steps();	

	void encode_conjuct(std::vector<std::vector<int>>& local_cnf, int v1, std::vector<int> conjunct);
	void encode_disjunct(std::vector<std::vector<int>>& local_cnf, int v1, std::vector<int> disjunct);

	void create_attack_tables(std::vector<std::string>& attack_names, sql_wrapper^ wrapper);
	void drop_attack_tables(std::vector<std::string> attack_names, sql_wrapper^ wrapper);

	void fill_tables_for_postcondition(sql_wrapper^ wrapper);
	void drop_tables_for_postconditions(sql_wrapper^ wrapper);
	void process_enumeration(sql_wrapper^ wrapper);
	

	void proceed_with_steps_eq_one(sql_wrapper^ wrapper);

	bool is_name_exists(std::string name);
	
	std::map<int,std::string> get_names_from_table(sql_wrapper^ wrapper);
	std::map<std::string, std::shared_ptr<std::string>> types;

	std::map<std::string, fact> all_facts_template;
	
	std::map<std::string, fact> fact_template;

	std::map<std::string, fact> postconditions_template;
	std::map<std::string, std::vector<fact>> postconditions;

	std::map<std::string, attack_rule> attacks_template;

	std::map<std::string, std::vector<attack_rule>> postcond_to_attacks_template;

	std::map<int, std::vector<int>> postcond_to_attack_num, attack_to_facts_num;

	int start_postconditions, start_attacks, delta, end_attack;

	std::map<fact, std::vector<attack_rule>> postcond_to_attacks;
	//std::map<fact, std::vector<int>> scissoring_nums;
	std::map<int, std::vector<int>> scissoring_nums;

	std::vector<std::vector<int>> assumptions;
	std::map<int, int> assumptions_num;
	std::vector<std::vector<int>> sat_assignments;

	std::map<int, std::vector<int>> sat_connections;

	std::set<int> facts_num;
	long long int nVars = 0;
	std::vector<std::vector< int >> cnf;
	int steps = 1;

	std::vector<std::vector<bool>> values;

	std::vector<std::string> attack_names;

	std::vector<int> patch_variables;
	std::vector<int> patch_variables_to_sort;
	
	std::vector<std::vector<int>> tmp_vector_assumption_patch;

	bool is_processed = false, steps_changed = false;

	Attack_Graph AG;
	scisorring_modes current_scissoring_mode = scisorring_modes::postcondition_scissoring;

	bool test_calc = false;

	std::vector<int> r;
};

