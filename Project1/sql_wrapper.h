#pragma once 
#include <string>
#include <vector>
#include <map>
#include <set>

using namespace System::Data;
using namespace System::Data::SQLite;
ref class sql_wrapper
{
public:
	sql_wrapper() ;
	~sql_wrapper();
	!sql_wrapper();
	bool open_connection(std::string db_path);
	void close_connection();
	SQLiteDataAdapter^ return_adapter(std::string table_name, std::vector<std::string> columns);
	void enumerate_table(std::string table_name, long long int& nV, bool is_temporary);
	void fill_table_for_graphviz(std::string dst_table_name, std::vector<std::string> dst_table_columns, std::string src_table_name, std::vector<std::string> src_table_columns);
	void fill_table_for_attack(std::string dst_table_name, std::map<std::string,std::vector<std::string>> dst_table_columns, std::map<std::string, std::vector<std::pair<std::string, std::string>>> params, std::map<std::string, std::vector<std::pair<std::string, std::string>>> consts, std::string or_attack_name);
	void fill_table_for_postcondition(std::string dst_table_name, std::vector<std::string> dst_table_columns, std::string src_table_name, std::vector<std::string> src_table_columns);
	void fill_names(std::string table_name, std::vector <std::string> columns);
	void update_table(std::string table_name, std::vector<std::map<std::string, std::string>> values_to_update, std::vector<std::map<std::string, std::string>> known_values);
	void create_table(std::string name, std::vector<std::string> colunms, std::vector<std::string> num_columns);
	void create_table(std::string name, std::vector<std::string> colunms, std::vector<std::string> num_columns, bool is_temporary);
	void attach_memory_database();
	void detach_memory_database();
	void add_row(std::string table_name, std::map<std::string, std::string> cols_to_values);
	void drop_table(std::string table_name);
	void drop_table(std::string table_name, bool is_temporary);
	std::vector<std::map<std::string, std::string>> select_from_table(std::string table_name, std::vector<std::string> values_to_select, std::map<std::string, std::string> known_values, bool is_temporary);
	
	void return_numbers_to_patch(std::string host_1, std::string host_2, std::vector<int>& numbers, std::set<std::string>& hosts_to_block);
	std::vector<int> select_from_table_for_patch(std::string table_name, std::string host_1, std::string host_2, std::set<std::string>& hosts_to_block);
private:
	SQLiteConnection ^ db;

	std::string secure_string(std::string s);
	void execute_statement(std::string statement);

	System::String^ in_memory_db = "tmp";
};

