#include "MainForm.h"
#include "minisat22_wrapper.h"
#include <set>
#include "my_types.h"
#include "attack_rule.h"
#include "mtl\Alg.h"
//#include <future>  

using namespace System;
using namespace System::Windows::Forms;
using namespace System::IO;
using namespace System::IO::Compression;


[STAThreadAttribute]
void Main(array<String^>^ args) {
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	Project1::MainForm form;
	Application::Run(%form);	
}

System::Void Project1::MainForm::openToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
	clear_extract_folder();

	if (openFile->ShowDialog() == System::Windows::Forms::DialogResult::OK) {
		ZipFile::ExtractToDirectory(openFile->FileName, extract_path);		
		wrapper_sql->open_connection(MarshalString(db_path));
		delete s_system;
		s_system = new security_system;

		if (PostScissoring->Checked) {
			s_system->set_scissoing_mode(scisorring_modes::postcondition_scissoring);
		}
		else {
			s_system->set_scissoing_mode(scisorring_modes::attacks_scissoring);
		}

		if (wrapper_xml->open_file(MarshalString(xml_path), *s_system)) {
			clear_all_boxes();
			set_names();
			set_types_names(s_system->return_types_names());
			MessageBox::Show("Open successfully.");
		}
		else {
			MessageBox::Show("Error while opening file.");
		}		
	}	
}

bool Project1::MainForm::add_fact_proto(System::String^ fact_name, std::vector<std::string> types, std::vector<std::string> names, std::vector<std::string> groups)
{
	std::string fact_std = MarshalString(fact_name);
	if (s_system->add_fact_template(fact_std, types, names, groups)) {
		FactsNamesCmBox->Items->Add(fact_name);
		FactProtoNameCmBox->Items->Add(fact_name);
		AttackFactNamesCmb->Items->Add(fact_name);		
		wrapper_sql->create_table(fact_std, names, std::vector<std::string>{"num", "name"}, false);
		return true;
	}
	else {
		MessageBox::Show("Already exists fact with that name.");
		return false;
	}
}

void Project1::MainForm::set_names()
{
	using namespace std;
	vector<string> facts_names, or_names, and_names;
	s_system->return_all_names(facts_names, or_names, and_names);

	clear_cm_boxes();

	for (auto& f : facts_names) {
		System::String^ name = gcnew System::String(f.c_str());
		FactsNamesCmBox->Items->Add(name);
		FactProtoNameCmBox->Items->Add(name);
		AttackFactNamesCmb->Items->Add(name);		
	}

	if (FactsNamesCmBox->Items->Count > 0) {
		FactsNamesCmBox->SelectedIndex = 0;
		FactProtoNameCmBox->SelectedIndex = 0;
		AttackFactNamesCmb->SelectedIndex = 0;
	}

	for (auto& f : or_names) {
		System::String^ name = gcnew System::String(f.c_str());
		PcondNames->Items->Add(name);
		AttackToPcondCmb->Items->Add(name);
		AttackFactNamesCmb->Items->Add(name);
	}

	if (PcondNames->Items->Count > 0) {
		PcondNames->SelectedIndex = 0;
		AttackToPcondCmb->SelectedIndex = 0;
	}

	for (auto& f : and_names) {
		System::String^ name = gcnew System::String(f.c_str());
		AttackNames->Items->Add(name);
	}

	if (AttackNames->Items->Count > 0) {
		AttackNames->SelectedIndex = 0;
	}
}

void Project1::MainForm::set_types_names(std::vector<std::string> types)
{
	for (auto& t : types) {
		String^ s_type = gcnew System::String(t.c_str());
		TypesLbx->Items->Add(s_type);
		PcondSignTypeCmb->Items->Add(s_type);
		AttackSignTypeCmb->Items->Add(s_type);
		FactVariableTypesCmBox->Items->Add(s_type);
	}

	if (PcondSignTypeCmb->Items->Count > 0) {
		PcondSignTypeCmb->SelectedIndex = 0;
		AttackSignTypeCmb->SelectedIndex = 0;
		FactVariableTypesCmBox->SelectedIndex = 0;
	}
}


void Project1::MainForm::move_listview_row_up(ListView^ lv)
{
	int selected_index = lv->SelectedItems[0]->Index;

	if (selected_index > 0) {
		ListViewItem^ it = (ListViewItem^)lv->Items[selected_index - 1]->Clone();
		ListViewItem^ it2 = (ListViewItem^)lv->Items[selected_index]->Clone();
		lv->Items[selected_index - 1] = it2;
		lv->Items[selected_index] = it;
		selected_index--;
	}

	lv->Focus();
	lv->Items[selected_index]->Selected = true;
}

void Project1::MainForm::move_listview_row_down(ListView^ lv)
{
	int size = lv->Items->Count;
	int selected_index = lv->SelectedItems[0]->Index;

	if (selected_index < size-1) {
		ListViewItem^ it = (ListViewItem^)lv->Items[selected_index + 1]->Clone();
		ListViewItem^ it2 = (ListViewItem^)lv->Items[selected_index]->Clone();
		lv->Items[selected_index + 1] = it2;
		lv->Items[selected_index] = it;
		selected_index++;
	}

	lv->Focus();
	lv->Items[selected_index]->Selected = true;
}



void Project1::MainForm::add_row_to_fact_proto_lv(String^ t, String^ name, String^ group)
{
	ListViewItem^ it = gcnew ListViewItem();
	it->Text = t;	
	if (name != "") {
		ListViewItem::ListViewSubItem^ sub_it = gcnew ListViewItem::ListViewSubItem();
		sub_it->Text = name;
		it->SubItems->Add(sub_it);

		ListViewItem::ListViewSubItem^ sub_it2 = gcnew ListViewItem::ListViewSubItem();
		sub_it2->Text = group;
		it->SubItems->Add(sub_it2);

		FactSignatureLV->Items->Add(it);
	}
	else {
		MessageBox::Show("Name field is empty!");
	}
}

void Project1::MainForm::add_row_to_or_attack_lv(String^ t, String^ name)
{
	ListViewItem^ it = gcnew ListViewItem();
	it->Text = t;
	if (name != "") {
		ListViewItem::ListViewSubItem^ sub_it = gcnew ListViewItem::ListViewSubItem();
		sub_it->Text = name;
		it->SubItems->Add(sub_it);

		PcondSignLV->Items->Add(it);
	}
	else {
		MessageBox::Show("Name field is empty!");
	}
}

bool Project1::MainForm::add_postcond_proto(System::String^ fact_name, std::vector<std::string> types, std::vector<std::string> names, std::vector<std::string> groups)
{
	std::string fact_std = MarshalString(fact_name);
	if (s_system->add_postcond_template(fact_std, types, names, groups)) {
		AttackToPcondCmb->Items->Add(fact_name);
		return true;
	}
	return false;
}

void Project1::MainForm::add_row_to_and_attack_lv(String^ t, String^ name, String^ value)
{
	ListViewItem^ it = gcnew ListViewItem();
	it->Text = t;
	if (name != "") {
		ListViewItem::ListViewSubItem^ sub_it = gcnew ListViewItem::ListViewSubItem();
		sub_it->Text = name;
		it->SubItems->Add(sub_it);

		ListViewItem::ListViewSubItem^ sub_it2 = gcnew ListViewItem::ListViewSubItem();
		sub_it2->Text = value;
		it->SubItems->Add(sub_it2);

		AttackSignLV->Items->Add(it);
	}
	else {
		MessageBox::Show("Name field is empty!");
	}
}

void Project1::MainForm::set_status(String^ status)
{
	StatusLbl->Text = status;
}

void Project1::MainForm::clear_cm_boxes()
{
	FactsNamesCmBox->Items->Clear();
	FactProtoNameCmBox->Items->Clear();
	AttackFactNamesCmb->Items->Clear();

	PcondNames->Items->Clear();
	AttackToPcondCmb->Items->Clear();

	AttackNames->Items->Clear();
}

void Project1::MainForm::delete_type(String^ name)
{
	PcondSignTypeCmb->Items->Remove(name);
	AttackSignTypeCmb->Items->Remove(name);
	FactVariableTypesCmBox->Items->Remove(name);
}

void Project1::MainForm::set_times(double gen_time, double solve_time)
{
	GenTimeLbl->Text = System::Convert::ToString(gen_time);
	SolvingTimeLbl->Text = System::Convert::ToString(solve_time);
}

void Project1::MainForm::clear_all_boxes()
{
	CNFTextBox->Clear();
	TypeTextBox->Clear();
	TypesLbx->Items->Clear();
	FactProtoNameCmBox->Items->Clear();
	FactsNamesCmBox->Items->Clear();
	PcondNames->Items->Clear();
	AttackNames->Items->Clear();
	AttackFactNamesCmb->Items->Clear();
	dataSet1->Reset();
}

void Project1::MainForm::set_status(std::string status)
{
	StatusLbl->Text = gcnew String(status.c_str());
	if (status == "Unsolved") {
		MessageBox::Show("No solution");
	}
}

System::Void Project1::MainForm::SAT_AG_build_Click(System::Object^ sender, System::EventArgs^ e)
{	
	double gen_time, solve_time;
	std::string message = s_system->solve_task_iter(wrapper_sql, gen_time, solve_time, false);
	//std::future<std::string> aa = std::async(s_system->solve_task_iter,  wrapper_sql, gen_time, solve_time, false);
	set_status(message);
	set_times(gen_time, solve_time);
}

System::Void Project1::MainForm::SaveGraphvizButton_Click(System::Object^ sender, System::EventArgs^ e)
{
	if (saveGraphvizFile->ShowDialog() == System::Windows::Forms::DialogResult::OK) {
		s_system->save_gv_file(MarshalString(saveGraphvizFile->FileName));
	}
}

System::Void Project1::MainForm::WriteCnf_Click(System::Object^ sender, System::EventArgs^ e)
{
	CNFTextBox->Text = gcnew String(s_system->return_cnf().c_str());
}

System::Void Project1::MainForm::Show_fact_table_Click(System::Object^ sender, System::EventArgs^ e)
{
	dataSet1->Reset();
	ad = wrapper_sql->return_adapter(MarshalString(FactsNamesCmBox->Text), s_system->return_fact_template(MarshalString(FactsNamesCmBox->Text)).return_columns(false));
	ad->Fill(dataSet1, FactsNamesCmBox->Text);
	AllFactsView->DataSource = dataSet1->Tables[0];	
}

System::Void Project1::MainForm::Update_Fact_Table_Click(System::Object^ sender, System::EventArgs^ e)
{
	ad->Update(dataSet1->Tables[0]);
	s_system->set_proceed(false);
}

System::Void Project1::MainForm::DeleteFact_Click(System::Object^ sender, System::EventArgs^ e)
{
	int id = AllFactsView->CurrentRow->Index;
	DataRow^ row = dataSet1->Tables[0]->Rows[id];
	row->Delete();
 	ad->Update(dataSet1->Tables[0]);
	s_system->set_proceed(false);
}

System::Void Project1::MainForm::AddTypeBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	System::String^ t = TypeTextBox->Text;
	if (t != "") {
		TypeTextBox->Text = "";
		TypesLbx->Items->Add(t);

		FactVariableTypesCmBox->Items->Add(t);
		FactVariableTypesCmBox->SelectedIndex = 0;

		PcondSignTypeCmb->Items->Add(t);
		PcondSignTypeCmb->SelectedIndex = 0;

		AttackSignTypeCmb->Items->Add(t);
		AttackSignTypeCmb->SelectedIndex = 0;

		s_system->add_type(MarshalString(t));
	}
	TypeTextBox->Focus();
}

System::Void Project1::MainForm::UpdateTypeBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	int index = TypesLbx->SelectedIndex;
	if (index == -1) {
		MessageBox::Show("No fact selected");
	}
	else {
		System::String^ new_val = Microsoft::VisualBasic::Interaction::InputBox("Enter new value", "Update Type", "", -1, -1);
		if (new_val != "") {
			std::string old_std, new_std;
			old_std = MarshalString(TypesLbx->Items[index]->ToString());
			new_std = MarshalString(new_val);
			s_system->update_type(old_std, new_std);

			TypesLbx->Items[index] = new_val;
			FactVariableTypesCmBox->Items[index] = new_val;
			PcondSignTypeCmb->Items[index] = new_val;
			AttackSignTypeCmb->Items[index] = new_val;
		}
	}
}

System::Void Project1::MainForm::DeleteTypeBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	int index = TypesLbx->SelectedIndex;
	std::string type_name = MarshalString(TypesLbx->Items[index]->ToString());
	if (s_system->delete_type(type_name)) {
		String^ name = TypesLbx->Items[index]->ToString();
		delete_type(name);
		TypesLbx->Items->RemoveAt(index);
	}
	else {
		MessageBox::Show("Can't delete this type.\t\nThere is fact that uses it.");
	}
}

System::Void Project1::MainForm::AddFactVariableBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	add_row_to_fact_proto_lv(FactVariableTypesCmBox->Text, FactVariableNameTxt->Text, FactVariableGroupTxt->Text);
	FactVariableTypesCmBox->Focus();
	FactVariableNameTxt->Text = "";
	FactVariableGroupTxt->Text = "";

}

System::Void Project1::MainForm::DeleteFactVariableBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	if (FactSignatureLV->SelectedItems->Count > 0) {
		int index = FactSignatureLV->SelectedItems[0]->Index;
		FactSignatureLV->Items->RemoveAt(index);
	}
}

System::Void Project1::MainForm::SaveFactBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	int size = FactSignatureLV->Items->Count;
	if (size > 0 && FactProtoNameCmBox->Text != "") {
		std::vector<std::string> groups, names, types;
		for (int i = 0; i < size; ++i) {
			types.push_back(MarshalString(FactSignatureLV->Items[i]->SubItems[0]->Text->ToString()));
			names.push_back(MarshalString(FactSignatureLV->Items[i]->SubItems[1]->Text->ToString()));
			groups.push_back(MarshalString(FactSignatureLV->Items[i]->SubItems[2]->Text->ToString()));
		}
		add_fact_proto(FactProtoNameCmBox->Text, types, names, groups);
		FactSignatureLV->Items->Clear();
		FactVariableNameTxt->Text = "";
		FactVariableGroupTxt->Text = "";
	}
	else {
		MessageBox::Show("Empty name field or signature");
	}
}

System::Void Project1::MainForm::FactUpVariableBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	move_listview_row_up(FactSignatureLV);
}

System::Void Project1::MainForm::FactDownVariableBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	move_listview_row_down(FactSignatureLV);
}

System::Void Project1::MainForm::FactProtoNameCmBox_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e)
{
	if (FactVariableTypesCmBox->Items->Count > 0) {
		FactVariableTypesCmBox->SelectedIndex = 0;
	}
	FactVariableNameTxt->Text = "";
	FactVariableGroupTxt->Text = "";
	signs s = s_system->return_fact_template(MarshalString(FactProtoNameCmBox->Text)).return_signature();
	FactSignatureLV->Items->Clear();
	for (auto& el : s) {
		String ^t, ^name, ^group;
		t = gcnew String(el.first._return_type().c_str());
		name = gcnew String(el.first.return_name().c_str());
		group = gcnew String(el.second.c_str());
		add_row_to_fact_proto_lv(t, name, group);
	}
}

System::Void Project1::MainForm::AddPostcondVariableBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	add_row_to_or_attack_lv(PcondSignTypeCmb->Text, OrAttackSignNameTxt->Text);
	PcondSignTypeCmb->Focus();
	OrAttackSignNameTxt->Text = "";
}

System::Void Project1::MainForm::DeletePostcondVariableBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	int index = PcondSignLV->SelectedItems[0]->Index;
	if (index != -1) PcondSignLV->Items->RemoveAt(index);
}

System::Void Project1::MainForm::DeletePostcondBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	std::string attack_name = MarshalString(PcondNames->Text);
	if (s_system->delete_postcondition(attack_name)) {
		PcondNames->Items->Remove(PcondNames->Text);
		PcondNames->SelectedIndex = 0;
	}
	else {
		MessageBox::Show("Can't delete this attack");
	}
}

System::Void Project1::MainForm::AttackFromPostcondBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	int size = PcondSignLV->Items->Count;
	AttackSignLV->Items->Clear();
	for (int i = 0; i < size; ++i) {
		ListViewItem^ it = (ListViewItem^)PcondSignLV->Items[i]->Clone();
		AttackSignLV->Items->Add(it);
	}
	AttackToPcondCmb->SelectedIndex = AttackToPcondCmb->FindStringExact(PcondNames->Text);
}

System::Void Project1::MainForm::AttackSignLV_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e)
{
	if (AttackSignLV->SelectedItems->Count > 0) {
		int index = AttackSignLV->SelectedItems[0]->Index;
		ListViewItem^ it = AttackSignLV->Items[index];
		AttackSignTypeCmb->SelectedIndex = AttackSignTypeCmb->FindStringExact(it->Text);
		AttackSignNameTxt->Text = it->SubItems[1]->Text;
		if (it->SubItems->Count == 3) {
			AttackSignValueTxt->Text = it->SubItems[2]->Text;
		}
		else {
			AttackSignValueTxt->Text = "";
		}
		AttackSignValueTxt->Focus();
	}
}

System::Void Project1::MainForm::DeleteAttackVariableBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	int index = AttackSignLV->FocusedItem->Index;
	if (index > -1) AttackSignLV->Items->RemoveAt(index);
}

System::Void Project1::MainForm::AddAttackVariableBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	add_row_to_and_attack_lv(AttackSignTypeCmb->Text, AttackSignNameTxt->Text, AttackSignValueTxt->Text);
}

System::Void Project1::MainForm::UpdateAttackVariableBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	int index = AttackSignLV->FocusedItem->Index;
	if (index > -1) {
		AttackSignLV->Items[index]->Text = AttackSignTypeCmb->Text;
		AttackSignLV->Items[index]->SubItems[1]->Text = AttackSignNameTxt->Text;
		if (AttackSignLV->Items[index]->SubItems->Count == 3) {
			AttackSignLV->Items[index]->SubItems[2]->Text = AttackSignValueTxt->Text;
		}
		else {
			AttackSignLV->Items[index]->SubItems->Add(AttackSignValueTxt->Text);
		}
	}
}

System::Void Project1::MainForm::ORUpVariableBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	move_listview_row_up(PcondSignLV);
}

System::Void Project1::MainForm::ORDownVariableBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	move_listview_row_down(PcondSignLV);
}

System::Void Project1::MainForm::ANDUpVariableBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	move_listview_row_up(AttackSignLV);
}

System::Void Project1::MainForm::ANDDownVariableBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	move_listview_row_down(AttackSignLV);
}



System::Void Project1::MainForm::AttackFactNamesCmb_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e)
{
	std::string fact_name = MarshalString(AttackFactNamesCmb->Text);
	std::vector<std::string> names = s_system->return_fact_template(fact_name).return_columns(false);
	AttackFactNamesLv->Items->Clear();
	AttackFactValuesLv->Items->Clear();
	for (auto& name : names) {
		String^ s = gcnew String(name.c_str());
		AttackFactNamesLv->Items->Add(s);
		AttackFactValuesLv->Items->Add("");
	}
}

System::Void Project1::MainForm::AttackFactValuesLv_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e)
{

}

System::Void Project1::MainForm::AttackAddFactBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	std::string fact_name = MarshalString(AttackFactNamesCmb->Text);
	signs s = s_system->return_fact_template(fact_name).return_signature();
	int size = s.size();
	for (int i = 0; i < size; ++i) {
		std::string val = MarshalString(AttackFactValuesLv->Items[i]->Text);
		s[i].first.set_value(val);
	}
	fact f(fact_name, s);
	attack_facts->push_back(f);
	String^ full_name = gcnew String(f.return_full_name().c_str());
	AttackFactLb->Items->Add(full_name);
	AttackFactNamesCmb->Focus();
}

System::Void Project1::MainForm::PostcondNames_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e)
{
	std::string fact_name = MarshalString(PcondNames->Text);
	fact pcond = s_system->return_fact_template(fact_name);

	signs s = pcond.return_signature();

	PcondSignLV->Items->Clear();

	for (auto& el : s) {
		String^ t = gcnew String(el.first.return_type()->c_str());
		String^ name = gcnew String(el.first.return_name().c_str());
		ListViewItem^ it = gcnew ListViewItem();

		it->Text = t;
		it->SubItems->Add(name);
		PcondSignLV->Items->Add(it);
	}
	std::vector<std::string> attacks = s_system->return_attack_for_postcondition(fact_name);
	
	AttackNames->Items->Clear();
	for (auto& a : attacks) {
		AttackNames->Items->Add(gcnew String(a.c_str()));
	}
	AttackNames->SelectedIndex = 0;
}

System::Void Project1::MainForm::AttackNames_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e)
{
	std::string and_name = MarshalString(AttackNames->Text);
	
	attack_rule attack = s_system->return_attack_template(and_name);

	std::string or_name = attack.return_pcond();
	AttackToPcondCmb->SelectedIndex = AttackToPcondCmb->FindStringExact(gcnew String(or_name.c_str()));

	signs s = attack.return_signature();

	AttackSignLV->Items->Clear();

	for (auto& el : s) {
		String^ t = gcnew String(el.first.return_type()->c_str());
		String^ name = gcnew String(el.first.return_name().c_str());
		String^ val = gcnew String(el.first.return_value().c_str());
		ListViewItem^ it = gcnew ListViewItem();

		it->Text = t;
		it->SubItems->Add(name);
		it->SubItems->Add(val);
		AttackSignLV->Items->Add(it);

	}

	std::vector<fact> facts = attack.return_facts();
	attack_facts->clear();
	attack_facts->insert(attack_facts->end(),facts.begin(), facts.end());
	AttackFactLb->Items->Clear();
	for (auto& f : facts) {
		String^ name = gcnew String(f.return_full_name().c_str());
		AttackFactLb->Items->Add(name);
	}
}

System::Void Project1::MainForm::AttackFactLb_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e)
{
	int index =	AttackFactLb->SelectedIndex;
	if (attack_facts->size() >= index && index >= 0) {
		signs s = attack_facts->at(index).return_signature();
		String^ attack_name = gcnew String(attack_facts->at(index).return_name().c_str());
		AttackFactNamesCmb->SelectedIndex = AttackFactNamesCmb->FindStringExact(attack_name);

		AttackFactNamesLv->Items->Clear();
		AttackFactValuesLv->Items->Clear();
		for (auto el : s) {
			String^ name = gcnew String(el.first.return_name().c_str());
			String^ val = gcnew String(el.first.return_value().c_str());
			AttackFactNamesLv->Items->Add(name);
			AttackFactValuesLv->Items->Add(val);
		}
		
		AttackFactValuesLv->Focus();
	}
}

System::Void Project1::MainForm::SavePostcondkBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	int size = PcondSignLV->Items->Count;
	signs s;
	if (size > 0 && PcondNames->Text != "") {
		std::vector<std::string> groups, names, types;
		for (int i = 0; i < size; ++i) {
			types.push_back(MarshalString(PcondSignLV->Items[i]->SubItems[0]->Text->ToString()));
			names.push_back(MarshalString(PcondSignLV->Items[i]->SubItems[1]->Text->ToString()));
			groups.push_back("");
		}
		if (add_postcond_proto(PcondNames->Text, types, names, groups)) {			
			AttackFactNamesCmb->Items->Add(PcondNames->Text);
			PcondNames->Items->Add(PcondNames->Text);
			PcondSignLV->Items->Clear();
		}
		else {
			MessageBox::Show("Already exists postcondition with such name.");
		}
	}
	else {
		MessageBox::Show("Empty name field or signature");
	}
}

System::Void Project1::MainForm::SaveAttackBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	std::string attack_name = MarshalString(AttackNames->Text);
	signs s;
	int size = AttackSignLV->Items->Count;
	AttackNames->Items->Add(AttackNames->Text);
	for (int i = 0; i < size; ++i) {
		std::string type_name = MarshalString(AttackSignLV->Items[i]->Text);
		auto type_prt = s_system->return_type_ptr(type_name);
		std::string name = MarshalString(AttackSignLV->Items[i]->SubItems[1]->Text);
		std::string val = MarshalString(AttackSignLV->Items[i]->SubItems[2]->Text);
		record r(type_prt, name, val);
		s.push_back(std::make_pair(r, ""));
	}
	std::string pcond_name = MarshalString(AttackToPcondCmb->Text);

	std::vector<std::string> columns;
	for (auto& f : *attack_facts) {
		std::string fact_name = f.return_name();
		signs fact_sign = f.return_signature();
		for (auto& el : fact_sign) {
			std::string val = el.first.return_value();
			std::string n = el.first.return_name();
			if (is_variable(val) && val != "*" && (std::find(columns.begin(), columns.end(), val) == columns.end())) { columns.push_back(val); }
			if (val == "*") { columns.push_back(fact_name + "_" + n); }
		}

	}

	if (s_system->add_attack_template(attack_name, s, *attack_facts, pcond_name)) {
		attack_facts->clear();
		AttackSignLV->Items->Clear();
		AttackFactLb->Items->Clear();
	}
	else {
		MessageBox::Show("Already exists attack with such name.");
	}
}

System::Void Project1::MainForm::AttackDeleteFactBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	if (AttackFactLb->SelectedItems->Count > 0) {
		int index = AttackFactLb->SelectedIndex;
		String^ s = gcnew String(attack_facts->at(index).return_full_name().c_str());
		attack_facts->erase(attack_facts->begin() + index);
		AttackFactLb->SelectedIndex = -1;
		AttackFactLb->Items->Remove(s);
	}
}

System::Void Project1::MainForm::AttackUpdateFactBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	if (AttackFactLb->SelectedItems->Count > 0) {
		int index = AttackFactLb->SelectedIndex;
		std::string fact_name = attack_facts->at(index).return_name();
		signs s = s_system->return_fact_template(fact_name).return_signature();
		int size = s.size();
		for (int i = 0; i < size; ++i) {
			std::string val = MarshalString(AttackFactValuesLv->Items[i]->Text);
			s[i].first.set_value(val);
		}
		fact f(fact_name, s);
		attack_facts->at(index) = f;
	}
}

System::Void Project1::MainForm::DeleteAttackBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	std::string or_name = MarshalString(AttackToPcondCmb->Text);
	std::string and_name = MarshalString(AttackNames->Text);
	if (s_system->delete_attack(and_name, or_name)) {
		AttackNames->Items->Remove(AttackNames->Text);
		AttackNames->SelectedIndex = 0;
	}
	else {
		MessageBox::Show("Can't delete this attack");
	}
}

System::Void Project1::MainForm::DeleteFactBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	//do nothing right now
 	bool condition = (MessageBox::Show("This action will delete table for this fact are you sure?",
 		"Delete fact prototype", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == ::DialogResult::Yes);


 	if (condition) {		
 		if (s_system->delete_fact_template(MarshalString(FactProtoNameCmBox->Text))) {
 			wrapper_sql->drop_table(MarshalString(FactProtoNameCmBox->Text));
 			int index = FactProtoNameCmBox->SelectedIndex;
 			FactProtoNameCmBox->Items->RemoveAt(index);
 		}
		else {
			MessageBox::Show("Unable to delete fact because it is used.");
		}
 	}
}

System::Void Project1::MainForm::saveToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
	if (saveFileDialog->ShowDialog() == System::Windows::Forms::DialogResult::OK) {
		if (File::Exists(saveFileDialog->FileName)) {
			File::Delete(saveFileDialog->FileName);
		}
		std::string f_name = MarshalString(saveFileDialog->FileName);
		wrapper_xml->save_file(MarshalString(xml_path), *s_system);
		wrapper_sql->close_connection();
		
		ZipFile::CreateFromDirectory(extract_path, saveFileDialog->FileName);
		wrapper_sql->open_connection(MarshalString(db_path));
		//clear_extract_folder();
	}
}

System::Void Project1::MainForm::Plus_AG_build_Click(System::Object^ sender, System::EventArgs^ e)
{
	double gen_time, solve_time;
	s_system->solve_task_plus(wrapper_sql, gen_time, solve_time, false);
	
	set_times(gen_time, solve_time);
}


System::Void Project1::MainForm::GetNamesToBlockBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	if (facts_to_block != nullptr) {
		delete facts_to_block;
		delete attacks_to_block;
	}
	facts_to_block = new std::map<std::string, int>;
	attacks_to_block = new std::map<std::string, int>;

	AllFactsNamesLB->Items->Clear();
	FactsNamesToBlockLB->Items->Clear();

	AllPcondNamesLB->Items->Clear();
	PcondsToBlockLB->Items->Clear();

	s_system->get_names_from_table(wrapper_sql, *facts_to_block, *attacks_to_block);
	for (auto& f : *facts_to_block) {
		System::String^ s = gcnew System::String(f.first.c_str());
		AllFactsNamesLB->Items->Add(s);
	}
	for (auto& a : *attacks_to_block) {
		System::String^ s = gcnew System::String(a.first.c_str());
		AllPcondNamesLB->Items->Add(s);
	}
}

System::Void Project1::MainForm::AddFactToBlockBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	int index = AllFactsNamesLB->SelectedIndex;
	int len = AllFactsNamesLB->SelectedIndices->Count;

	while (len>0){
		FactsNamesToBlockLB->Items->Add(AllFactsNamesLB->Items[AllFactsNamesLB->SelectedIndices[0]]);
		AllFactsNamesLB->Items->RemoveAt(AllFactsNamesLB->SelectedIndices[0]);
		len--;
	}
	
}

System::Void Project1::MainForm::DeleteFactFromBlockBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	int index = FactsNamesToBlockLB->SelectedIndex;
	AllFactsNamesLB->Items->Add(FactsNamesToBlockLB->Items[index]);
	FactsNamesToBlockLB->Items->RemoveAt(index);
}

System::Void Project1::MainForm::AddPostcondToBlockBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	int index = AllPcondNamesLB->SelectedIndex;
	int len = AllPcondNamesLB->SelectedIndices->Count;

	while (len > 0) {
		PcondsToBlockLB->Items->Add(AllPcondNamesLB->Items[AllPcondNamesLB->SelectedIndices[0]]);
		AllPcondNamesLB->Items->RemoveAt(AllPcondNamesLB->SelectedIndices[0]);
		len--;
	}
}

System::Void Project1::MainForm::DeletePostcondkFromBlockBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	int index = PcondsToBlockLB->SelectedIndex;
	AllPcondNamesLB->Items->Add(PcondsToBlockLB->Items[index]);
	PcondsToBlockLB->Items->RemoveAt(index);
}

System::Void Project1::MainForm::CalcBlocksBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	double gen_time, solve_time;
	
	std::vector<int> vars_for_patch, vars_to_block;
	for (int i = 0; i < FactsNamesToBlockLB->Items->Count; ++i) {
		std::string name = MarshalString(FactsNamesToBlockLB->Items[i]->ToString());
		vars_for_patch.push_back((*facts_to_block)[name]);
	}
	s_system->set_variables_for_patch(vars_for_patch);
	for (int i = 0; i < PcondsToBlockLB->Items->Count; ++i) {
		std::string name = MarshalString(PcondsToBlockLB->Items[i]->ToString());
		vars_to_block.push_back((*attacks_to_block)[name]);
	}
	int lower_bound = int::Parse(LowBlockingBoundaryTxt->Text), upper_bound = int::Parse(UpBlockingBoundaryTxt->Text);
	int steps = int::Parse(NumberOfStepsTxt->Text);
	double timeout = double::Parse(TimeoutTxtBox->Text);
	std::string message = s_system->solve_task_iter_for_patch(wrapper_sql, steps, gen_time, solve_time, vars_to_block, true, lower_bound, upper_bound, timeout);

	set_status(message);
	set_times(gen_time, solve_time);

}

System::Void Project1::MainForm::WriteGraphvizBlockingBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	if (saveGraphvizFile->ShowDialog() == System::Windows::Forms::DialogResult::OK) {
		s_system->write_gv_file_patch(MarshalString(saveGraphvizFile->FileName), wrapper_sql);
	}
}

System::Void Project1::MainForm::GraphvizToTxtBoxCPP_Click(System::Object^ sender, System::EventArgs^ e)
{
	std::string fname = "tmp.txt";
	s_system->write_gv_file_iterable_plus(fname, wrapper_sql);
	std::ifstream ifs("tmp.txt");
	std::stringstream ss;
	if (ifs) {
		ss << ifs.rdbuf();
		ifs.close();
	}
	System::String^ gv = gcnew System::String(ss.str().c_str());
	CNFTextBox->Text = gv;
	remove(fname.c_str());
}

System::Void Project1::MainForm::WriteGraphvizBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	std::string gv_text = "";
	s_system->write_gv_file(gv_text);	
	System::String^ gv = gcnew System::String(gv_text.c_str());
	CNFTextBox->Text = gv;
}

System::Void Project1::MainForm::showPatchesBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	std::vector<int> patches = s_system->get_patches();
	System::String^ msg="";
	for (auto &i : *facts_to_block) {		
		if (std::find(patches.begin(), patches.end(), i.second) != patches.end()) {			
			msg += gcnew System::String(i.first.c_str());
		}
	}
	MessageBox::Show(msg);
}

System::Void Project1::MainForm::TestPatchesBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	if (opentTestFiles->ShowDialog() == System::Windows::Forms::DialogResult::OK) {
		int range = opentTestFiles->FileNames->Length;
		for (int i = 0; i < range; ++i) {
			clear_extract_folder();
			ZipFile::ExtractToDirectory(opentTestFiles->FileNames[i], extract_path);
			wrapper_sql->open_connection(MarshalString(db_path));
			security_system test_system;
			if (wrapper_xml->open_file(MarshalString(xml_path), test_system)) {

				double gen_time, solve_time;
				test_system.solve_task_plus(wrapper_sql, gen_time, solve_time, true);

				std::vector<int> attack_for_patch, facts_for_patch;

				int steps = test_system.get_numbers_to_patch(attack_for_patch, facts_for_patch, wrapper_sql);
				if (steps > 1) {
					test_system.set_variables_for_patch(facts_for_patch);
					int all_facts = facts_for_patch.size();
					bool solved = false;
					int counter = int::Parse(textBox1->Text);
					double timeout = double::Parse(TimeoutTestTxt->Text);
					std::string message = test_system.solve_task_iter_for_patch(wrapper_sql, steps, gen_time, solve_time, attack_for_patch, false, counter, 0, timeout);

					if (message == "Unsolved") {
						if (counter == all_facts) {
							System::String^ res_str = gcnew System::String(i.ToString() + ": UNSOLVED\t\n");
							TestingBox->AppendText(res_str);
							break;
						}
					}
					else {
						set_times(gen_time, solve_time);
						solved = true;
						double all_time = gen_time + solve_time;
						System::String^ res_str = gcnew System::String(i.ToString() + ": " + steps.ToString() + ", " + counter.ToString() + ", " + all_time.ToString() + "\t\n");
						TestingBox->AppendText(res_str);
					}

					
				}
				else {
					System::String^ res_str = gcnew System::String(i.ToString() + ": " + steps.ToString() + "\t\n");
					TestingBox->AppendText(res_str);
				}

			}
		}
	}
}

System::Void Project1::MainForm::AtScissoring_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
	if (AtScissoring->Checked) {
		s_system->set_scissoing_mode(scisorring_modes::attacks_scissoring);
	}
}

System::Void Project1::MainForm::PostScissoring_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
	if (PostScissoring->Checked) {
		s_system->set_scissoing_mode(scisorring_modes::postcondition_scissoring);
	}
}

System::Void Project1::MainForm::TestTimesBtn_Click(System::Object^ sender, System::EventArgs^ e)
{
	if (opentTestFiles->ShowDialog() == System::Windows::Forms::DialogResult::OK) {
		int range = opentTestFiles->FileNames->Length;
		for (int i = 0; i < range; ++i) {
			clear_extract_folder();
			ZipFile::ExtractToDirectory(opentTestFiles->FileNames[i], extract_path);
			sql_wrapper^ wrapper_sql_test = gcnew sql_wrapper();
			wrapper_sql_test->open_connection(MarshalString(db_path));
			security_system s_system_test;
			//delete s_system;
			//s_system = new security_system;
			if (wrapper_xml->open_file(MarshalString(opentTestFiles->FileNames[i]), s_system_test)) {

				double gen_time, solve_time;
				s_system_test.solve_task_iter(wrapper_sql_test, gen_time, solve_time, true);
				double all_time = gen_time + solve_time;
				System::String^ res_str = gcnew System::String(i.ToString() + ":" + all_time.ToString());

				/*s_system_test.solve_plus(wrapper_sql_test, gen_time, solve_time, true);
				all_time = gen_time + solve_time;
				res_str += ", " + all_time.ToString() */
				res_str+= "\t\n";
				TestingBox->AppendText(res_str);
			}
			
			//s_system_test.drop_all_tables(wrapper_sql_test);
			//wrapper_sql_test->close_connection();
			//delete wrapper_sql_test;
			delete wrapper_sql_test;
		}
	}
}

void Project1::MainForm::clear_extract_folder()
{	
	if (File::Exists(xml_path)) {
		File::Delete(xml_path);
	}

	delete ad;
	AllFactsView->DataSource = NULL;
	wrapper_sql->close_connection();
	GC::Collect();
	GC::WaitForPendingFinalizers();
	if (File::Exists(db_path)) {
		File::Delete(db_path);
	}
	
}

System::Void Project1::MainForm::newToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e)
{
	wrapper_sql->close_connection();
	clear_extract_folder();
	wrapper_sql->open_connection(MarshalString(db_path));
	
	delete s_system;
	s_system = new security_system();

	delete attack_facts;
	attack_facts = new std::vector<fact>;

	delete facts_to_block;
	facts_to_block = new std::map<std::string, int>;

	delete attacks_to_block;
	attacks_to_block = new std::map<std::string, int>;
}
