#pragma once
#include <vector>
#include <string>
#include <map>
#include <functional>

#include "my_types.h"



class fact
{
public:
	fact() {  };
	fact(std::string fact_name, signs in_signature, bool act = true);
	std::string return_name() const { return name; }
	void set_active(bool act) { active = act; }
	bool is_active() { return active; }
	virtual void add_num(int n) const { num.push_back(n); }
	int return_num(int step) const { return num[step]; }
	signs return_signature() const { return signature; }
	std::string return_full_name() const;	
	virtual std::vector<std::string> return_columns(bool with_num = true) const;
	std::vector<std::string> return_additional_columns() const;
	std::vector < std::vector < std::string >> return_values_by_signature(signs s);
	bool operator==(const fact& r1) const { return name == r1.name && signature == r1.signature; }
	bool operator<(const fact& r1) const;
protected:
	std::string name;	
	signs signature;
	bool active;

private:
	sbg signature_by_group;
	mutable std::vector<int> num;

	
};
