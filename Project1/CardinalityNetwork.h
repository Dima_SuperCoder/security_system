#pragma once
#include <vector>

class CardinalityNetwork
{
public:
	
	static std::vector<int> Notmoreatstart(int n, std::vector<int> tobesorted, long long int &nVars, std::vector<std::vector<int>> &cnf);
	static std::vector<int> Notmoreatstart(int n, int m, std::vector<int> tobesorted, long long int &nVars, std::vector<std::vector<int>> &cnf);
private:
	static int twoceil(int k);
	static std::vector <int> Selectodd(std::vector<int> a);
	static std::vector <int> Selecteven(std::vector<int> a);
	static std::vector <int> HMerge(std::vector<int> &a, std::vector<int> &b, int n, long long int &nVars, std::vector<std::vector<int>> &network);
	static std::vector<int> HSort(std::vector<int> &a, int n, long long int &nVars, std::vector<std::vector<int>> &network);
};

